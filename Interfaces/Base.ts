/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import * as https from 'N/https';
import {ErrorMessage} from '../chrobinsonapi';

/**
 * @category Method
 * @hidden
 * */
export interface BaseOptions {
    Failed(clientResponse: https.ClientResponse): void;

    BadRequest(errorMessages: ErrorMessage[]): void;
}

export interface RefreshAccessTokenOptions extends BaseOptions {
    Refreshed(): void;
}
