/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import * as https from 'N/https';

/** @category Method */
export interface GeneratedBOLRetrievalOptions extends BaseOptions {
    loadNumber: number;
    customerCode: string;
    orderNumber: number;
    showItemNotes?: boolean;

    Created(clientResponse: https.ClientResponse): void;
}
