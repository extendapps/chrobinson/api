/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {EventType} from '../Objects/Event/BaseEvent';
import {NavisphereEvent} from '../Objects/NavisphereEvent';

/** @category Method */
export interface EventRetrievalOptions extends BaseOptions {
    from?: string;
    to?: string;
    loadNumber?: string;
    orderNumber?: string;
    billToReferenceNumber?: string;
    customerReferenceNumber?: string;
    navisphereTrackingNumber?: string;
    eventType?: EventType;
    skip?: number;
    take?: number;

    OK(events: NavisphereEvent[]): void;
}

export interface EventRetrievalResponse {
    totalCount?: number;
    results?: NavisphereEvent[];
}
