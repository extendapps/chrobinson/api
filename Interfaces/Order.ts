/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {Order} from '../Objects/Order';

/** @category Method */
export interface OrderCreateOptions extends BaseOptions {
    order: Order;

    Created(orderNumber: number): void;
}

/** @category Method */
export interface OrderDeleteOptions extends BaseOptions {
    orderNumber: number;

    Deleted(): void;
}

export interface OrderUpdateOptions extends BaseOptions {
    order: Order;

    OK(): void;
}

export interface OrderCreateResponse {
    orderNumber: number;
}
