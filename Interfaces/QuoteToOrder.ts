/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {QuoteToOrder} from '../Objects/QuoteToOrder/QuoteToOrder';

/** @category Method */
export interface QuoteToOrderOptions extends BaseOptions {
    quoteToOrders: QuoteToOrder[];

    Created(quoteOrders: QuoteOrder[]): void;
}

export interface QuoteOrder {
    quoteId: number;
    orderNumber: number;
    trackingNumber: string;
}
