/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {Quote} from '../Objects/Quote';
import {RatingRequest} from '../Objects/Rating/RatingRequest';

/** @category Method */
export interface RatingRequestOptions extends BaseOptions {
    request: RatingRequest;

    Created(quoteSummaries: Quote[]): void;
}

export interface RatingRequestResponse {
    quoteSummaries: Quote[];
}
