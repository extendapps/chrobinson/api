/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Address {
    addressLine1?: string;
    addressLine2?: string;
    address1?: string;
    address2?: string;
    address3?: string;
    address4?: string;
    city: string;
    county?: string;
    lat?: string | number;
    long?: string | number;
    stateProvinceCode: string;
    postalCode: string;
    country: string;
    latitude?: number;
    longitude?: number;
}
