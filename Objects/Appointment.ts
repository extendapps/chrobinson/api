/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Appointment {
    scheduledOpenDateTimeUTC: string;
    scheduledCloseDateTimeUTC: string;
    changeReason: '1';
    scheduledCompany: string;
    scheduledWithName: string;
}
