/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface BlindLocation {
    blindName?: string;
    blindAddress?: string;
    blindCity?: string;
    blindState?: string;
}
