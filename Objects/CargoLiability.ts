/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {CurrencyCode} from './CurrencyCode';

export interface CargoLiability {
    perPound: number;
    max: number;
    amount: number;
    currencyCode: CurrencyCode;
}
