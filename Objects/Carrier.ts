/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Carrier {
    name: string;
    carrierCode?: string;
    carrierName?: string;
    scac: string;
    proNumber: string;
    driverName?: string;
    driverPhone?: string;
    secondDriverName?: string;
    sealNumber?: string;
    licensePlate?: string;
    tractorNumber?: string;
    trailerNumber?: string;
    dotNumber?: string;
}
