/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Charge {
    rate: number;
    rateCode: string;
    quantity?: number;
    alternateDescription?: string;
    chargeType?: string;
}
