/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {Address} from './Address';
import {ReferenceNumber} from './ReferenceNumber';

export interface CollectOnDelivered {
    amount: number;
    paymentType: string;
    payableTo: string;
    mailTo: string;
    address: Address;
    referenceNumbers: ReferenceNumber[];
}
