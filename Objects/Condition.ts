/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type Condition = 'Active' | 'OnHold' | 'PartiallyFinLocked' | 'FinanciallyLocked' | 'Rejected' | 'Cancelled' | 'WaitingAcceptance' | 'WaitingRecommendation' | 'NotShipReady' | 'Unreleased';
