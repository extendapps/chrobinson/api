/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {ContactMethod} from './ContactMethod';
import {ReferenceNumber} from './ReferenceNumber';
import {ContactType} from './ContactType';

export interface Contact {
    name: string;
    type: ContactType;
    companyName: string;
    contactMethods: ContactMethod[];
    phoneNumber?: string;
    email?: string;
    referenceNumbers?: ReferenceNumber[];
}
