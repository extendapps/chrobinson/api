/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ContactMethodType} from './ContactMethodType';

export interface ContactMethod {
    method: ContactMethodType;
    value: string;
}
