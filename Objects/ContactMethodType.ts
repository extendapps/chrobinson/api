/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type ContactMethodType = 'Email' | 'Phone' | 'Fax';
