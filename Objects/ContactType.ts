/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type ContactType = 'Vendor' | 'Buyer' | 'Contact' | 'OrderedBy' | 'NotifyParty' | 'OnlineContact' | 'SchedWith' | 'POCustomer';
