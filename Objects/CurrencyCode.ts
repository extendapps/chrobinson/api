/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type CurrencyCode = 'USD' | 'CAD' | 'MXN' | 'EUR';
