/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {EquipmentType} from './EquipmentType';

export interface Equipment {
    equipmentType?: EquipmentType;
    quantity?: number;
}
