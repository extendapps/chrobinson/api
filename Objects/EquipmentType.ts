/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type EquipmentType = 'Van' | 'Reefer' | 'Flatbed' | 'Lcl' | 'Container20' | 'Container40' | 'Container40HighCube' | 'Container45HighCube' | 'Container20FlatRack' | 'Container40FlatRack' | 'Container40HighCubeFlatRack' | 'Container45FlatRack' | 'Container20OpenTop' | 'Container40OpenTop' | 'Container20Reefer' | 'Container40Reefer' | 'Container40HighCubeReefer' | 'Container45Reefer' | 'Container20ReeferDryUsage' | 'Container40ReeferDryUsage' | 'Container40HighCubeReeferDryUsage' | 'Container45ReeferDryUsage' | 'Container20Platform';
