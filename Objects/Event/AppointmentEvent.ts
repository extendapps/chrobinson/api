/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Appointment} from '../Appointment';
import {SingleLoadEvent} from './BaseEvent';
import {EventLocation} from './EventLocation';

interface AppointmentEvent extends SingleLoadEvent {
    eventType: 'APPOINTMENT UPDATED';
    location: EventLocation;
    appointment: Appointment;
}

export interface AppointmentUpdatedEvent extends AppointmentEvent {
    eventType: 'APPOINTMENT UPDATED';
}
