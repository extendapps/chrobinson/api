/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {OrderDetail} from './OrderDetail';
import {Carrier} from '../Carrier';

export type EventType = 'ORDER CREATED' | 'ORDER REJECTED' | 'ORDER UPDATED' | 'ORDER CANCELED' | 'ORDER COMPLETED' | 'LOAD CREATED' | 'LOAD BOOKED' | 'LOAD CANCELLED' | 'CARRIER ARRIVED' | 'CARRIER DEPARTED' | 'LOAD PICKED UP' | 'LOAD DELIVERED' | 'IN TRANSIT' | 'APPOINTMENT UPDATED' | 'PACKAGE IN TRANSIT' | 'PACKAGE PICKED UP' | 'PACKAGE DELIVERED';

export interface BaseEvent {
    eventType: EventType;
}

export interface SingleLoadEvent {
    loadNumber: number;
    orderDetails: OrderDetail[];
    carrier: Carrier;
}

export interface MultiLoadEvent {
    loadNumbers: number[];
    orderDetail: OrderDetail[];
}
