/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface EventAddress {
    addressLine1: string;
    addressLine2?: string;
    city: string;
    stateProvinceCode: string;
    postalCode: string;
    country: string;
    latitude?: number;
    longitude?: number;
}
