/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {ReferenceNumber} from '../ReferenceNumber';
import {FreightClass} from '../FreightClass';
import {PackagingCode} from '../PackagingCode';
import {WeightUnit} from '../WeightUnit';
import {LinearUnit} from '../LinearUnit';
import {VolumeUnit} from '../VolumeUnit';

export interface EventItem {
    packagingType: PackagingCode;
    description?: string;
    productCode?: string;
    freightClass?: FreightClass;
    insuranceValue?: number;
    primaryReferenceNumber?: string;
    poNumber?: string;
    skuNumber?: string;
    pluNumber?: string;
    actualQuantity?: string;
    actualWeight?: number;
    actualWeightUnitOfMeasure?: WeightUnit;
    actualPallets?: string;
    actualVolume?: string;
    actualVolumeUnitOfMeasure?: VolumeUnit;
    actualPackagingLength?: string;
    actualPackagingHeight?: string;
    actualPackagingWidth?: string;
    actualPackagingUnitOfMeasure?: LinearUnit;
    referenceNumbers?: ReferenceNumber[];
}
