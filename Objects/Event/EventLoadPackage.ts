/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ReferenceNumber} from '../ReferenceNumber';

export interface EventLoadPackage {
    trackingNumber?: string;
    packageType?: string;
    referenceNumbers: ReferenceNumber[];
}
