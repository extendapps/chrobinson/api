/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Address} from '../Address';
import {Contact} from '../Contact';
import {BlindLocation} from '../BlindLocation';
import {ReferenceNumber} from '../ReferenceNumber';
import {SpecialRequirement} from '../SpecialRequirement';
import {LoadingType} from '../LoadingType';

export interface EventLocation {
    chrLocationId?: string;
    customerLocationId?: string;
    city?: string;
    stateProvinceCode?: string;
    country?: 'US';
    latitude?: number;
    longitude?: number;
    type?: 'origin' | 'destination' | 'Pick' | 'Drop';
    style?: string;
    stopSequenceNumber?: number;
    sequenceNumber?: number;
    name?: string;
    address?: Address;
    pickUpNumber?: string;
    deliveryNumber?: string;
    locationContact?: Contact;
    appointmentNumber?: string;
    blindLocation?: BlindLocation;
    openDateTime?: string;
    closeDateTime?: string;
    arrivalDate?: string;
    arrivalDateTime?: string;
    departureDate?: string;
    departureDateTime?: string;
    scheduledOpenDateTime?: string;
    scheduledCloseDateTime?: string;
    scheduledEnteredDate?: string;
    readyAvailableDateTime?: string;
    isDropTrailerNeeded?: boolean;
    scheduledWith?: string;
    etaDateTime?: string;
    rsDateTime?: string;
    specialRequirement?: SpecialRequirement;
    scalesRequired?: boolean;
    hasDropTrailer?: boolean;
    loadingTypeCode?: LoadingType;
    specialInstructions?: string;
    contacts?: Contact[];
    referenceNumbers?: ReferenceNumber[];
}
