/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ReferenceNumber} from '../ReferenceNumber';

export interface EventPackage {
    trackingNumber?: string;
    packageType?: string;
    ediCode: string;
    note: string;
    referenceNumbers: ReferenceNumber[];
}
