/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface InTransitAddress {
    city: string;
    stateProvinceCode: string;
    country: string;
    latitude?: number;
    longitude?: number;
}
