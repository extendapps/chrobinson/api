/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {SingleLoadEvent} from './BaseEvent';
import {InTransitAddress} from './InTransit/InTransitAddress';

export interface InTransitEvent extends SingleLoadEvent {
    eventType: 'IN TRANSIT';
    location: InTransitAddress;
    notes: string;
}
