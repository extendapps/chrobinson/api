/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {Charge} from '../../Charge';
import {Contact} from '../../Contact';
import {ReferenceNumber} from '../../ReferenceNumber';
import {InvoiceMethod} from '../../InvoiceMethod';
import {CurrencyCode} from '../../CurrencyCode';

export interface BookedBillTo {
    customerCode: string;
    customerName: string;
    currencyCode: CurrencyCode;
    invoiceMethod: InvoiceMethod;
    milesBilled: string;
    exchangeRate: string;
    charges?: Charge[];
    contacts?: Contact[];
    referenceNumbers?: ReferenceNumber[];
}
