/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {Contact} from '../../Contact';
import {ReferenceNumber} from '../../ReferenceNumber';

export interface BookedCustomer {
    customerCode: string;
    customerName: string;
    contacts?: Contact[];
    referenceNumbers?: ReferenceNumber[];
}
