/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {OrderItem} from '../Order/OrderItem';
import {OrderPackage} from '../Order/OrderPackage';
import {SingleLoadEvent} from './BaseEvent';
import {BookedBillTo} from './Load/BookedBillTo';
import {BookedCustomer} from './Load/BookedCustomer';

export interface LoadEvent extends SingleLoadEvent {
    eventType: 'LOAD CREATED' | 'LOAD BOOKED' | 'LOAD CANCELLED';
    equipmentType: string;
    mode: string;
    serviceOffering: string;
    readyByDate: string;
    pickupByDate: string;
    deliveryAvailableByDate: string;
    deliveryByDate: string;
    billTos: BookedBillTo[];
    customers?: BookedCustomer[];
    locations: Location[];
    items: OrderItem[];
    packages: OrderPackage[];
}

export interface LoadCreatedEvent extends LoadEvent {
    eventType: 'LOAD CREATED';
}

export interface LoadBookedEvent extends LoadEvent {
    eventType: 'LOAD BOOKED';
}

export interface LoadCancelledEvent extends LoadEvent {
    eventType: 'LOAD CANCELLED';
}
