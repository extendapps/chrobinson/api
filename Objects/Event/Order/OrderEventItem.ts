/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {ReferenceNumber} from '../../ReferenceNumber';
import {FreightClass} from '../../FreightClass';
import {PackagingCode} from '../../PackagingCode';
import {WeightUnit} from '../../WeightUnit';
import {LinearUnit} from '../../LinearUnit';
import {VolumeUnit} from '../../VolumeUnit';

export interface OrderEventItem {
    packagingType: PackagingCode;
    description?: string;
    productCode?: string;
    freightClass?: FreightClass;
    upcNumber?: string;
    skuNumber?: string;
    pluNumber?: string;
    insuranceValue?: number;
    minWeight?: number;
    minWeightUnitOfMeasure?: WeightUnit;
    maxWeight?: number;
    maxWeightUnitOfMeasure?: WeightUnit;
    length?: number;
    lengthUnitOfMeasure?: LinearUnit;
    height?: number;
    heightUnitOfMeasure?: LinearUnit;
    width?: number;
    widthUnitOfMeasure?: LinearUnit;
    volume?: number;
    volumeUnitOfMeasure?: VolumeUnit;
    pallets?: number;
    palletSpaces?: number;
    trailerLengthUsed?: number;
    trailerLengthUsedUnitOfMeasure?: LinearUnit;
    referenceNumbers?: ReferenceNumber[];
}
