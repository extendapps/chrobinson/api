/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ReferenceNumber} from '../../ReferenceNumber';
import {OrderEventLocationType} from './OrderEventLocationType';
import {OrderEventLocationStyle} from './OrderEventLocationStyle';
import {EventAddress} from '../EventAddress';

export interface OrderEventLocation {
    chrLocationId: string;
    customerLocationId?: string;
    type: OrderEventLocationType;
    style: OrderEventLocationStyle;
    name?: string;
    address?: EventAddress;
    referenceNumbers?: ReferenceNumber[];
}
