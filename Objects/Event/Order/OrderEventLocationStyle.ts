/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type OrderEventLocationStyle = 'Customers' | 'Distributor' | 'Warehouse' | 'Plant' | 'Pool' | 'CrossDock' | 'WarehouseSpecial' | 'Agent' | 'Terminal' | 'RailRamp' | 'Port';
