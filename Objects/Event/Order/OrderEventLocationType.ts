/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type OrderEventLocationType = 'Pick' | 'Drop' | 'Intermediate' | 'OriginPort' | 'DestinationPort' | 'IntermediatePort';
