/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {Condition} from '../../Condition';
import {OrderEventLocation} from './OrderEventLocation';
import {OrderEventItem} from './OrderEventItem';

export interface OrderEventService {
    condition: Condition;
    mode: string; // TODO: Create an orderEventMode Enum
    description: string;
    serviceLevel: string; // TODO: Create an orderEventServiceLevel Enum
    loadNumbers?: number[];
    locations: OrderEventLocation[];
    items: OrderEventItem[];
}
