/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {OrderEventService} from './Order/OrderEventService';
import {Condition} from '../Condition';

export interface OrderDetail {
    orderNumber?: number;
    customerReferenceNumber?: string;
    billToReferenceNumber?: string;
    navisphereTrackingNumber?: string;
    navisphereTrackingLink?: string;
    condition?: Condition;
    services?: OrderEventService[];
}
