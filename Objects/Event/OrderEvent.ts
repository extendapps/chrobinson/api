/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ReferenceNumber} from '../ReferenceNumber';
import {MultiLoadEvent} from './BaseEvent';

export interface OrderEvent extends MultiLoadEvent {
    eventType: 'ORDER CREATED' | 'ORDER REJECTED' | 'ORDER UPDATED' | 'ORDER CANCELED' | 'ORDER COMPLETED';
    referenceNumbers?: ReferenceNumber[];
}

export interface OrderCreatedEvent extends OrderEvent {
    eventType: 'ORDER CREATED';
}

export interface OrderRejectedEvent extends OrderEvent {
    eventType: 'ORDER REJECTED';
}

export interface OrderUpdatedEvent extends OrderEvent {
    eventType: 'ORDER UPDATED';
}

export interface OrderCanceledEvent extends OrderEvent {
    eventType: 'ORDER CANCELED';
}

export interface OrderCompletedEvent extends OrderEvent {
    eventType: 'ORDER COMPLETED';
}
