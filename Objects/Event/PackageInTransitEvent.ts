/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {SingleLoadEvent} from './BaseEvent';
import {EventPackage} from './EventPackage';
import {InTransitAddress} from './InTransit/InTransitAddress';

export interface PackageInTransitEvent extends SingleLoadEvent {
    eventType: 'PACKAGE IN TRANSIT';
    location: InTransitAddress;
    packages: EventPackage[];
}
