/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {SingleLoadEvent} from './BaseEvent';
import {EventPackage} from './EventPackage';
import {EventLocation} from './EventLocation';

export interface PackageStopEvent extends SingleLoadEvent {
    eventType: 'PACKAGE PICKED UP' | 'PACKAGE DELIVERED';
    location: EventLocation;
    packages: EventPackage[];
}

export interface PackagePickedUpEvent extends PackageStopEvent {
    eventType: 'PACKAGE PICKED UP';
}

export interface PackageDeliveredEvent extends PackageStopEvent {
    eventType: 'PACKAGE DELIVERED';
}
