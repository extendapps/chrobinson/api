/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {SingleLoadEvent} from './BaseEvent';
import {EventLocation} from './EventLocation';
import {EventItem} from './EventItem';
import {EventLoadPackage} from './EventLoadPackage';

export interface StopEvent extends SingleLoadEvent {
    eventType: 'CARRIER ARRIVED' | 'CARRIER DEPARTED' | 'LOAD PICKED UP' | 'LOAD DELIVERED';
    lateReasonCode: string;  // TODO: Implement lateCodeEnum
    proofOfDelivery: string;
    location: EventLocation;
    items: EventItem[];
    packages: EventLoadPackage[];
}

export interface CarrierArrivedEvent extends StopEvent {
    eventType: 'CARRIER ARRIVED';
}

export interface CarrierDepartedEvent extends StopEvent {
    eventType: 'CARRIER DEPARTED';
}

export interface LoadPickedUpEvent extends StopEvent {
    eventType: 'LOAD PICKED UP';
}

export interface LoadDeliveredEvent extends StopEvent {
    eventType: 'LOAD DELIVERED';
}
