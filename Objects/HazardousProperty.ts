/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface HazardousProperty {
    description: string;
    technicalName?: string;
    unNumber?: string;
    classCode?: string;
    packagingGroup?: 'I' | 'II' | 'III' | string;
    emergencyPhone?: string;
    authCode?: 'C' | 'D' | 'I';
    isPlacarded?: boolean;
    isReportableQuantity?: boolean;
    isPrimaryCommodity?: boolean;
    isNOSCommodity?: boolean;
}
