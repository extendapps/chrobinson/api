/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type InvoiceMethod = 'PP' | 'CL' | 'TP' | 'PC' | 'CC';
