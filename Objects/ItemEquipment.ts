/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {LoadingType} from './LoadingType';

export interface ItemEquipment {
    itemEquipmentCode?: string;
    maxTrailerAge?: number;
    tarpTypeCode?: '10' | '2' | '4' | '6' | '8' | 'MESH' | 'NO |TARP' | 'SMOKE' | 'TOP';
    loadingTypeCode?: LoadingType;
    requiredStrapsCount?: number;
    requiredChainsCount?: number;
    coilRacks?: number;
    locksBars?: number;
    isOwOd?: boolean;
    safetyEquipmentRequired?: boolean;
}
