/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type LinearUnit = 'Inches' | 'Feet' | 'Centimeters' | 'Meters';
