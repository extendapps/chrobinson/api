/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type LoadingType = 'CRANE' | 'REAR' | 'SIDE' | 'REAR/SIDE' | 'TOP/SIDE' | 'DOCK' | 'STRING' | 'STRING/TOW';
