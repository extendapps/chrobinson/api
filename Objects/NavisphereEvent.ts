/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {OrderCanceledEvent, OrderCompletedEvent, OrderCreatedEvent, OrderRejectedEvent, OrderUpdatedEvent} from './Event/OrderEvent';
import {LoadBookedEvent, LoadCancelledEvent, LoadCreatedEvent} from './Event/LoadEvent';
import {AppointmentUpdatedEvent} from './Event/AppointmentEvent';
import {CarrierArrivedEvent, CarrierDepartedEvent, LoadDeliveredEvent, LoadPickedUpEvent} from './Event/StopEvent';
import {InTransitEvent} from './Event/InTransitEvent';
import {PackageInTransitEvent} from './Event/PackageInTransitEvent';
import {PackageDeliveredEvent, PackagePickedUpEvent} from './Event/PackageStopEvent';

export interface NavisphereEvent {
    time: string; // 2021-01-28T08:14:11.807Z
    customer: string;
    eventTime: string; // 2021-01-28T08:14:11.807Z
    customerReferenceNumber: string;
    billToReferenceNumber: string;
    event: OrderCreatedEvent | OrderRejectedEvent | OrderUpdatedEvent | OrderCanceledEvent | OrderCompletedEvent | LoadCreatedEvent | LoadBookedEvent | LoadCancelledEvent | CarrierArrivedEvent | CarrierDepartedEvent | LoadPickedUpEvent | LoadDeliveredEvent | AppointmentUpdatedEvent | InTransitEvent | PackageInTransitEvent | PackagePickedUpEvent | PackageDeliveredEvent;
}
