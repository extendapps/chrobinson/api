/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {OrderCustomer} from './Order/OrderCustomer';
import {ReferenceNumber} from './ReferenceNumber';
import {OrderService} from './Order/OrderService';
import {OrderBillTo} from './Order/OrderBillTo';
import {MeasurementSystem} from './MeasurementSystem';

export interface Order {
    orderNumber?: number; // This would only be used on an Order Update.
    customers: OrderCustomer[];
    billTos: OrderBillTo[];
    services: OrderService[];
    measurementSystem?: MeasurementSystem;
    referenceNumbers?: ReferenceNumber[];
    notes?: string;
}
