/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface OrderAddress {
    address1: string;
    address2?: string;
    address3?: string;
    address4?: string;
    city: string;
    county?: string;
    lat?: number;
    long?: number;
    stateProvinceCode: string;
    country: string;
    postalCode: string;
}
