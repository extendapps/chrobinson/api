/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {Charge} from '../Charge';
import {Contact} from '../Contact';
import {ReferenceNumber} from '../ReferenceNumber';
import {FreightTerm} from '../FreightTerm';
import {CurrencyCode} from '../CurrencyCode';

export interface OrderBillTo {
    customerCode: string;
    charges?: Charge[];
    currencyCode: CurrencyCode;
    freightTerms?: FreightTerm;
    contacts?: Contact[];
    referenceNumbers?: ReferenceNumber[];
}
