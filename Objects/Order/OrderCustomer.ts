/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {Contact} from '../Contact';
import {ReferenceNumber} from '../ReferenceNumber';

export interface OrderCustomer {
    customerCode: string;
    contacts?: Contact[];
    referenceNumbers?: ReferenceNumber[];
}
