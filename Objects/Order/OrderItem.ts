/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {ReferenceNumber} from '../ReferenceNumber';
import {Relationships} from '../Relationships';
import {ItemEquipment} from '../ItemEquipment';
import {HazardousProperty} from '../HazardousProperty';
import {FreightClass} from '../FreightClass';
import {PackagingCode} from '../PackagingCode';
import {WeightUnit} from '../WeightUnit';
import {LinearUnit} from '../LinearUnit';
import {VolumeUnit} from '../VolumeUnit';
import {TemperatureType} from '../TemperatureType';
import {TemperatureUnit} from '../TemperatureUnit';

export interface OrderItem {
    actualWeight?: number;
    actualQuantity?: string;
    packagingType: PackagingCode;
    quantity?: number;
    description?: string;
    productCode?: string;
    freightClass?: FreightClass;
    weight?: number;
    weightUnitOfMeasure?: WeightUnit;
    packagingLength?: number;
    packagingWidth?: number;
    packagingHeight?: number;
    packagingUnitOfMeasure?: LinearUnit;
    relationships?: Relationships;
    equipmentLength?: number;
    equipmentHeight?: number;
    equipmentWidth?: number;
    equipmentUnitOfMeasure?: LinearUnit;
    totalTrailerFeetUsed?: number;
    itemEquipment?: ItemEquipment;
    packagingVolume?: number;
    packagingVolumeUnitOfMeasure?: VolumeUnit;
    insuranceValue?: number;
    pallets?: number;
    palletSpaces?: number;
    nationalMotorFreightClass?: string;
    hazardousProperties?: HazardousProperty;
    upcNumber?: string;
    skuNumber?: string;
    pluNumber?: string;
    isStackable?: boolean;
    isTemperatureSensitive?: boolean;
    temperatureType?: TemperatureType;
    temperatureUnit?: TemperatureUnit;
    requiredTemperatureHigh?: number;
    requiredTemperatureLow?: number;
    notes?: string;
    referenceNumbers?: ReferenceNumber[];
}
