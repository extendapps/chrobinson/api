/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ReferenceNumber} from '../ReferenceNumber';
import {SpecialRequirement} from '../SpecialRequirement';
import {OrderAddress} from './OrderAddress';
import {BlindLocation} from '../BlindLocation';
import {Contact} from '../Contact';
import {LoadingType} from '../LoadingType';

export interface OrderLocation {
    customerLocationId?: string;
    type: 'origin' | 'destination';
    name?: string;
    address?: OrderAddress;
    bindLocation?: BlindLocation;
    openDateTime?: string;
    closeDateTime?: string;
    arrivalDate?: string;
    departureDate?: string;
    scheduledOpenDateTime?: string;
    scheduledCloseDateTime?: string;
    readyAvailableDateTime?: string;
    rsDateTime?: string;
    specialRequirement?: SpecialRequirement;
    scalesRequired?: boolean;
    hasDropTrailer?: boolean;
    loadingTypeCode?: LoadingType;
    specialInstructions?: string;
    contacts?: Contact[];
    referenceNumbers?: ReferenceNumber[];
}
