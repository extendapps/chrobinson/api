/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {PackagingCode} from '../PackagingCode';
import {LinearUnit} from '../LinearUnit';
import {VolumeUnit} from '../VolumeUnit';

export interface OrderPackage {
    packageType?: PackagingCode;
    parcelPackageType?: string;
    weight?: number;
    length?: number;
    width?: number;
    height?: number;
    packageUnitOfMeasure?: LinearUnit;
    volume?: number;
    packageVolumeUnitOfMeasure?: VolumeUnit;
    weightLb?: number;
    declaredValue?: number;
    codAmount?: number;
    trackingNumber?: string;
    notes?: string;
}
