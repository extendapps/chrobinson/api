/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {ReferenceNumber} from '../ReferenceNumber';
import {OrderItem} from './OrderItem';
import {OrderPackage} from './OrderPackage';
import {CollectOnDelivered} from '../CollectOnDelivered';
import {OrderLocation} from './OrderLocation';
import {SellMode} from '../SellMode';

export interface OrderService {
    dateCalculationRule?: 'promisedShipDate' | 'promisedDeliveryDate';
    serviceLevel?: string;
    suggestedScac?: string;
    isCritical?: boolean;
    isDangerous?: boolean;
    isHot?: boolean;
    isHighValue?: boolean;
    isPartial?: boolean;
    suggestedCarrierPartyCode?: string;
    sellMode?: SellMode;
    locations: OrderLocation[];
    items: OrderItem[];
    packages?: OrderPackage[];
    collectOnDelivered?: CollectOnDelivered;
    referenceNumbers?: ReferenceNumber[];
}
