/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Rate} from './Rate';
import {Transit} from './Transit';
import {OrderCustomer} from './Order/OrderCustomer';
import {Carrier} from './Carrier';
import {CargoLiability} from './CargoLiability';
import {TransportModeType} from './TransportMode';
import {EquipmentType} from './EquipmentType';

export interface Quote {
    quoteId: number;
    carrier?: Carrier;
    customer: OrderCustomer;
    totalCharge: number;
    totalFreightCharge: number;
    totalAccessorialCharge: number;
    quoteExpirationDate?: string;
    transit: Transit;
    rates: Rate[];
    transportModeType: TransportModeType;
    equipmentType: EquipmentType;
    cargoLiability?: CargoLiability;
    distance: number;
}
