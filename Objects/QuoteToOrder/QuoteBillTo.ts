/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {Contact} from '../Contact';
import {ReferenceNumber} from '../ReferenceNumber';
import {CurrencyCode} from '../CurrencyCode';

export interface QuoteBillTo {
    currencyCode: CurrencyCode;
    contacts?: Contact[];
    referenceNumbers?: ReferenceNumber[];
}
