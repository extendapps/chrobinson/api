/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ReferenceNumber} from '../ReferenceNumber';
import {OrderAddress} from '../Order/OrderAddress';

export interface QuoteLocation {
    customerLocationId?: string;
    locationName?: string;
    address?: OrderAddress;
    phone?: string;
    emailAddress?: string;
    contactName?: string;
    requestedShipDateOpen: string;
    requestedShipDateClose: string;
    specialInstructions?: string;
    referenceNumbers?: ReferenceNumber[];
}
