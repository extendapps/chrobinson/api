/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {ReferenceNumber} from '../ReferenceNumber';

export interface QuoteService {
    referenceNumbers?: ReferenceNumber[];
    suggestedScac?: string;
    suggestedCarrierPartyCode?: string;
}
