/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {OrderCustomer} from '../Order/OrderCustomer';
import {QuoteBillTo} from './QuoteBillTo';
import {QuoteService} from './QuoteService';
import {QuoteLocation} from './QuoteLocation';
import {ReferenceNumber} from '../ReferenceNumber';

export interface QuoteToOrder {
    quoteId: number;
    customer?: OrderCustomer;
    billTo?: QuoteBillTo;
    service?: QuoteService;
    origin: QuoteLocation;
    destination: QuoteLocation;
    notes?: string;
    referenceNumbers?: ReferenceNumber[];
}
