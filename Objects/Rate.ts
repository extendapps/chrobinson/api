/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {CurrencyCode} from './CurrencyCode';

export interface Rate {
    rateId: number;
    totalRate: number;
    unitRate: number;
    quantity: number;
    rateCode: string;
    rateCodeValue: string;
    currencyCode: CurrencyCode;
    isOptional: boolean;
}
