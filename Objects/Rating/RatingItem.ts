/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {ReferenceNumber} from '../ReferenceNumber';
import {FreightClass} from '../FreightClass';
import {PackagingCode} from '../PackagingCode';
import {WeightUnit} from '../WeightUnit';
import {LinearUnit} from '../LinearUnit';
import {VolumeUnit} from '../VolumeUnit';
import {TemperatureType} from '../TemperatureType';
import {TemperatureUnit} from '../TemperatureUnit';

export interface RatingItem {
    description?: string;
    freightClass?: FreightClass;
    actualWeight: number;
    weightUnit: WeightUnit;
    length?: number;
    width?: number;
    height?: number;
    linearUnit?: LinearUnit;
    pallets?: number;
    pieces?: number;
    palletSpaces?: number;
    volume?: number;
    volumeUnit?: VolumeUnit;
    density?: number;
    linearSpace?: number;
    declaredValue?: number;
    packagingCode?: PackagingCode;
    productCode?: string;
    productName?: string;
    temperatureSensitive?: TemperatureType;
    temperatureUnit?: TemperatureUnit;
    requiredTemperatureHigh?: number;
    requiredTemperatureLow?: number;
    unitsPerPallet?: number;
    unitWeight?: number;
    unitVolume?: number;
    isStackable?: boolean;
    isOverWeightOverDimensional?: boolean;
    isUsedGood?: boolean;
    isHazardous?: boolean;
    hazardousDescription?: string;
    hazardousEmergencyPhone?: string;
    nmfc?: string;
    upc?: string;
    sku?: string;
    plu?: string;
    referenceNumbers?: ReferenceNumber[];
}
