/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ReferenceNumber} from '../ReferenceNumber';
import {SpecialRequirement} from '../SpecialRequirement';

export interface RatingLocation {
    locationName?: string;
    address1?: string;
    address2?: string;
    address3?: string;
    city?: string;
    stateProvinceCode?: string;
    countryCode: string;
    postalCode: string;
    requestedOpenDateTime?: string;
    requestedCloseDateTime?: string;
    latitude?: string | number;
    longitude?: string | number;
    specialRequirement?: SpecialRequirement;
    isPort?: boolean | string;
    unLocode?: string;
    iata?: string;
    customerLocationId?: string;
    referenceNumbers?: ReferenceNumber[];
}
