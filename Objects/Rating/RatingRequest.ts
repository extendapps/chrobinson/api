/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {RatingItem} from './RatingItem';
import {RatingLocation} from './RatingLocation';
import {TransportMode} from '../TransportMode';
import {ReferenceNumber} from '../ReferenceNumber';

export interface RatingRequest {
    transactionId?: string;
    items: RatingItem[];
    origin: RatingLocation;
    destination: RatingLocation;
    shipDate: string;
    platform: string;
    customerCode: string;
    transportModes: TransportMode[];
    referenceNumbers?: ReferenceNumber[];
    optionalAccessorials?: string[];
}
