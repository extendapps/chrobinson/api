/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface ReferenceNumber {
    type: 'POLineNum' | 'ORD' | 'QUOTEID' | 'InfoFrom' | 'CustomerItemIdentifier1' | 'OrderItemId' | 'OrderID' | 'ORDTRK' | 'PRO' | 'SHID' | 'MBOL' | 'PO' | 'BOL' | 'CRID' | 'CUSTPO' | 'DEL' | 'JNO' | 'PU' | 'CON' | 'APPT';
    value: string;
}
