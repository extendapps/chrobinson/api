/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Relationships {
    innerPackQuantity?: number;
    innerPackUomCode?: string;
    masterPackQuantity?: number;
}
