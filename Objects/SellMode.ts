/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type SellMode = 'LTL' | 'TL' | 'LTLFlatbed' | 'TLFlatbed' | 'IMDL' | 'Bulk' | 'Consolidated' | 'DomesticAir' | 'SmallParcel' | 'Ocean' | 'Tautliner';
