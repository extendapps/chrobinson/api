/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface SpecialRequirement {
    liftGate?: boolean | string;
    insidePickup?: boolean | string;
    insideDelivery?: boolean | string;
    residentialNonCommercial?: boolean | string;
    limitedAccess?: boolean | string;
    tradeShoworConvention?: boolean | string;
    constructionSite?: boolean | string;
    dropOffAtCarrierTerminal?: boolean | string;
    pickupAtCarrierTerminal?: boolean | string;
    appointmentNotification?: boolean | string;
    sortAndSegregate?: boolean | string;
}
