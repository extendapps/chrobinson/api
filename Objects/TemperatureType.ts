/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type TemperatureType = 'Dry' | 'Fresh' | 'Frozen' | 'Chilled' | 'DeepFrozen' | 'Refrigerated' | 'Protect' | 'Custom' | 'Produce' | 'Mixed' | 'Empty';
