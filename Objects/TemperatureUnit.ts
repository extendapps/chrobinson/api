/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type TemperatureUnit = 'Fahrenheit' | 'Celsius' | 'Empty';
