/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Transit {
    minimumTransitDays: number;
    maximumTransitDays: number;
    originService?: string;
    destinationService?: string;
}
