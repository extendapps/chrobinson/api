/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {Equipment} from './Equipment';

export interface TransportMode {
    mode: TransportModeType;
    equipments?: Equipment[];
}

export type TransportModeType = 'LTL' | 'TL' | 'Air' | 'Ocean' | 'Bulk' | 'Consol' | 'Flatbed';
