/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export type WeightUnit = 'Pounds' | 'Kilograms';
