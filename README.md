# C.H.Robinson API NetSuite module

A distributable module for `C. H. Robinson` connectivity using NetSuite API's 

See https://developer.chrobinson.com/api-reference for more details

## Installation Instructions

`npm install --save-dev @extendapps/chrobinsonapi`

Once installed, you'll need to make this module **relative** to you code in order to confirm to NetSuite's **relative** imports

### Dev-Ops Suggestions

Add the following commands to you "scripts" section in your package.json
> "chrapi-setup": "cd Source; ln -s ../node_modules/\\@extendapps/chrobinsonapi/"

> "chrapi-deploy": "cp Source/chrobinsonapi/chrobinsonapi.js FileCabinet/PATH_TO_ROOT/",

### Update .gitignore

Add the following to you .gitignore file

> /Source/chrobinsonapi/

### Create a symbolic link (Manual)

* Navigate to the root folder of you TypeScript source
* `ln -s ../node_modules/\@extendapps/chrobinsonapi/`

This will create a **reference** to the chrobinsonapi folder and allow you to use the appropriate import statement

`import {CHRobinsonAPI} from './chrobinsonapi';`

## Usage

### More Details

[Techincal Details](docs/README.md)
