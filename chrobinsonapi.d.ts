/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * performioapi.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
import { RatingRequestOptions } from './Interfaces/RatingRequest';
import { OrderCreateOptions, OrderDeleteOptions, OrderUpdateOptions } from './Interfaces/Order';
import { EventRetrievalOptions } from './Interfaces/Event';
import { RefreshAccessTokenOptions } from './Interfaces/Base';
import { QuoteToOrderOptions } from './Interfaces/QuoteToOrder';
import { GeneratedBOLRetrievalOptions } from './Interfaces/Documents';
export declare class CHRobinsonAPI {
    private readonly secretScriptId;
    private readonly isProduction;
    private static readonly CACHE_NAME;
    private static readonly DOMAIN;
    private loggingEnabled;
    private readonly domain;
    constructor(secretScriptId?: string, isProduction?: boolean);
    get Mappings(): Mapping[];
    set EnableLogging(toggle: boolean);
    private get AccessToken();
    /** @internal */
    private get AccessTokenDetails();
    refreshAccessToken(options: RefreshAccessTokenOptions): void;
    generatedBOLRetrieval(options: GeneratedBOLRetrievalOptions): void;
    eventRetrieval(options: EventRetrievalOptions): void;
    ratingRequest(options: RatingRequestOptions): void;
    updateOrder(options: OrderUpdateOptions): void;
    createOrder(options: OrderCreateOptions): void;
    quoteToOrder(options: QuoteToOrderOptions): void;
    deleteOrder(options: OrderDeleteOptions): void;
    private log;
    /** @internal */
    private makeGETRequest;
    /** @internal */
    private makeDELETERequest;
    /** @internal */
    private makePOSTRequest;
    /** @internal */
    private makePUTRequest;
    /** @internal */
    private makeRequest;
}
export interface Mapping {
    key: string;
    value: string;
    customerName: string;
    claimCreatedUtc: Date;
}
export interface ErrorMessage {
    message: string;
    path: string[];
    type: string;
    context: Context;
}
export interface Context {
    child?: string;
    value?: {
        type: string;
        value: string;
    }[];
    key: string;
    label: string;
}
