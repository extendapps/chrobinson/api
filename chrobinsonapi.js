/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * performioapi.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
define(["require", "exports", "N/https", "N/log", "N/cache", "N/crypto", "N/encode", "N/config"], function (require, exports, https, log, cache, crypto, encode, config) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.CHRobinsonAPI = void 0;
    // noinspection JSUnusedGlobalSymbols
    var CHRobinsonAPI = exports.CHRobinsonAPI = /** @class */ (function () {
        function CHRobinsonAPI(secretScriptId, isProduction) {
            if (secretScriptId === void 0) { secretScriptId = 'custsecret_chr_secret'; }
            if (isProduction === void 0) { isProduction = false; }
            this.secretScriptId = secretScriptId;
            this.isProduction = isProduction;
            this.loggingEnabled = false;
            this.domain = isProduction ? CHRobinsonAPI.DOMAIN.Production : CHRobinsonAPI.DOMAIN.Sandbox;
        }
        Object.defineProperty(CHRobinsonAPI.prototype, "Mappings", {
            get: function () {
                return this.AccessTokenDetails.mappings;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(CHRobinsonAPI.prototype, "EnableLogging", {
            set: function (toggle) {
                this.loggingEnabled = toggle;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(CHRobinsonAPI.prototype, "AccessToken", {
            get: function () {
                return this.AccessTokenDetails.accessToken;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(CHRobinsonAPI.prototype, "AccessTokenDetails", {
            /** @internal */
            get: function () {
                var _this = this;
                var cachedAccessTokenResponse = {
                    accessToken: null,
                    mappings: []
                };
                var accessTokenCache = cache.getCache({
                    name: CHRobinsonAPI.CACHE_NAME,
                    scope: cache.Scope.PROTECTED
                });
                var accessTokenResponseString = accessTokenCache.get({ key: this.secretScriptId });
                if (accessTokenResponseString && accessTokenResponseString.length > 5) {
                    cachedAccessTokenResponse = JSON.parse(accessTokenResponseString);
                    this.log('audit', 'AccessTokenDetails', 'Using cached Access Token');
                }
                else {
                    this.log('audit', 'AccessTokenDetails', 'Fetching Access Token');
                    var companyInfo = config.load({
                        type: config.Type.COMPANY_INFORMATION
                    });
                    var timestamp = new Date().valueOf();
                    var companyId = companyInfo.getValue({ fieldId: 'companyid' });
                    var query = "timestamp=".concat(timestamp, "&accountId=").concat(companyId);
                    this.log('audit', 'AccessTokenDetails - query', query);
                    var secretKey = crypto.createSecretKey({
                        secret: this.secretScriptId,
                        encoding: encode.Encoding.UTF_8
                    });
                    var hmacSha256 = crypto.createHmac({
                        algorithm: crypto.HashAlg.SHA256,
                        key: secretKey
                    });
                    hmacSha256.update({
                        input: query,
                        inputEncoding: encode.Encoding.UTF_8
                    });
                    var digestSha256 = hmacSha256.digest({
                        outputEncoding: encode.Encoding.HEX
                    });
                    this.makeRequest({
                        method: https.Method.GET,
                        isTokenRequest: true,
                        version: 'v1',
                        resource: "integrations/settings?".concat(query),
                        headers: {
                            'Content-Type': 'application/json',
                            'x-signature': digestSha256.toLowerCase()
                        },
                        OK: function (clientResponse) {
                            accessTokenCache.put({
                                key: _this.secretScriptId,
                                value: clientResponse.body,
                                ttl: 3600 // 1 hour
                            });
                            cachedAccessTokenResponse = JSON.parse(clientResponse.body);
                        },
                        Failed: function (clientResponse) {
                            _this.log('error', 'Generate Token - Failed', clientResponse);
                        }
                    });
                    if (cachedAccessTokenResponse.accessToken === null) {
                        accessTokenCache.remove({ key: this.secretScriptId });
                    }
                }
                return cachedAccessTokenResponse;
            },
            enumerable: false,
            configurable: true
        });
        CHRobinsonAPI.prototype.refreshAccessToken = function (options) {
            var accessTokenCache = cache.getCache({
                name: CHRobinsonAPI.CACHE_NAME,
                scope: cache.Scope.PROTECTED
            });
            accessTokenCache.remove({ key: this.secretScriptId });
            var newAccessToken = this.AccessToken;
            if (newAccessToken === null) {
                var failed = {
                    body: 'Failed to refresh Access Token.  Check logs for further details.',
                    code: 200,
                    headers: []
                };
                options.Failed(failed);
            }
            else {
                options.Refreshed();
            }
        };
        CHRobinsonAPI.prototype.generatedBOLRetrieval = function (options) {
            var _this = this;
            this.makeGETRequest({
                resource: "documents/generateBols?loadNumber=".concat(options.loadNumber, "&customerCode=").concat(options.customerCode, "&orderNumber=").concat(options.orderNumber),
                version: 'v1',
                Created: function (clientResponse) {
                    options.Created(clientResponse);
                },
                BadRequest: function (errorMessages) {
                    if (options.BadRequest) {
                        options.BadRequest(errorMessages);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        _this.log('error', 'eventRetrieval', clientResponse);
                    }
                }
            });
        };
        CHRobinsonAPI.prototype.eventRetrieval = function (options) {
            var _this = this;
            var params = [];
            if (options.from) {
                params.push("from=".concat(options.from));
            }
            if (options.to) {
                params.push("to=".concat(options.to));
            }
            if (options.loadNumber) {
                params.push("loadNumber=".concat(options.loadNumber));
            }
            if (options.orderNumber) {
                params.push("orderNumber=".concat(options.orderNumber));
            }
            if (options.billToReferenceNumber) {
                params.push("billToReferenceNumber=".concat(options.billToReferenceNumber));
            }
            if (options.customerReferenceNumber) {
                params.push("customerReferenceNumber=".concat(options.customerReferenceNumber));
            }
            if (options.navisphereTrackingNumber) {
                params.push("navisphereTrackingNumber=".concat(options.navisphereTrackingNumber));
            }
            if (options.eventType) {
                params.push("eventType=".concat(options.eventType));
            }
            if (options.skip) {
                params.push("skip=".concat(options.skip));
            }
            if (options.take) {
                params.push("take=".concat(options.take));
            }
            var resource = 'events';
            if (params.length > 0) {
                resource += "?".concat(params.join('&'), "}");
            }
            this.makeGETRequest({
                resource: resource,
                version: 'v2',
                headers: {
                    'Content-Type': 'application/json',
                },
                OK: function (clientResponse) {
                    var eventRetrievalResponse = JSON.parse(clientResponse.body);
                    options.OK(eventRetrievalResponse.results);
                },
                BadRequest: function (errorMessages) {
                    if (options.BadRequest) {
                        options.BadRequest(errorMessages);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        _this.log('error', 'eventRetrieval', clientResponse);
                    }
                }
            });
        };
        CHRobinsonAPI.prototype.ratingRequest = function (options) {
            var _this = this;
            this.makePOSTRequest({
                resource: 'quotes',
                version: 'v1',
                payload: options.request,
                headers: {
                    'Content-Type': 'application/json',
                },
                Created: function (clientResponse) {
                    var ratingRequestResponse = JSON.parse(clientResponse.body);
                    options.Created(ratingRequestResponse.quoteSummaries);
                },
                BadRequest: function (errorMessages) {
                    if (options.BadRequest) {
                        options.BadRequest(errorMessages);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        _this.log('error', 'ratingRequest', clientResponse);
                    }
                }
            });
        };
        CHRobinsonAPI.prototype.updateOrder = function (options) {
            var _this = this;
            this.makePUTRequest({
                resource: 'orders',
                version: 'v1',
                payload: options.order,
                headers: {
                    'Content-Type': 'application/json',
                },
                OK: function (clientResponse) {
                    _this.log('audit', 'updateOrder', clientResponse);
                    options.OK();
                },
                BadRequest: function (errorMessages) {
                    if (options.BadRequest) {
                        options.BadRequest(errorMessages);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        _this.log('error', 'createOrder', clientResponse);
                    }
                }
            });
        };
        CHRobinsonAPI.prototype.createOrder = function (options) {
            var _this = this;
            this.makePOSTRequest({
                resource: 'orders',
                version: 'v1',
                payload: options.order,
                headers: {
                    'Content-Type': 'application/json',
                },
                Created: function (clientResponse) {
                    var orderCreateResponse = JSON.parse(clientResponse.body);
                    options.Created(orderCreateResponse.orderNumber);
                },
                BadRequest: function (errorMessages) {
                    if (options.BadRequest) {
                        options.BadRequest(errorMessages);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        _this.log('error', 'createOrder', clientResponse);
                    }
                }
            });
        };
        CHRobinsonAPI.prototype.quoteToOrder = function (options) {
            var _this = this;
            this.makePOSTRequest({
                resource: 'orders/quotes',
                version: 'v1',
                payload: options.quoteToOrders,
                headers: {
                    'Content-Type': 'application/json',
                },
                Created: function (clientResponse) {
                    var quoteOrders = JSON.parse(clientResponse.body);
                    options.Created(quoteOrders);
                },
                BadRequest: function (errorMessages) {
                    if (options.BadRequest) {
                        options.BadRequest(errorMessages);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        _this.log('error', 'quoteToOrder', clientResponse);
                    }
                }
            });
        };
        CHRobinsonAPI.prototype.deleteOrder = function (options) {
            var _this = this;
            this.makeDELETERequest({
                version: 'v1',
                resource: "orders/".concat(options.orderNumber),
                headers: {
                    'Content-Type': 'application/json',
                },
                OK: function () {
                    options.Deleted();
                },
                BadRequest: function (errorMessages) {
                    if (options.BadRequest) {
                        options.BadRequest(errorMessages);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        _this.log('error', 'deleteOrder', clientResponse);
                    }
                }
            });
        };
        CHRobinsonAPI.prototype.log = function (type, title, message) {
            if (this.loggingEnabled) {
                title = "CHRobinsonAPI: ".concat(title);
                switch (type) {
                    case 'audit':
                        log.audit(title, message);
                        break;
                    case 'error':
                        log.error(title, message);
                        break;
                    case 'emergency':
                        log.emergency(title, message);
                        break;
                    case 'debug':
                        log.debug(title, message);
                        break;
                    default:
                        break;
                }
            }
        };
        /** @internal */
        CHRobinsonAPI.prototype.makeGETRequest = function (options) {
            var makeRequestOptions = options;
            makeRequestOptions.method = https.Method.GET;
            this.makeRequest(makeRequestOptions);
        };
        /** @internal */
        CHRobinsonAPI.prototype.makeDELETERequest = function (options) {
            var makeRequestOptions = options;
            makeRequestOptions.method = https.Method.DELETE;
            this.makeRequest(makeRequestOptions);
        };
        /** @internal */
        CHRobinsonAPI.prototype.makePOSTRequest = function (options) {
            var makeRequestOptions = options;
            makeRequestOptions.method = https.Method.POST;
            this.makeRequest(makeRequestOptions);
        };
        /** @internal */
        CHRobinsonAPI.prototype.makePUTRequest = function (options) {
            var makeRequestOptions = options;
            makeRequestOptions.method = https.Method.PUT;
            this.makeRequest(makeRequestOptions);
        };
        /** @internal */
        CHRobinsonAPI.prototype.makeRequest = function (options) {
            var clientResponse = null;
            var requestOptions = {
                method: options.method,
                url: "https://".concat(this.domain, "/").concat(options.version, "/").concat(options.resource)
            };
            requestOptions.headers = options.headers || {};
            if (!options.isTokenRequest) {
                requestOptions.headers.Authorization = "Bearer ".concat(this.AccessToken);
            }
            if (options.payload) {
                requestOptions.body = JSON.stringify(options.payload);
            }
            try {
                this.log('audit', 'makeRequest - requestOptions', requestOptions);
                clientResponse = https.request(requestOptions);
                this.log('audit', 'makeRequest - clientResponse', clientResponse);
                switch (clientResponse.code) {
                    case 200:
                        if (options.OK) {
                            options.OK(clientResponse);
                        }
                        break;
                    case 201:
                        if (options.Created) {
                            options.Created(clientResponse);
                        }
                        break;
                    case 202:
                        if (options.Accepted) {
                            options.Accepted(clientResponse);
                        }
                        break;
                    case 400:
                        if (options.BadRequest && clientResponse.body) {
                            options.BadRequest(JSON.parse(clientResponse.body));
                        }
                        else {
                            options.Failed(clientResponse);
                        }
                        break;
                    case 401:
                        if (options.Unauthorized) {
                            options.Unauthorized(clientResponse);
                        }
                        else {
                            options.Failed(clientResponse);
                        }
                        break;
                    case 403:
                        if (options.Forbidden) {
                            options.Forbidden(clientResponse);
                        }
                        else {
                            options.Failed(clientResponse);
                        }
                        break;
                    case 404:
                        if (options.NotFound) {
                            options.NotFound(clientResponse);
                        }
                        else {
                            options.Failed(clientResponse);
                        }
                        break;
                    case 500:
                        if (options.ServerError) {
                            options.ServerError(clientResponse);
                        }
                        else {
                            options.Failed(clientResponse);
                        }
                        break;
                    default:
                        options.Failed(clientResponse);
                        break;
                }
            }
            catch (e) {
                options.retryCount = options.retryCount || 0;
                if (options.retryCount <= 5) {
                    options.retryCount = options.retryCount + 1;
                    this.makeRequest(options);
                }
                else {
                    this.log('error', 'makeRequest', e);
                    options.Failed(clientResponse);
                }
            }
        };
        CHRobinsonAPI.CACHE_NAME = 'CHR_AccessToken';
        CHRobinsonAPI.DOMAIN = {
            Production: 'api.navisphere.com',
            Sandbox: 'sandbox-api.navisphere.com'
        };
        return CHRobinsonAPI;
    }());
});
