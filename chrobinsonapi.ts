/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * performioapi.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import * as https from 'N/https';
import * as log from 'N/log';
import * as cache from 'N/cache';
import * as crypto from 'N/crypto';
import * as encode from 'N/encode';
import * as config from 'N/config';
import {RatingRequestOptions, RatingRequestResponse} from './Interfaces/RatingRequest';
import {OrderCreateOptions, OrderCreateResponse, OrderDeleteOptions, OrderUpdateOptions} from './Interfaces/Order';
import {EventRetrievalOptions, EventRetrievalResponse} from './Interfaces/Event';
import {RefreshAccessTokenOptions} from './Interfaces/Base';
import * as record from 'N/record';
import {QuoteOrder, QuoteToOrderOptions} from './Interfaces/QuoteToOrder';
import {GeneratedBOLRetrievalOptions} from './Interfaces/Documents';

// noinspection JSUnusedGlobalSymbols
export class CHRobinsonAPI {
    private static readonly CACHE_NAME = 'CHR_AccessToken';
    private static readonly DOMAIN = {
        Production: 'api.navisphere.com',
        Sandbox: 'sandbox-api.navisphere.com'
    };
    private loggingEnabled = false;
    private readonly domain: string;

    public constructor(private readonly secretScriptId = 'custsecret_chr_secret', private readonly isProduction: boolean = false) {
        this.domain = isProduction ? CHRobinsonAPI.DOMAIN.Production : CHRobinsonAPI.DOMAIN.Sandbox;
    }

    public get Mappings(): Mapping[] {
        return this.AccessTokenDetails.mappings;
    }

    public set EnableLogging(toggle: boolean) {
        this.loggingEnabled = toggle;
    }

    private get AccessToken(): string {
        return this.AccessTokenDetails.accessToken;
    }

    /** @internal */
    private get AccessTokenDetails(): AccessTokenResponse {
        let cachedAccessTokenResponse: AccessTokenResponse = {
            accessToken: null,
            mappings: []
        };

        const accessTokenCache: cache.Cache = cache.getCache({
            name: CHRobinsonAPI.CACHE_NAME,
            scope: cache.Scope.PROTECTED
        });
        const accessTokenResponseString: string = accessTokenCache.get({key: this.secretScriptId});

        if (accessTokenResponseString && accessTokenResponseString.length > 5) {
            cachedAccessTokenResponse = JSON.parse(accessTokenResponseString);
            this.log('audit', 'AccessTokenDetails', 'Using cached Access Token');
        } else {
            this.log('audit', 'AccessTokenDetails', 'Fetching Access Token');
            const companyInfo: record.Record = config.load({
                type: config.Type.COMPANY_INFORMATION
            });

            const timestamp: number = new Date().valueOf();
            const companyId: string = <string>companyInfo.getValue({fieldId: 'companyid'});
            const query = `timestamp=${timestamp}&accountId=${companyId}`;

            this.log('audit', 'AccessTokenDetails - query', query);

            const secretKey: crypto.SecretKey = crypto.createSecretKey({
                secret: this.secretScriptId,
                encoding: encode.Encoding.UTF_8
            });

            const hmacSha256: crypto.Hmac = crypto.createHmac({
                algorithm: crypto.HashAlg.SHA256,
                key: secretKey
            });

            hmacSha256.update({
                input: query,
                inputEncoding: encode.Encoding.UTF_8
            });

            const digestSha256: string = hmacSha256.digest({
                outputEncoding: encode.Encoding.HEX
            });

            this.makeRequest({
                method: https.Method.GET,
                isTokenRequest: true,
                version: 'v1',
                resource: `integrations/settings?${query}`,
                headers: {
                    'Content-Type': 'application/json',
                    'x-signature': digestSha256.toLowerCase()
                },
                OK: clientResponse => {
                    accessTokenCache.put({
                        key: this.secretScriptId,
                        value: clientResponse.body,
                        ttl: 3600 // 1 hour
                    });
                    cachedAccessTokenResponse = JSON.parse(clientResponse.body);
                },
                Failed: clientResponse => {
                    this.log('error', 'Generate Token - Failed', clientResponse);
                }
            });

            if (cachedAccessTokenResponse.accessToken === null) {
                accessTokenCache.remove({key: this.secretScriptId});
            }
        }

        return cachedAccessTokenResponse;
    }

    public refreshAccessToken(options: RefreshAccessTokenOptions): void {
        const accessTokenCache: cache.Cache = cache.getCache({
            name: CHRobinsonAPI.CACHE_NAME,
            scope: cache.Scope.PROTECTED
        });
        accessTokenCache.remove({key: this.secretScriptId});
        const newAccessToken = this.AccessToken;

        if (newAccessToken === null) {
            const failed: https.ClientResponse = {
                body: 'Failed to refresh Access Token.  Check logs for further details.',
                code: 200,
                headers: []
            };
            options.Failed(failed);
        } else {
            options.Refreshed();
        }
    }

    public generatedBOLRetrieval(options: GeneratedBOLRetrievalOptions): void {
        this.makeGETRequest({
            resource: `documents/generateBols?loadNumber=${options.loadNumber}&customerCode=${options.customerCode}&orderNumber=${options.orderNumber}`,
            version: 'v1',
            Created: clientResponse => {
                options.Created(clientResponse);
            },
            BadRequest: errorMessages => {
                if (options.BadRequest) {
                    options.BadRequest(errorMessages);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    this.log('error', 'eventRetrieval', clientResponse);
                }
            }
        });
    }

    public eventRetrieval(options: EventRetrievalOptions): void {
        const params: string[] = [];

        if (options.from) {
            params.push(`from=${options.from}`);
        }
        if (options.to) {
            params.push(`to=${options.to}`);
        }
        if (options.loadNumber) {
            params.push(`loadNumber=${options.loadNumber}`);
        }
        if (options.orderNumber) {
            params.push(`orderNumber=${options.orderNumber}`);
        }
        if (options.billToReferenceNumber) {
            params.push(`billToReferenceNumber=${options.billToReferenceNumber}`);
        }
        if (options.customerReferenceNumber) {
            params.push(`customerReferenceNumber=${options.customerReferenceNumber}`);
        }
        if (options.navisphereTrackingNumber) {
            params.push(`navisphereTrackingNumber=${options.navisphereTrackingNumber}`);
        }
        if (options.eventType) {
            params.push(`eventType=${options.eventType}`);
        }
        if (options.skip) {
            params.push(`skip=${options.skip}`);
        }
        if (options.take) {
            params.push(`take=${options.take}`);
        }

        let resource = 'events';
        if (params.length > 0) {
            resource += `?${params.join('&')}}`;
        }

        this.makeGETRequest({
            resource: resource,
            version: 'v2',
            headers: {
                'Content-Type': 'application/json',
            },
            OK: clientResponse => {
                const eventRetrievalResponse: EventRetrievalResponse = JSON.parse(clientResponse.body);
                options.OK(eventRetrievalResponse.results);
            },
            BadRequest: errorMessages => {
                if (options.BadRequest) {
                    options.BadRequest(errorMessages);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    this.log('error', 'eventRetrieval', clientResponse);
                }
            }
        });
    }

    public ratingRequest(options: RatingRequestOptions): void {
        this.makePOSTRequest({
            resource: 'quotes',
            version: 'v1',
            payload: options.request,
            headers: {
                'Content-Type': 'application/json',
            },
            Created: clientResponse => {
                const ratingRequestResponse: RatingRequestResponse = JSON.parse(clientResponse.body);
                options.Created(ratingRequestResponse.quoteSummaries);
            },
            BadRequest: errorMessages => {
                if (options.BadRequest) {
                    options.BadRequest(errorMessages);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    this.log('error', 'ratingRequest', clientResponse);
                }
            }
        });
    }

    public updateOrder(options: OrderUpdateOptions): void {
        this.makePUTRequest({
            resource: 'orders',
            version: 'v1',
            payload: options.order,
            headers: {
                'Content-Type': 'application/json',
            },
            OK: clientResponse => {
                this.log('audit', 'updateOrder', clientResponse);
                options.OK();
            },
            BadRequest: errorMessages => {
                if (options.BadRequest) {
                    options.BadRequest(errorMessages);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    this.log('error', 'updateOrder', clientResponse);
                }
            }
        });
    }

    public createOrder(options: OrderCreateOptions): void {
        this.makePOSTRequest({
            resource: 'orders',
            version: 'v1',
            payload: options.order,
            headers: {
                'Content-Type': 'application/json',
            },
            Created: clientResponse => {
                const orderCreateResponse: OrderCreateResponse = JSON.parse(clientResponse.body);
                options.Created(orderCreateResponse.orderNumber);
            },
            BadRequest: errorMessages => {
                if (options.BadRequest) {
                    options.BadRequest(errorMessages);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    this.log('error', 'createOrder', clientResponse);
                }
            }
        });
    }

    public quoteToOrder(options: QuoteToOrderOptions): void {
        this.makePOSTRequest({
            resource: 'orders/quotes',
            version: 'v1',
            payload: options.quoteToOrders,
            headers: {
                'Content-Type': 'application/json',
            },
            Created: clientResponse => {
                const quoteOrders: QuoteOrder[] = JSON.parse(clientResponse.body);
                options.Created(quoteOrders);
            },
            BadRequest: errorMessages => {
                if (options.BadRequest) {
                    options.BadRequest(errorMessages);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    this.log('error', 'quoteToOrder', clientResponse);
                }
            }
        });
    }

    public deleteOrder(options: OrderDeleteOptions): void {
        this.makeDELETERequest({
            version: 'v1',
            resource: `orders/${options.orderNumber}`,
            headers: {
                'Content-Type': 'application/json',
            },
            OK: () => {
                options.Deleted();
            },
            BadRequest: errorMessages => {
                if (options.BadRequest) {
                    options.BadRequest(errorMessages);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    this.log('error', 'deleteOrder', clientResponse);
                }
            }
        });
    }

    private log(type: 'audit' | 'error' | 'emergency' | 'debug', title: string, message: unknown): void {
        if (this.loggingEnabled) {
            title = `CHRobinsonAPI: ${title}`;
            switch (type) {
                case 'audit':
                    log.audit(title, message);
                    break;

                case 'error':
                    log.error(title, message);
                    break;

                case 'emergency':
                    log.emergency(title, message);
                    break;

                case 'debug':
                    log.debug(title, message);
                    break;

                default:
                    break;
            }
        }
    }

    /** @internal */
    private makeGETRequest(options: MakeGETRequestOptions) {
        const makeRequestOptions: MakeRequestOptions = <MakeRequestOptions>options;
        makeRequestOptions.method = https.Method.GET;
        this.makeRequest(makeRequestOptions);
    }

    /** @internal */
    private makeDELETERequest(options: MakeDELETERequestOptions) {
        const makeRequestOptions: MakeRequestOptions = <never>options;
        makeRequestOptions.method = https.Method.DELETE;
        this.makeRequest(makeRequestOptions);
    }

    /** @internal */
    private makePOSTRequest(options: MakePOSTRequestOptions) {
        const makeRequestOptions: MakeRequestOptions = <MakeRequestOptions>options;
        makeRequestOptions.method = https.Method.POST;
        this.makeRequest(makeRequestOptions);
    }

    /** @internal */
    private makePUTRequest(options: MakePUTRequestOptions) {
        const makeRequestOptions: MakeRequestOptions = <MakeRequestOptions>options;
        makeRequestOptions.method = https.Method.PUT;
        this.makeRequest(makeRequestOptions);
    }

    /** @internal */
    private makeRequest(options: MakeRequestOptions) {
        let clientResponse: https.ClientResponse = null;
        const requestOptions: https.RequestOptions = {
            method: options.method,
            url: `https://${this.domain}/${options.version}/${options.resource}`
        };

        requestOptions.headers = options.headers || {};

        if (!options.isTokenRequest) {
            requestOptions.headers.Authorization = `Bearer ${this.AccessToken}`;
        }

        if (options.payload) {
            requestOptions.body = JSON.stringify(options.payload);
        }

        try {
            this.log('audit', 'makeRequest - requestOptions', requestOptions);
            clientResponse = https.request(requestOptions);
            this.log('audit', 'makeRequest - clientResponse', clientResponse);

            switch (clientResponse.code) {
                case 200:
                    if (options.OK) {
                        options.OK(clientResponse);
                    }
                    break;

                case 201:
                    if (options.Created) {
                        options.Created(clientResponse);
                    }
                    break;

                case 202:
                    if (options.Accepted) {
                        options.Accepted(clientResponse);
                    }
                    break;

                case 400:
                    if (options.BadRequest && clientResponse.body) {
                        options.BadRequest(JSON.parse(clientResponse.body));
                    } else {
                        options.Failed(clientResponse);
                    }
                    break;

                case 401:
                    if (options.Unauthorized) {
                        options.Unauthorized(clientResponse);
                    } else {
                        options.Failed(clientResponse);
                    }
                    break;

                case 403:
                    if (options.Forbidden) {
                        options.Forbidden(clientResponse);
                    } else {
                        options.Failed(clientResponse);
                    }
                    break;

                case 404:
                    if (options.NotFound) {
                        options.NotFound(clientResponse);
                    } else {
                        options.Failed(clientResponse);
                    }
                    break;

                case 500:
                    if (options.ServerError) {
                        options.ServerError(clientResponse);
                    } else {
                        options.Failed(clientResponse);
                    }
                    break;

                default:
                    options.Failed(clientResponse);
                    break;
            }

        } catch (e) {
            options.retryCount = options.retryCount || 0;
            if (options.retryCount <= 5) {
                options.retryCount = options.retryCount + 1;
                this.makeRequest(options);
            } else {
                this.log('error', 'makeRequest', e);
                options.Failed(clientResponse);
            }
        }
    }
}

/** @internal */
interface BaseRequestOptions {
    isTokenRequest?: boolean;
    resource: string;
    version: string;
    headers?: unknown;

    BadRequest?(errorMessages: ErrorMessage[]): void;

    Failed(clientResponse: https.ClientResponse): void;
}

/** @internal */
interface MakePOSTRequestOptions extends BaseRequestOptions {
    payload: unknown;

    Created(clientResponse: https.ClientResponse): void;
}

/** @internal */
interface MakePUTRequestOptions extends BaseRequestOptions {
    payload: unknown;

    OK(clientResponse: https.ClientResponse): void;
}

/** @internal */
interface MakeGETRequestOptions extends BaseRequestOptions {

    OK?(clientResponse: https.ClientResponse): void; // generateBols breaks the GET model of an OK (200) .. and instead returns a Created (201)

    Created?(clientResponse: https.ClientResponse): void; // generateBols breaks the GET model of an OK (200) .. and instead returns a Created (201)
}

/** @internal */
interface MakeDELETERequestOptions extends BaseRequestOptions {

    OK(): void;
}

/** @internal */
interface MakeRequestOptions extends BaseRequestOptions {
    method: https.Method;
    payload?: unknown;
    retryCount?: number;

    OK?(clientResponse: https.ClientResponse): void;

    Created?(clientResponse: https.ClientResponse): void;

    Accepted?(clientResponse: https.ClientResponse): void;

    Unauthorized?(clientResponse: https.ClientResponse): void;

    Forbidden?(clientResponse: https.ClientResponse): void;

    NotFound?(clientResponse: https.ClientResponse): void;

    ServerError?(clientResponse: https.ClientResponse): void;
}

export interface Mapping {
    key: string;
    value: string;
    customerName: string;
    claimCreatedUtc: Date;
}

/** @internal */
interface AccessTokenResponse {
    accessToken: string;
    mappings: Mapping[];
}

export interface ErrorMessage {
    message: string;
    path: string[];
    type: string;
    context: Context;
}

export interface Context {
    child?: string;
    value?: { type: string; value: string }[];
    key: string;
    label: string;
}
