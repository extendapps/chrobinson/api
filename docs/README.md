**[C.H.Robinson API NetSuite module](README.md)**

> Globals

# C.H.Robinson API NetSuite module

## Index

### Classes

* [CHRobinsonAPI](classes/chrobinsonapi.md)

### Method Interfaces

* [EventRetrievalOptions](interfaces/eventretrievaloptions.md)
* [GeneratedBOLRetrievalOptions](interfaces/generatedbolretrievaloptions.md)
* [OrderCreateOptions](interfaces/ordercreateoptions.md)
* [OrderDeleteOptions](interfaces/orderdeleteoptions.md)
* [QuoteToOrderOptions](interfaces/quotetoorderoptions.md)
* [RatingRequestOptions](interfaces/ratingrequestoptions.md)

### Other Interfaces

* [Address](interfaces/address.md)
* [Appointment](interfaces/appointment.md)
* [AppointmentEvent](interfaces/appointmentevent.md)
* [AppointmentUpdatedEvent](interfaces/appointmentupdatedevent.md)
* [BaseEvent](interfaces/baseevent.md)
* [BlindLocation](interfaces/blindlocation.md)
* [BookedBillTo](interfaces/bookedbillto.md)
* [BookedCustomer](interfaces/bookedcustomer.md)
* [CargoLiability](interfaces/cargoliability.md)
* [Carrier](interfaces/carrier.md)
* [CarrierArrivedEvent](interfaces/carrierarrivedevent.md)
* [CarrierDepartedEvent](interfaces/carrierdepartedevent.md)
* [Charge](interfaces/charge.md)
* [CollectOnDelivered](interfaces/collectondelivered.md)
* [Contact](interfaces/contact.md)
* [ContactMethod](interfaces/contactmethod.md)
* [Context](interfaces/context.md)
* [Equipment](interfaces/equipment.md)
* [ErrorMessage](interfaces/errormessage.md)
* [EventAddress](interfaces/eventaddress.md)
* [EventItem](interfaces/eventitem.md)
* [EventLoadPackage](interfaces/eventloadpackage.md)
* [EventLocation](interfaces/eventlocation.md)
* [EventPackage](interfaces/eventpackage.md)
* [EventRetrievalResponse](interfaces/eventretrievalresponse.md)
* [HazardousProperty](interfaces/hazardousproperty.md)
* [InTransitAddress](interfaces/intransitaddress.md)
* [InTransitEvent](interfaces/intransitevent.md)
* [ItemEquipment](interfaces/itemequipment.md)
* [LoadBookedEvent](interfaces/loadbookedevent.md)
* [LoadCancelledEvent](interfaces/loadcancelledevent.md)
* [LoadCreatedEvent](interfaces/loadcreatedevent.md)
* [LoadDeliveredEvent](interfaces/loaddeliveredevent.md)
* [LoadEvent](interfaces/loadevent.md)
* [LoadPickedUpEvent](interfaces/loadpickedupevent.md)
* [Mapping](interfaces/mapping.md)
* [MultiLoadEvent](interfaces/multiloadevent.md)
* [NavisphereEvent](interfaces/navisphereevent.md)
* [Order](interfaces/order.md)
* [OrderAddress](interfaces/orderaddress.md)
* [OrderBillTo](interfaces/orderbillto.md)
* [OrderCanceledEvent](interfaces/ordercanceledevent.md)
* [OrderCompletedEvent](interfaces/ordercompletedevent.md)
* [OrderCreateResponse](interfaces/ordercreateresponse.md)
* [OrderCreatedEvent](interfaces/ordercreatedevent.md)
* [OrderCustomer](interfaces/ordercustomer.md)
* [OrderDetail](interfaces/orderdetail.md)
* [OrderEvent](interfaces/orderevent.md)
* [OrderEventItem](interfaces/ordereventitem.md)
* [OrderEventLocation](interfaces/ordereventlocation.md)
* [OrderEventService](interfaces/ordereventservice.md)
* [OrderItem](interfaces/orderitem.md)
* [OrderLocation](interfaces/orderlocation.md)
* [OrderPackage](interfaces/orderpackage.md)
* [OrderRejectedEvent](interfaces/orderrejectedevent.md)
* [OrderService](interfaces/orderservice.md)
* [OrderUpdatedEvent](interfaces/orderupdatedevent.md)
* [PackageDeliveredEvent](interfaces/packagedeliveredevent.md)
* [PackageInTransitEvent](interfaces/packageintransitevent.md)
* [PackagePickedUpEvent](interfaces/packagepickedupevent.md)
* [PackageStopEvent](interfaces/packagestopevent.md)
* [Quote](interfaces/quote.md)
* [QuoteBillTo](interfaces/quotebillto.md)
* [QuoteLocation](interfaces/quotelocation.md)
* [QuoteOrder](interfaces/quoteorder.md)
* [QuoteService](interfaces/quoteservice.md)
* [QuoteToOrder](interfaces/quotetoorder.md)
* [Rate](interfaces/rate.md)
* [RatingItem](interfaces/ratingitem.md)
* [RatingLocation](interfaces/ratinglocation.md)
* [RatingRequest](interfaces/ratingrequest.md)
* [RatingRequestResponse](interfaces/ratingrequestresponse.md)
* [ReferenceNumber](interfaces/referencenumber.md)
* [RefreshAccessTokenOptions](interfaces/refreshaccesstokenoptions.md)
* [Relationships](interfaces/relationships.md)
* [SingleLoadEvent](interfaces/singleloadevent.md)
* [SpecialRequirement](interfaces/specialrequirement.md)
* [StopEvent](interfaces/stopevent.md)
* [Transit](interfaces/transit.md)
* [TransportMode](interfaces/transportmode.md)

### Type aliases

* [Condition](README.md#condition)
* [ContactType](README.md#contacttype)
* [CurrencyCode](README.md#currencycode)
* [EquipmentType](README.md#equipmenttype)
* [EventType](README.md#eventtype)
* [FreightClass](README.md#freightclass)
* [FreightTerm](README.md#freightterm)
* [InvoiceMethod](README.md#invoicemethod)
* [LinearUnit](README.md#linearunit)
* [LoadingType](README.md#loadingtype)
* [MeasurementSystem](README.md#measurementsystem)
* [OrderEventLocationStyle](README.md#ordereventlocationstyle)
* [OrderEventLocationType](README.md#ordereventlocationtype)
* [PackagingCode](README.md#packagingcode)
* [SellMode](README.md#sellmode)
* [TemperatureType](README.md#temperaturetype)
* [TemperatureUnit](README.md#temperatureunit)
* [TransportModeType](README.md#transportmodetype)
* [VolumeUnit](README.md#volumeunit)
* [WeightUnit](README.md#weightunit)

### Object literals

* [orderCreatedEvent](README.md#ordercreatedevent)

## Type aliases

### Condition

Ƭ  **Condition**: \"Active\" \| \"OnHold\" \| \"PartiallyFinLocked\" \| \"FinanciallyLocked\" \| \"Rejected\" \| \"Cancelled\" \| \"WaitingAcceptance\" \| \"WaitingRecommendation\" \| \"NotShipReady\" \| \"Unreleased\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### ContactType

Ƭ  **ContactType**: \"Vendor\" \| \"Buyer\" \| \"Contact\" \| \"OrderedBy\" \| \"NotifyParty\" \| \"OnlineContact\" \| \"SchedWith\" \| \"POCustomer\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### CurrencyCode

Ƭ  **CurrencyCode**: \"USD\" \| \"CAD\" \| \"MXN\" \| \"EUR\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### EquipmentType

Ƭ  **EquipmentType**: \"Van\" \| \"Reefer\" \| \"Flatbed\" \| \"Lcl\" \| \"Container20\" \| \"Container40\" \| \"Container40HighCube\" \| \"Container45HighCube\" \| \"Container20FlatRack\" \| \"Container40FlatRack\" \| \"Container40HighCubeFlatRack\" \| \"Container45FlatRack\" \| \"Container20OpenTop\" \| \"Container40OpenTop\" \| \"Container20Reefer\" \| \"Container40Reefer\" \| \"Container40HighCubeReefer\" \| \"Container45Reefer\" \| \"Container20ReeferDryUsage\" \| \"Container40ReeferDryUsage\" \| \"Container40HighCubeReeferDryUsage\" \| \"Container45ReeferDryUsage\" \| \"Container20Platform\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### EventType

Ƭ  **EventType**: \"ORDER CREATED\" \| \"ORDER REJECTED\" \| \"ORDER UPDATED\" \| \"ORDER CANCELED\" \| \"ORDER COMPLETED\" \| \"LOAD CREATED\" \| \"LOAD BOOKED\" \| \"LOAD CANCELLED\" \| \"CARRIER ARRIVED\" \| \"CARRIER DEPARTED\" \| \"LOAD PICKED UP\" \| \"LOAD DELIVERED\" \| \"IN TRANSIT\" \| \"APPOINTMENT UPDATED\" \| \"PACKAGE IN TRANSIT\" \| \"PACKAGE PICKED UP\" \| \"PACKAGE DELIVERED\"

___

### FreightClass

Ƭ  **FreightClass**: \"50\" \| \"55\" \| \"60\" \| \"65\" \| \"70\" \| \"77.5\" \| \"85\" \| \"92.5\" \| \"100\" \| \"110\" \| \"125\" \| \"150\" \| \"175\" \| \"200\" \| \"250\" \| \"300\" \| \"400\" \| \"500\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### FreightTerm

Ƭ  **FreightTerm**: \"CL\" \| \"PP\" \| \"TP\" \| \"PC\" \| \"CC\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### InvoiceMethod

Ƭ  **InvoiceMethod**: \"PP\" \| \"CL\" \| \"TP\" \| \"PC\" \| \"CC\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### LinearUnit

Ƭ  **LinearUnit**: \"Inches\" \| \"Feet\" \| \"Centimeters\" \| \"Meters\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### LoadingType

Ƭ  **LoadingType**: \"CRANE\" \| \"REAR\" \| \"SIDE\" \| \"REAR/SIDE\" \| \"TOP/SIDE\" \| \"DOCK\" \| \"STRING\" \| \"STRING/TOW\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### MeasurementSystem

Ƭ  **MeasurementSystem**: \"Metric\" \| \"Standard\" \| \"Mixed\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### OrderEventLocationStyle

Ƭ  **OrderEventLocationStyle**: \"Customers\" \| \"Distributor\" \| \"Warehouse\" \| \"Plant\" \| \"Pool\" \| \"CrossDock\" \| \"WarehouseSpecial\" \| \"Agent\" \| \"Terminal\" \| \"RailRamp\" \| \"Port\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### OrderEventLocationType

Ƭ  **OrderEventLocationType**: \"Pick\" \| \"Drop\" \| \"Intermediate\" \| \"OriginPort\" \| \"DestinationPort\" \| \"IntermediatePort\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### PackagingCode

Ƭ  **PackagingCode**: \"BAL\" \| \"BIN\" \| \"BOT\" \| \"BXT\" \| \"JAR\" \| \"LSE\" \| \"LUG\" \| \"OTHR\" \| \"REL\" \| \"SAK\" \| \"SKD\" \| \"SKE\" \| \"SLP\" \| \"TLD\" \| \"TRY\" \| \"Empty\" \| \"BAG\" \| \"BOX\" \| \"BDL\" \| \"CTN\" \| \"CAS\" \| \"CRT\" \| \"DZ\" \| \"DRM\" \| \"EAC\" \| \"GYL\" \| \"HDW\" \| \"LNFT\" \| \"PKG\" \| \"PAL\" \| \"PLT\" \| \"PCS\" \| \"LBS\" \| \"ROL\" \| \"TOT\" \| \"YD\" \| \"Skids\" \| \"GAL\" \| \"KIT\" \| \"FT\" \| \"PAD\" \| \"PR\" \| \"SET\" \| \"SHT\" \| \"M\" \| \"KG\" \| \"IBC\" \| \"BAT\" \| \"AST\" \| \"CUB\" \| \"RKS\" \| \"PCK\" \| \"TH\" \| \"24BIN\" \| \"30BIN\" \| \"36BIN\" \| \"40BIN\" \| \"BSHL\" \| \"CLLPK\" \| \"DRC\" \| \"ECO\" \| \"EURO\" \| \"FLAT\" \| \"HFBIN\" \| \"HFCTN\" \| \"HVYPK\" \| \"IFCO\" \| \"I6408\" \| \"I6411\" \| \"I6413\" \| \"I6416\" \| \"I6419\" \| \"I6423\" \| \"I6425\" \| \"I6428\" \| \"ORBIS\" \| \"O6409\" \| \"O6411\" \| \"O6413\" \| \"O6416\" \| \"O6419\" \| \"O6425\" \| \"O6428\" \| \"P4311\" \| \"P4317\" \| \"P6408\" \| \"P6411\" \| \"P6413\" \| \"P6416\" \| \"P6419\" \| \"P6423\" \| \"P6425\" \| \"P6428\" \| \"RPC\" \| \"R6411\" \| \"R6413\" \| \"R6416\" \| \"TNBIN\" \| \"TOSCA\" \| \"T6408\" \| \"T6411\" \| \"T6413\" \| \"T6416\" \| \"T6419\" \| \"T6423\" \| \"T6425\" \| \"T6428\" \| \"WDCRT\" \| \"BRL\" \| \"RCK\" \| \"TUB\" \| \"CG\" \| \"DSP\" \| \"SQF\" \| \"CYL\" \| \"KEG\" \| \"IFCLB\" \| \"PLT1\" \| \"PLT2\" \| \"PLT3\" \| \"CAR\" \| \"COIL\" \| \"PI\" \| \"GOH\" \| \"BLK\" \| \"POG\" \| \"L\" \| \"IPLT\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### SellMode

Ƭ  **SellMode**: \"LTL\" \| \"TL\" \| \"LTLFlatbed\" \| \"TLFlatbed\" \| \"IMDL\" \| \"Bulk\" \| \"Consolidated\" \| \"DomesticAir\" \| \"SmallParcel\" \| \"Ocean\" \| \"Tautliner\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### TemperatureType

Ƭ  **TemperatureType**: \"Dry\" \| \"Fresh\" \| \"Frozen\" \| \"Chilled\" \| \"DeepFrozen\" \| \"Refrigerated\" \| \"Protect\" \| \"Custom\" \| \"Produce\" \| \"Mixed\" \| \"Empty\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### TemperatureUnit

Ƭ  **TemperatureUnit**: \"Fahrenheit\" \| \"Celsius\" \| \"Empty\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### TransportModeType

Ƭ  **TransportModeType**: \"LTL\" \| \"TL\" \| \"Air\" \| \"Ocean\" \| \"Bulk\" \| \"Consol\" \| \"Flatbed\"

___

### VolumeUnit

Ƭ  **VolumeUnit**: \"CubicFeet\" \| \"CubicMeters\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

___

### WeightUnit

Ƭ  **WeightUnit**: \"Pounds\" \| \"Kilograms\"

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Object literals

### orderCreatedEvent

▪ `Const` **orderCreatedEvent**: object

#### Properties:

Name | Type | Value |
------ | ------ | ------ |
`eventType` | \"ORDER CREATED\" | "ORDER CREATED" |
`loadNumbers` | undefined[] | [] |
`orderDetail` | { condition: \"Active\" = "Active"; navisphereTrackingLink: string = "https://online.chrobinson.com/tracking/#/?trackingNumber=6ZA4KNM82DZ"; navisphereTrackingNumber: string = "6ZA4KNM82DZ"; orderNumber: number = 7855070; services: { condition: \"Active\" = "Active"; description: string = "Truckload Transportation"; items: { description: string = "Widgets"; freightClass: \"70\" = "70"; height: null = null; heightUnitOfMeasure: null = null; insuranceValue: null = null; length: null = null; lengthUnitOfMeasure: null = null; maxWeight: null = null; maxWeightUnitOfMeasure: null = null; minWeight: null = null; minWeightUnitOfMeasure: null = null; packagingType: \"PLT\" = "PLT"; palletSpaces: number = 1; pallets: number = 1; pluNumber: string = "SIMPCustPLU12345"; productCode: string = "MISC"; referenceNumbers: { type: \"PO\" = "PO"; value: string = "SIMPCustPO12345" }[] = [
                                {
                                    'type': 'PO',
                                    'value': 'SIMPCustPO12345'
                                }
                            ]; skuNumber: string = "SIMPCustSKU12345"; trailerLengthUsed: number = 1; trailerLengthUsedUnitOfMeasure: \"Feet\" = "Feet"; upcNumber: string = "SIMPCustUPC12345"; volume: number = 7680; volumeUnitOfMeasure: \"CubicFeet\" = "CubicFeet"; width: null = null; widthUnitOfMeasure: null = null }[] = [
                        {
                            'packagingType': 'PLT',
                            'description': 'Widgets',
                            'productCode': 'MISC',
                            'freightClass': '70',
                            'pluNumber': 'SIMPCustPLU12345',
                            'skuNumber': 'SIMPCustSKU12345',
                            'upcNumber': 'SIMPCustUPC12345',
                            'insuranceValue': null,
                            'minWeight': null,
                            'minWeightUnitOfMeasure': null,
                            'maxWeight': null,
                            'maxWeightUnitOfMeasure': null,
                            'length': null,
                            'lengthUnitOfMeasure': null,
                            'height': null,
                            'heightUnitOfMeasure': null,
                            'width': null,
                            'widthUnitOfMeasure': null,
                            'volume': 7680,
                            'volumeUnitOfMeasure': 'CubicFeet',
                            'pallets': 1,
                            'palletSpaces': 1,
                            'trailerLengthUsed': 1,
                            'trailerLengthUsedUnitOfMeasure': 'Feet',
                            'referenceNumbers': [
                                {
                                    'type': 'PO',
                                    'value': 'SIMPCustPO12345'
                                }
                            ]
                        }
                    ]; locations: ({ chrLocationId: string = "W5188129-P"; customerLocationId: string = "W541849"; name: string = "Holaday Parks Inc Interurban Exchange 4/5"; referenceNumbers: { type: \"PU\" = "PU"; value: string = "SIMPCustPU12345" }[] = [
                                {
                                    'type': 'PU',
                                    'value': 'SIMPCustPU12345'
                                }
                            ]; style: \"Warehouse\" = "Warehouse"; type: \"Pick\" = "Pick"; address: { addressLine1: string = "410 Terry Ave N"; addressLine2: string = ""; city: string = "Seattle"; country: string = "US"; latitude: number = 47.63215637207031; longitude: number = -122.34669494628906; postalCode: string = "981095210"; stateProvinceCode: null = null }  } \| { chrLocationId: string = "W7223174-D"; customerLocationId: string = "W541849"; name: string = "Google Inc"; referenceNumbers: { type: \"DEL\" = "DEL"; value: string = "SIMPCustDEL12345" }[] = [
                                {
                                    'type': 'DEL',
                                    'value': 'SIMPCustDEL12345'
                                }
                            ]; style: \"Warehouse\" = "Warehouse"; type: \"Drop\" = "Drop"; address: { addressLine1: string = "1600 Amphitheatre Parkway"; addressLine2: string = ""; city: string = "Mountain View"; country: string = "US"; latitude: number = 37.410301208496094; longitude: number = -122.09291076660156; postalCode: string = "94043"; stateProvinceCode: null = null }  })[] = [
                        {
                            'chrLocationId': 'W5188129-P',
                            'customerLocationId': 'W541849',
                            'type': 'Pick',
                            'style': 'Warehouse',
                            'name': 'Holaday Parks Inc Interurban Exchange 4/5',
                            'address': {
                                'addressLine1': '410 Terry Ave N',
                                'addressLine2': '',
                                'city': 'Seattle',
                                'stateProvinceCode': null,
                                'postalCode': '981095210',
                                'country': 'US',
                                'latitude': 47.63215637207031,
                                'longitude': -122.34669494628906
                            },
                            'referenceNumbers': [
                                {
                                    'type': 'PU',
                                    'value': 'SIMPCustPU12345'
                                }
                            ]
                        },
                        {
                            'chrLocationId': 'W7223174-D',
                            'customerLocationId': 'W541849',
                            'type': 'Drop',
                            'style': 'Warehouse',
                            'name': 'Google Inc',
                            'address': {
                                'addressLine1': '1600 Amphitheatre Parkway',
                                'addressLine2': '',
                                'city': 'Mountain View',
                                'stateProvinceCode': null,
                                'postalCode': '94043',
                                'country': 'US',
                                'latitude': 37.410301208496094,
                                'longitude': -122.09291076660156
                            },
                            'referenceNumbers': [
                                {
                                    'type': 'DEL',
                                    'value': 'SIMPCustDEL12345'
                                }
                            ]
                        }
                    ]; mode: string = "Van"; serviceLevel: string = "Standard" }[] = [
                {
                    'condition': 'Active',
                    'mode': 'Van',
                    'description': 'Truckload Transportation',
                    'serviceLevel': 'Standard',
                    'locations': [
                        {
                            'chrLocationId': 'W5188129-P',
                            'customerLocationId': 'W541849',
                            'type': 'Pick',
                            'style': 'Warehouse',
                            'name': 'Holaday Parks Inc Interurban Exchange 4/5',
                            'address': {
                                'addressLine1': '410 Terry Ave N',
                                'addressLine2': '',
                                'city': 'Seattle',
                                'stateProvinceCode': null,
                                'postalCode': '981095210',
                                'country': 'US',
                                'latitude': 47.63215637207031,
                                'longitude': -122.34669494628906
                            },
                            'referenceNumbers': [
                                {
                                    'type': 'PU',
                                    'value': 'SIMPCustPU12345'
                                }
                            ]
                        },
                        {
                            'chrLocationId': 'W7223174-D',
                            'customerLocationId': 'W541849',
                            'type': 'Drop',
                            'style': 'Warehouse',
                            'name': 'Google Inc',
                            'address': {
                                'addressLine1': '1600 Amphitheatre Parkway',
                                'addressLine2': '',
                                'city': 'Mountain View',
                                'stateProvinceCode': null,
                                'postalCode': '94043',
                                'country': 'US',
                                'latitude': 37.410301208496094,
                                'longitude': -122.09291076660156
                            },
                            'referenceNumbers': [
                                {
                                    'type': 'DEL',
                                    'value': 'SIMPCustDEL12345'
                                }
                            ]
                        }
                    ],
                    'items': [
                        {
                            'packagingType': 'PLT',
                            'description': 'Widgets',
                            'productCode': 'MISC',
                            'freightClass': '70',
                            'pluNumber': 'SIMPCustPLU12345',
                            'skuNumber': 'SIMPCustSKU12345',
                            'upcNumber': 'SIMPCustUPC12345',
                            'insuranceValue': null,
                            'minWeight': null,
                            'minWeightUnitOfMeasure': null,
                            'maxWeight': null,
                            'maxWeightUnitOfMeasure': null,
                            'length': null,
                            'lengthUnitOfMeasure': null,
                            'height': null,
                            'heightUnitOfMeasure': null,
                            'width': null,
                            'widthUnitOfMeasure': null,
                            'volume': 7680,
                            'volumeUnitOfMeasure': 'CubicFeet',
                            'pallets': 1,
                            'palletSpaces': 1,
                            'trailerLengthUsed': 1,
                            'trailerLengthUsedUnitOfMeasure': 'Feet',
                            'referenceNumbers': [
                                {
                                    'type': 'PO',
                                    'value': 'SIMPCustPO12345'
                                }
                            ]
                        }
                    ]
                }
            ] }[] | [         {             'navisphereTrackingNumber': '6ZA4KNM82DZ',             'navisphereTrackingLink': 'https://online.chrobinson.com/tracking/#/?trackingNumber=6ZA4KNM82DZ',             'orderNumber': 7855070,             'condition': 'Active',             'services': [                 {                     'condition': 'Active',                     'mode': 'Van',                     'description': 'Truckload Transportation',                     'serviceLevel': 'Standard',                     'locations': [                         {                             'chrLocationId': 'W5188129-P',                             'customerLocationId': 'W541849',                             'type': 'Pick',                             'style': 'Warehouse',                             'name': 'Holaday Parks Inc Interurban Exchange 4/5',                             'address': {                                 'addressLine1': '410 Terry Ave N',                                 'addressLine2': '',                                 'city': 'Seattle',                                 'stateProvinceCode': null,                                 'postalCode': '981095210',                                 'country': 'US',                                 'latitude': 47.63215637207031,                                 'longitude': -122.34669494628906                             },                             'referenceNumbers': [                                 {                                     'type': 'PU',                                     'value': 'SIMPCustPU12345'                                 }                             ]                         },                         {                             'chrLocationId': 'W7223174-D',                             'customerLocationId': 'W541849',                             'type': 'Drop',                             'style': 'Warehouse',                             'name': 'Google Inc',                             'address': {                                 'addressLine1': '1600 Amphitheatre Parkway',                                 'addressLine2': '',                                 'city': 'Mountain View',                                 'stateProvinceCode': null,                                 'postalCode': '94043',                                 'country': 'US',                                 'latitude': 37.410301208496094,                                 'longitude': -122.09291076660156                             },                             'referenceNumbers': [                                 {                                     'type': 'DEL',                                     'value': 'SIMPCustDEL12345'                                 }                             ]                         }                     ],                     'items': [                         {                             'packagingType': 'PLT',                             'description': 'Widgets',                             'productCode': 'MISC',                             'freightClass': '70',                             'pluNumber': 'SIMPCustPLU12345',                             'skuNumber': 'SIMPCustSKU12345',                             'upcNumber': 'SIMPCustUPC12345',                             'insuranceValue': null,                             'minWeight': null,                             'minWeightUnitOfMeasure': null,                             'maxWeight': null,                             'maxWeightUnitOfMeasure': null,                             'length': null,                             'lengthUnitOfMeasure': null,                             'height': null,                             'heightUnitOfMeasure': null,                             'width': null,                             'widthUnitOfMeasure': null,                             'volume': 7680,                             'volumeUnitOfMeasure': 'CubicFeet',                             'pallets': 1,                             'palletSpaces': 1,                             'trailerLengthUsed': 1,                             'trailerLengthUsedUnitOfMeasure': 'Feet',                             'referenceNumbers': [                                 {                                     'type': 'PO',                                     'value': 'SIMPCustPO12345'                                 }                             ]                         }                     ]                 }             ]         }     ] |
`referenceNumbers` | ({ type: \"MBOL\" = "MBOL"; value: string = "SIMPCustMBOL7497" } \| { type: \"SHID\" = "SHID"; value: string = "SIMPCustSHID7497" } \| { type: \"PU\" = "PU"; value: string = "SIMPCustPU12345" } \| { type: \"DEL\" = "DEL"; value: string = "SIMPCustDEL12345" } \| { type: \"PO\" = "PO"; value: string = "SIMPCustPO12345" } \| { type: \"PRO\" = "PRO"; value: string = "SIMPCustPRO12345" } \| { type: \"CON\" = "CON"; value: string = "CON7497" } \| { type: \"JNO\" = "JNO"; value: string = "NOT A REAL ORDER!!!!!!!!!!" } \| { type: \"CRID\" = "CRID"; value: string = "59edfb6e8ce6700001553f3c" } \| { type: \"ORDTRK\" = "ORDTRK"; value: string = "6ZA4KNM82DZ" })[] | [         {             'type': 'MBOL',             'value': 'SIMPCustMBOL7497'         },         {             'type': 'SHID',             'value': 'SIMPCustSHID7497'         },         {             'type': 'PU',             'value': 'SIMPCustPU12345'         },         {             'type': 'DEL',             'value': 'SIMPCustDEL12345'         },         {             'type': 'PO',             'value': 'SIMPCustPO12345'         },         {             'type': 'PRO',             'value': 'SIMPCustPRO12345'         },         {             'type': 'CON',             'value': 'CON7497'         },         {             'type': 'JNO',             'value': 'NOT A REAL ORDER!!!!!!!!!!'         },         {             'type': 'CRID',             'value': '59edfb6e8ce6700001553f3c'         },         {             'type': 'ORDTRK',             'value': '6ZA4KNM82DZ'         }     ] |
