**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / CHRobinsonAPI

# Class: CHRobinsonAPI

## Hierarchy

* **CHRobinsonAPI**

## Index

### Constructors

* [constructor](chrobinsonapi.md#constructor)

### Accessors

* [EnableLogging](chrobinsonapi.md#enablelogging)
* [Mappings](chrobinsonapi.md#mappings)

### Methods

* [createOrder](chrobinsonapi.md#createorder)
* [deleteOrder](chrobinsonapi.md#deleteorder)
* [eventRetrieval](chrobinsonapi.md#eventretrieval)
* [generatedBOLRetrieval](chrobinsonapi.md#generatedbolretrieval)
* [quoteToOrder](chrobinsonapi.md#quotetoorder)
* [ratingRequest](chrobinsonapi.md#ratingrequest)
* [refreshAccessToken](chrobinsonapi.md#refreshaccesstoken)

## Constructors

### constructor

\+ **new CHRobinsonAPI**(`secretScriptId?`: string, `isProduction?`: boolean): [CHRobinsonAPI](chrobinsonapi.md)

#### Parameters:

Name | Type | Default value |
------ | ------ | ------ |
`secretScriptId` | string | "custsecret\_chr\_secret" |
`isProduction` | boolean | false |

**Returns:** [CHRobinsonAPI](chrobinsonapi.md)

## Accessors

### EnableLogging

• set **EnableLogging**(`toggle`: boolean): void

#### Parameters:

Name | Type |
------ | ------ |
`toggle` | boolean |

**Returns:** void

___

### Mappings

• get **Mappings**(): [Mapping](../interfaces/mapping.md)[]

**Returns:** [Mapping](../interfaces/mapping.md)[]

## Methods

### createOrder

▸ **createOrder**(`options`: [OrderCreateOptions](../interfaces/ordercreateoptions.md)): void

#### Parameters:

Name | Type |
------ | ------ |
`options` | [OrderCreateOptions](../interfaces/ordercreateoptions.md) |

**Returns:** void

___

### deleteOrder

▸ **deleteOrder**(`options`: [OrderDeleteOptions](../interfaces/orderdeleteoptions.md)): void

#### Parameters:

Name | Type |
------ | ------ |
`options` | [OrderDeleteOptions](../interfaces/orderdeleteoptions.md) |

**Returns:** void

___

### eventRetrieval

▸ **eventRetrieval**(`options`: [EventRetrievalOptions](../interfaces/eventretrievaloptions.md)): void

#### Parameters:

Name | Type |
------ | ------ |
`options` | [EventRetrievalOptions](../interfaces/eventretrievaloptions.md) |

**Returns:** void

___

### generatedBOLRetrieval

▸ **generatedBOLRetrieval**(`options`: [GeneratedBOLRetrievalOptions](../interfaces/generatedbolretrievaloptions.md)): void

#### Parameters:

Name | Type |
------ | ------ |
`options` | [GeneratedBOLRetrievalOptions](../interfaces/generatedbolretrievaloptions.md) |

**Returns:** void

___

### quoteToOrder

▸ **quoteToOrder**(`options`: [QuoteToOrderOptions](../interfaces/quotetoorderoptions.md)): void

#### Parameters:

Name | Type |
------ | ------ |
`options` | [QuoteToOrderOptions](../interfaces/quotetoorderoptions.md) |

**Returns:** void

___

### ratingRequest

▸ **ratingRequest**(`options`: [RatingRequestOptions](../interfaces/ratingrequestoptions.md)): void

#### Parameters:

Name | Type |
------ | ------ |
`options` | [RatingRequestOptions](../interfaces/ratingrequestoptions.md) |

**Returns:** void

___

### refreshAccessToken

▸ **refreshAccessToken**(`options`: [RefreshAccessTokenOptions](../interfaces/refreshaccesstokenoptions.md)): void

#### Parameters:

Name | Type |
------ | ------ |
`options` | [RefreshAccessTokenOptions](../interfaces/refreshaccesstokenoptions.md) |

**Returns:** void
