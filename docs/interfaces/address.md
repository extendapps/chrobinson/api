**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Address

# Interface: Address

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **Address**

## Index

### Properties

* [address1](address.md#address1)
* [address2](address.md#address2)
* [address3](address.md#address3)
* [address4](address.md#address4)
* [addressLine1](address.md#addressline1)
* [addressLine2](address.md#addressline2)
* [city](address.md#city)
* [country](address.md#country)
* [county](address.md#county)
* [lat](address.md#lat)
* [latitude](address.md#latitude)
* [long](address.md#long)
* [longitude](address.md#longitude)
* [postalCode](address.md#postalcode)
* [stateProvinceCode](address.md#stateprovincecode)

## Properties

### address1

• `Optional` **address1**: string

___

### address2

• `Optional` **address2**: string

___

### address3

• `Optional` **address3**: string

___

### address4

• `Optional` **address4**: string

___

### addressLine1

• `Optional` **addressLine1**: string

___

### addressLine2

• `Optional` **addressLine2**: string

___

### city

•  **city**: string

___

### country

•  **country**: string

___

### county

• `Optional` **county**: string

___

### lat

• `Optional` **lat**: string \| number

___

### latitude

• `Optional` **latitude**: number

___

### long

• `Optional` **long**: string \| number

___

### longitude

• `Optional` **longitude**: number

___

### postalCode

•  **postalCode**: string

___

### stateProvinceCode

•  **stateProvinceCode**: string
