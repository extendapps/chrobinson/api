**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Appointment

# Interface: Appointment

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **Appointment**

## Index

### Properties

* [changeReason](appointment.md#changereason)
* [scheduledCloseDateTimeUTC](appointment.md#scheduledclosedatetimeutc)
* [scheduledCompany](appointment.md#scheduledcompany)
* [scheduledOpenDateTimeUTC](appointment.md#scheduledopendatetimeutc)
* [scheduledWithName](appointment.md#scheduledwithname)

## Properties

### changeReason

•  **changeReason**: \"1\"

___

### scheduledCloseDateTimeUTC

•  **scheduledCloseDateTimeUTC**: string

___

### scheduledCompany

•  **scheduledCompany**: string

___

### scheduledOpenDateTimeUTC

•  **scheduledOpenDateTimeUTC**: string

___

### scheduledWithName

•  **scheduledWithName**: string
