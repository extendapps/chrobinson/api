**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / AppointmentEvent

# Interface: AppointmentEvent

## Hierarchy

* [SingleLoadEvent](singleloadevent.md)

  ↳ **AppointmentEvent**

  ↳↳ [AppointmentUpdatedEvent](appointmentupdatedevent.md)

## Index

### Properties

* [appointment](appointmentevent.md#appointment)
* [carrier](appointmentevent.md#carrier)
* [eventType](appointmentevent.md#eventtype)
* [loadNumber](appointmentevent.md#loadnumber)
* [location](appointmentevent.md#location)
* [orderDetails](appointmentevent.md#orderdetails)

## Properties

### appointment

•  **appointment**: [Appointment](appointment.md)

___

### carrier

•  **carrier**: [Carrier](carrier.md)

___

### eventType

•  **eventType**: \"APPOINTMENT UPDATED\"

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*
