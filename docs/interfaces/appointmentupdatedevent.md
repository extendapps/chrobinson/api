**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / AppointmentUpdatedEvent

# Interface: AppointmentUpdatedEvent

## Hierarchy

* [AppointmentEvent](appointmentevent.md)

  ↳ **AppointmentUpdatedEvent**

## Index

### Properties

* [appointment](appointmentupdatedevent.md#appointment)
* [carrier](appointmentupdatedevent.md#carrier)
* [eventType](appointmentupdatedevent.md#eventtype)
* [loadNumber](appointmentupdatedevent.md#loadnumber)
* [location](appointmentupdatedevent.md#location)
* [orderDetails](appointmentupdatedevent.md#orderdetails)

## Properties

### appointment

•  **appointment**: [Appointment](appointment.md)

*Inherited from [AppointmentEvent](appointmentevent.md).[appointment](appointmentevent.md#appointment)*

___

### carrier

•  **carrier**: [Carrier](carrier.md)

*Inherited from [AppointmentEvent](appointmentevent.md).[carrier](appointmentevent.md#carrier)*

___

### eventType

•  **eventType**: \"APPOINTMENT UPDATED\"

*Overrides [AppointmentEvent](appointmentevent.md).[eventType](appointmentevent.md#eventtype)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

*Inherited from [AppointmentEvent](appointmentevent.md).[location](appointmentevent.md#location)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*
