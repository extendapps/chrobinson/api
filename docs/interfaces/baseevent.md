**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / BaseEvent

# Interface: BaseEvent

## Hierarchy

* **BaseEvent**

## Index

### Properties

* [eventType](baseevent.md#eventtype)

## Properties

### eventType

•  **eventType**: [EventType](../README.md#eventtype)
