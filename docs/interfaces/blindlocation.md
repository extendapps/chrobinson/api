**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / BlindLocation

# Interface: BlindLocation

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **BlindLocation**

## Index

### Properties

* [blindAddress](blindlocation.md#blindaddress)
* [blindCity](blindlocation.md#blindcity)
* [blindName](blindlocation.md#blindname)
* [blindState](blindlocation.md#blindstate)

## Properties

### blindAddress

• `Optional` **blindAddress**: string

___

### blindCity

• `Optional` **blindCity**: string

___

### blindName

• `Optional` **blindName**: string

___

### blindState

• `Optional` **blindState**: string
