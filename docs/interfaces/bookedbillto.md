**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / BookedBillTo

# Interface: BookedBillTo

## Hierarchy

* **BookedBillTo**

## Index

### Properties

* [charges](bookedbillto.md#charges)
* [contacts](bookedbillto.md#contacts)
* [currencyCode](bookedbillto.md#currencycode)
* [customerCode](bookedbillto.md#customercode)
* [customerName](bookedbillto.md#customername)
* [exchangeRate](bookedbillto.md#exchangerate)
* [invoiceMethod](bookedbillto.md#invoicemethod)
* [milesBilled](bookedbillto.md#milesbilled)
* [referenceNumbers](bookedbillto.md#referencenumbers)

## Properties

### charges

• `Optional` **charges**: [Charge](charge.md)[]

___

### contacts

• `Optional` **contacts**: [Contact](contact.md)[]

___

### currencyCode

•  **currencyCode**: [CurrencyCode](../README.md#currencycode)

___

### customerCode

•  **customerCode**: string

___

### customerName

•  **customerName**: string

___

### exchangeRate

•  **exchangeRate**: string

___

### invoiceMethod

•  **invoiceMethod**: [InvoiceMethod](../README.md#invoicemethod)

___

### milesBilled

•  **milesBilled**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]
