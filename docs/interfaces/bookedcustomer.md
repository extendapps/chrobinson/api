**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / BookedCustomer

# Interface: BookedCustomer

## Hierarchy

* **BookedCustomer**

## Index

### Properties

* [contacts](bookedcustomer.md#contacts)
* [customerCode](bookedcustomer.md#customercode)
* [customerName](bookedcustomer.md#customername)
* [referenceNumbers](bookedcustomer.md#referencenumbers)

## Properties

### contacts

• `Optional` **contacts**: [Contact](contact.md)[]

___

### customerCode

•  **customerCode**: string

___

### customerName

•  **customerName**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]
