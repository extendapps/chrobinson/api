**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / CargoLiability

# Interface: CargoLiability

## Hierarchy

* **CargoLiability**

## Index

### Properties

* [amount](cargoliability.md#amount)
* [currencyCode](cargoliability.md#currencycode)
* [max](cargoliability.md#max)
* [perPound](cargoliability.md#perpound)

## Properties

### amount

•  **amount**: number

___

### currencyCode

•  **currencyCode**: [CurrencyCode](../README.md#currencycode)

___

### max

•  **max**: number

___

### perPound

•  **perPound**: number
