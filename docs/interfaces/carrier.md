**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Carrier

# Interface: Carrier

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **Carrier**

## Index

### Properties

* [carrierCode](carrier.md#carriercode)
* [carrierName](carrier.md#carriername)
* [dotNumber](carrier.md#dotnumber)
* [driverName](carrier.md#drivername)
* [driverPhone](carrier.md#driverphone)
* [licensePlate](carrier.md#licenseplate)
* [name](carrier.md#name)
* [proNumber](carrier.md#pronumber)
* [scac](carrier.md#scac)
* [sealNumber](carrier.md#sealnumber)
* [secondDriverName](carrier.md#seconddrivername)
* [tractorNumber](carrier.md#tractornumber)
* [trailerNumber](carrier.md#trailernumber)

## Properties

### carrierCode

• `Optional` **carrierCode**: string

___

### carrierName

• `Optional` **carrierName**: string

___

### dotNumber

• `Optional` **dotNumber**: string

___

### driverName

• `Optional` **driverName**: string

___

### driverPhone

• `Optional` **driverPhone**: string

___

### licensePlate

• `Optional` **licensePlate**: string

___

### name

•  **name**: string

___

### proNumber

•  **proNumber**: string

___

### scac

•  **scac**: string

___

### sealNumber

• `Optional` **sealNumber**: string

___

### secondDriverName

• `Optional` **secondDriverName**: string

___

### tractorNumber

• `Optional` **tractorNumber**: string

___

### trailerNumber

• `Optional` **trailerNumber**: string
