**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / CarrierArrivedEvent

# Interface: CarrierArrivedEvent

## Hierarchy

* [StopEvent](stopevent.md)

  ↳ **CarrierArrivedEvent**

## Index

### Properties

* [carrier](carrierarrivedevent.md#carrier)
* [eventType](carrierarrivedevent.md#eventtype)
* [items](carrierarrivedevent.md#items)
* [lateReasonCode](carrierarrivedevent.md#latereasoncode)
* [loadNumber](carrierarrivedevent.md#loadnumber)
* [location](carrierarrivedevent.md#location)
* [orderDetails](carrierarrivedevent.md#orderdetails)
* [packages](carrierarrivedevent.md#packages)
* [proofOfDelivery](carrierarrivedevent.md#proofofdelivery)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

*Inherited from [StopEvent](stopevent.md).[carrier](stopevent.md#carrier)*

___

### eventType

•  **eventType**: \"CARRIER ARRIVED\"

*Overrides [StopEvent](stopevent.md).[eventType](stopevent.md#eventtype)*

___

### items

•  **items**: [EventItem](eventitem.md)[]

*Inherited from [StopEvent](stopevent.md).[items](stopevent.md#items)*

___

### lateReasonCode

•  **lateReasonCode**: string

*Inherited from [StopEvent](stopevent.md).[lateReasonCode](stopevent.md#latereasoncode)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

*Inherited from [StopEvent](stopevent.md).[location](stopevent.md#location)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventLoadPackage](eventloadpackage.md)[]

*Inherited from [StopEvent](stopevent.md).[packages](stopevent.md#packages)*

___

### proofOfDelivery

•  **proofOfDelivery**: string

*Inherited from [StopEvent](stopevent.md).[proofOfDelivery](stopevent.md#proofofdelivery)*
