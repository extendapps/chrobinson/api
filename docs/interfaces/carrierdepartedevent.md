**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / CarrierDepartedEvent

# Interface: CarrierDepartedEvent

## Hierarchy

* [StopEvent](stopevent.md)

  ↳ **CarrierDepartedEvent**

## Index

### Properties

* [carrier](carrierdepartedevent.md#carrier)
* [eventType](carrierdepartedevent.md#eventtype)
* [items](carrierdepartedevent.md#items)
* [lateReasonCode](carrierdepartedevent.md#latereasoncode)
* [loadNumber](carrierdepartedevent.md#loadnumber)
* [location](carrierdepartedevent.md#location)
* [orderDetails](carrierdepartedevent.md#orderdetails)
* [packages](carrierdepartedevent.md#packages)
* [proofOfDelivery](carrierdepartedevent.md#proofofdelivery)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

*Inherited from [StopEvent](stopevent.md).[carrier](stopevent.md#carrier)*

___

### eventType

•  **eventType**: \"CARRIER DEPARTED\"

*Overrides [StopEvent](stopevent.md).[eventType](stopevent.md#eventtype)*

___

### items

•  **items**: [EventItem](eventitem.md)[]

*Inherited from [StopEvent](stopevent.md).[items](stopevent.md#items)*

___

### lateReasonCode

•  **lateReasonCode**: string

*Inherited from [StopEvent](stopevent.md).[lateReasonCode](stopevent.md#latereasoncode)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

*Inherited from [StopEvent](stopevent.md).[location](stopevent.md#location)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventLoadPackage](eventloadpackage.md)[]

*Inherited from [StopEvent](stopevent.md).[packages](stopevent.md#packages)*

___

### proofOfDelivery

•  **proofOfDelivery**: string

*Inherited from [StopEvent](stopevent.md).[proofOfDelivery](stopevent.md#proofofdelivery)*
