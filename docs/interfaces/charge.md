**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Charge

# Interface: Charge

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **Charge**

## Index

### Properties

* [alternateDescription](charge.md#alternatedescription)
* [chargeType](charge.md#chargetype)
* [quantity](charge.md#quantity)
* [rate](charge.md#rate)
* [rateCode](charge.md#ratecode)

## Properties

### alternateDescription

• `Optional` **alternateDescription**: string

___

### chargeType

• `Optional` **chargeType**: string

___

### quantity

• `Optional` **quantity**: number

___

### rate

•  **rate**: number

___

### rateCode

•  **rateCode**: string
