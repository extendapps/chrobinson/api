**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / CollectOnDelivered

# Interface: CollectOnDelivered

## Hierarchy

* **CollectOnDelivered**

## Index

### Properties

* [address](collectondelivered.md#address)
* [amount](collectondelivered.md#amount)
* [mailTo](collectondelivered.md#mailto)
* [payableTo](collectondelivered.md#payableto)
* [paymentType](collectondelivered.md#paymenttype)
* [referenceNumbers](collectondelivered.md#referencenumbers)

## Properties

### address

•  **address**: [Address](address.md)

___

### amount

•  **amount**: number

___

### mailTo

•  **mailTo**: string

___

### payableTo

•  **payableTo**: string

___

### paymentType

•  **paymentType**: string

___

### referenceNumbers

•  **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]
