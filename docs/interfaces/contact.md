**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Contact

# Interface: Contact

## Hierarchy

* **Contact**

## Index

### Properties

* [companyName](contact.md#companyname)
* [contactMethods](contact.md#contactmethods)
* [email](contact.md#email)
* [name](contact.md#name)
* [phoneNumber](contact.md#phonenumber)
* [referenceNumbers](contact.md#referencenumbers)
* [type](contact.md#type)

## Properties

### companyName

•  **companyName**: string

___

### contactMethods

•  **contactMethods**: [ContactMethod](contactmethod.md)[]

___

### email

• `Optional` **email**: string

___

### name

•  **name**: string

___

### phoneNumber

• `Optional` **phoneNumber**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### type

•  **type**: [ContactType](../README.md#contacttype)
