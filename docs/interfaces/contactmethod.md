**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / ContactMethod

# Interface: ContactMethod

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **ContactMethod**

## Index

### Properties

* [method](contactmethod.md#method)
* [value](contactmethod.md#value)

## Properties

### method

•  **method**: \"Email\" \| \"Phone\" \| \"Fax\"

___

### value

•  **value**: string
