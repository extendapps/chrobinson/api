**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Context

# Interface: Context

## Hierarchy

* **Context**

## Index

### Properties

* [child](context.md#child)
* [key](context.md#key)
* [label](context.md#label)
* [value](context.md#value)

## Properties

### child

• `Optional` **child**: string

___

### key

•  **key**: string

___

### label

•  **label**: string

___

### value

• `Optional` **value**: { type: string ; value: string  }[]
