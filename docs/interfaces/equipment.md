**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Equipment

# Interface: Equipment

## Hierarchy

* **Equipment**

## Index

### Properties

* [equipmentType](equipment.md#equipmenttype)
* [quantity](equipment.md#quantity)

## Properties

### equipmentType

• `Optional` **equipmentType**: [EquipmentType](../README.md#equipmenttype)

___

### quantity

• `Optional` **quantity**: number
