**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / ErrorMessage

# Interface: ErrorMessage

## Hierarchy

* **ErrorMessage**

## Index

### Properties

* [context](errormessage.md#context)
* [message](errormessage.md#message)
* [path](errormessage.md#path)
* [type](errormessage.md#type)

## Properties

### context

•  **context**: [Context](context.md)

___

### message

•  **message**: string

___

### path

•  **path**: string[]

___

### type

•  **type**: string
