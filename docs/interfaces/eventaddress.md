**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / EventAddress

# Interface: EventAddress

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **EventAddress**

## Index

### Properties

* [addressLine1](eventaddress.md#addressline1)
* [addressLine2](eventaddress.md#addressline2)
* [city](eventaddress.md#city)
* [country](eventaddress.md#country)
* [latitude](eventaddress.md#latitude)
* [longitude](eventaddress.md#longitude)
* [postalCode](eventaddress.md#postalcode)
* [stateProvinceCode](eventaddress.md#stateprovincecode)

## Properties

### addressLine1

•  **addressLine1**: string

___

### addressLine2

• `Optional` **addressLine2**: string

___

### city

•  **city**: string

___

### country

•  **country**: string

___

### latitude

• `Optional` **latitude**: number

___

### longitude

• `Optional` **longitude**: number

___

### postalCode

•  **postalCode**: string

___

### stateProvinceCode

•  **stateProvinceCode**: string
