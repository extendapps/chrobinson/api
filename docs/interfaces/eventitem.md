**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / EventItem

# Interface: EventItem

## Hierarchy

* **EventItem**

## Index

### Properties

* [actualPackagingHeight](eventitem.md#actualpackagingheight)
* [actualPackagingLength](eventitem.md#actualpackaginglength)
* [actualPackagingUnitOfMeasure](eventitem.md#actualpackagingunitofmeasure)
* [actualPackagingWidth](eventitem.md#actualpackagingwidth)
* [actualPallets](eventitem.md#actualpallets)
* [actualQuantity](eventitem.md#actualquantity)
* [actualVolume](eventitem.md#actualvolume)
* [actualVolumeUnitOfMeasure](eventitem.md#actualvolumeunitofmeasure)
* [actualWeight](eventitem.md#actualweight)
* [actualWeightUnitOfMeasure](eventitem.md#actualweightunitofmeasure)
* [description](eventitem.md#description)
* [freightClass](eventitem.md#freightclass)
* [insuranceValue](eventitem.md#insurancevalue)
* [packagingType](eventitem.md#packagingtype)
* [pluNumber](eventitem.md#plunumber)
* [poNumber](eventitem.md#ponumber)
* [primaryReferenceNumber](eventitem.md#primaryreferencenumber)
* [productCode](eventitem.md#productcode)
* [referenceNumbers](eventitem.md#referencenumbers)
* [skuNumber](eventitem.md#skunumber)

## Properties

### actualPackagingHeight

• `Optional` **actualPackagingHeight**: string

___

### actualPackagingLength

• `Optional` **actualPackagingLength**: string

___

### actualPackagingUnitOfMeasure

• `Optional` **actualPackagingUnitOfMeasure**: [LinearUnit](../README.md#linearunit)

___

### actualPackagingWidth

• `Optional` **actualPackagingWidth**: string

___

### actualPallets

• `Optional` **actualPallets**: string

___

### actualQuantity

• `Optional` **actualQuantity**: string

___

### actualVolume

• `Optional` **actualVolume**: string

___

### actualVolumeUnitOfMeasure

• `Optional` **actualVolumeUnitOfMeasure**: [VolumeUnit](../README.md#volumeunit)

___

### actualWeight

• `Optional` **actualWeight**: number

___

### actualWeightUnitOfMeasure

• `Optional` **actualWeightUnitOfMeasure**: [WeightUnit](../README.md#weightunit)

___

### description

• `Optional` **description**: string

___

### freightClass

• `Optional` **freightClass**: [FreightClass](../README.md#freightclass)

___

### insuranceValue

• `Optional` **insuranceValue**: number

___

### packagingType

•  **packagingType**: [PackagingCode](../README.md#packagingcode)

___

### pluNumber

• `Optional` **pluNumber**: string

___

### poNumber

• `Optional` **poNumber**: string

___

### primaryReferenceNumber

• `Optional` **primaryReferenceNumber**: string

___

### productCode

• `Optional` **productCode**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### skuNumber

• `Optional` **skuNumber**: string
