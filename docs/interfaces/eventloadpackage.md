**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / EventLoadPackage

# Interface: EventLoadPackage

## Hierarchy

* **EventLoadPackage**

## Index

### Properties

* [packageType](eventloadpackage.md#packagetype)
* [referenceNumbers](eventloadpackage.md#referencenumbers)
* [trackingNumber](eventloadpackage.md#trackingnumber)

## Properties

### packageType

• `Optional` **packageType**: string

___

### referenceNumbers

•  **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### trackingNumber

• `Optional` **trackingNumber**: string
