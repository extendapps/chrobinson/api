**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / EventLocation

# Interface: EventLocation

## Hierarchy

* **EventLocation**

## Index

### Properties

* [address](eventlocation.md#address)
* [appointmentNumber](eventlocation.md#appointmentnumber)
* [arrivalDate](eventlocation.md#arrivaldate)
* [arrivalDateTime](eventlocation.md#arrivaldatetime)
* [blindLocation](eventlocation.md#blindlocation)
* [chrLocationId](eventlocation.md#chrlocationid)
* [city](eventlocation.md#city)
* [closeDateTime](eventlocation.md#closedatetime)
* [contacts](eventlocation.md#contacts)
* [country](eventlocation.md#country)
* [customerLocationId](eventlocation.md#customerlocationid)
* [deliveryNumber](eventlocation.md#deliverynumber)
* [departureDate](eventlocation.md#departuredate)
* [departureDateTime](eventlocation.md#departuredatetime)
* [etaDateTime](eventlocation.md#etadatetime)
* [hasDropTrailer](eventlocation.md#hasdroptrailer)
* [isDropTrailerNeeded](eventlocation.md#isdroptrailerneeded)
* [latitude](eventlocation.md#latitude)
* [loadingTypeCode](eventlocation.md#loadingtypecode)
* [locationContact](eventlocation.md#locationcontact)
* [longitude](eventlocation.md#longitude)
* [name](eventlocation.md#name)
* [openDateTime](eventlocation.md#opendatetime)
* [pickUpNumber](eventlocation.md#pickupnumber)
* [readyAvailableDateTime](eventlocation.md#readyavailabledatetime)
* [referenceNumbers](eventlocation.md#referencenumbers)
* [rsDateTime](eventlocation.md#rsdatetime)
* [scalesRequired](eventlocation.md#scalesrequired)
* [scheduledCloseDateTime](eventlocation.md#scheduledclosedatetime)
* [scheduledEnteredDate](eventlocation.md#scheduledentereddate)
* [scheduledOpenDateTime](eventlocation.md#scheduledopendatetime)
* [scheduledWith](eventlocation.md#scheduledwith)
* [sequenceNumber](eventlocation.md#sequencenumber)
* [specialInstructions](eventlocation.md#specialinstructions)
* [specialRequirement](eventlocation.md#specialrequirement)
* [stateProvinceCode](eventlocation.md#stateprovincecode)
* [stopSequenceNumber](eventlocation.md#stopsequencenumber)
* [style](eventlocation.md#style)
* [type](eventlocation.md#type)

## Properties

### address

• `Optional` **address**: [Address](address.md)

___

### appointmentNumber

• `Optional` **appointmentNumber**: string

___

### arrivalDate

• `Optional` **arrivalDate**: string

___

### arrivalDateTime

• `Optional` **arrivalDateTime**: string

___

### blindLocation

• `Optional` **blindLocation**: [BlindLocation](blindlocation.md)

___

### chrLocationId

• `Optional` **chrLocationId**: string

___

### city

• `Optional` **city**: string

___

### closeDateTime

• `Optional` **closeDateTime**: string

___

### contacts

• `Optional` **contacts**: [Contact](contact.md)[]

___

### country

• `Optional` **country**: \"US\"

___

### customerLocationId

• `Optional` **customerLocationId**: string

___

### deliveryNumber

• `Optional` **deliveryNumber**: string

___

### departureDate

• `Optional` **departureDate**: string

___

### departureDateTime

• `Optional` **departureDateTime**: string

___

### etaDateTime

• `Optional` **etaDateTime**: string

___

### hasDropTrailer

• `Optional` **hasDropTrailer**: boolean

___

### isDropTrailerNeeded

• `Optional` **isDropTrailerNeeded**: boolean

___

### latitude

• `Optional` **latitude**: number

___

### loadingTypeCode

• `Optional` **loadingTypeCode**: [LoadingType](../README.md#loadingtype)

___

### locationContact

• `Optional` **locationContact**: [Contact](contact.md)

___

### longitude

• `Optional` **longitude**: number

___

### name

• `Optional` **name**: string

___

### openDateTime

• `Optional` **openDateTime**: string

___

### pickUpNumber

• `Optional` **pickUpNumber**: string

___

### readyAvailableDateTime

• `Optional` **readyAvailableDateTime**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### rsDateTime

• `Optional` **rsDateTime**: string

___

### scalesRequired

• `Optional` **scalesRequired**: boolean

___

### scheduledCloseDateTime

• `Optional` **scheduledCloseDateTime**: string

___

### scheduledEnteredDate

• `Optional` **scheduledEnteredDate**: string

___

### scheduledOpenDateTime

• `Optional` **scheduledOpenDateTime**: string

___

### scheduledWith

• `Optional` **scheduledWith**: string

___

### sequenceNumber

• `Optional` **sequenceNumber**: number

___

### specialInstructions

• `Optional` **specialInstructions**: string

___

### specialRequirement

• `Optional` **specialRequirement**: [SpecialRequirement](specialrequirement.md)

___

### stateProvinceCode

• `Optional` **stateProvinceCode**: string

___

### stopSequenceNumber

• `Optional` **stopSequenceNumber**: number

___

### style

• `Optional` **style**: string

___

### type

• `Optional` **type**: \"origin\" \| \"destination\" \| \"Pick\" \| \"Drop\"
