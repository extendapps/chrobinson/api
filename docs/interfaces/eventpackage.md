**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / EventPackage

# Interface: EventPackage

## Hierarchy

* **EventPackage**

## Index

### Properties

* [ediCode](eventpackage.md#edicode)
* [note](eventpackage.md#note)
* [packageType](eventpackage.md#packagetype)
* [referenceNumbers](eventpackage.md#referencenumbers)
* [trackingNumber](eventpackage.md#trackingnumber)

## Properties

### ediCode

•  **ediCode**: string

___

### note

•  **note**: string

___

### packageType

• `Optional` **packageType**: string

___

### referenceNumbers

•  **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### trackingNumber

• `Optional` **trackingNumber**: string
