**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / EventRetrievalOptions

# Interface: EventRetrievalOptions

## Hierarchy

* BaseOptions

  ↳ **EventRetrievalOptions**

## Index

### Properties

* [billToReferenceNumber](eventretrievaloptions.md#billtoreferencenumber)
* [customerReferenceNumber](eventretrievaloptions.md#customerreferencenumber)
* [eventType](eventretrievaloptions.md#eventtype)
* [from](eventretrievaloptions.md#from)
* [loadNumber](eventretrievaloptions.md#loadnumber)
* [navisphereTrackingNumber](eventretrievaloptions.md#navispheretrackingnumber)
* [orderNumber](eventretrievaloptions.md#ordernumber)
* [skip](eventretrievaloptions.md#skip)
* [take](eventretrievaloptions.md#take)
* [to](eventretrievaloptions.md#to)

### Methods

* [BadRequest](eventretrievaloptions.md#badrequest)
* [Failed](eventretrievaloptions.md#failed)
* [OK](eventretrievaloptions.md#ok)

## Properties

### billToReferenceNumber

• `Optional` **billToReferenceNumber**: string

___

### customerReferenceNumber

• `Optional` **customerReferenceNumber**: string

___

### eventType

• `Optional` **eventType**: [EventType](../README.md#eventtype)

___

### from

• `Optional` **from**: string

___

### loadNumber

• `Optional` **loadNumber**: string

___

### navisphereTrackingNumber

• `Optional` **navisphereTrackingNumber**: string

___

### orderNumber

• `Optional` **orderNumber**: string

___

### skip

• `Optional` **skip**: number

___

### take

• `Optional` **take**: number

___

### to

• `Optional` **to**: string

## Methods

### BadRequest

▸ **BadRequest**(`errorMessages`: [ErrorMessage](errormessage.md)[]): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`errorMessages` | [ErrorMessage](errormessage.md)[] |

**Returns:** void

___

### Failed

▸ **Failed**(`clientResponse`: ClientResponse): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`clientResponse` | ClientResponse |

**Returns:** void

___

### OK

▸ **OK**(`events`: [NavisphereEvent](navisphereevent.md)[]): void

#### Parameters:

Name | Type |
------ | ------ |
`events` | [NavisphereEvent](navisphereevent.md)[] |

**Returns:** void
