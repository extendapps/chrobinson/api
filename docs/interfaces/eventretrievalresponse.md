**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / EventRetrievalResponse

# Interface: EventRetrievalResponse

## Hierarchy

* **EventRetrievalResponse**

## Index

### Properties

* [results](eventretrievalresponse.md#results)
* [totalCount](eventretrievalresponse.md#totalcount)

## Properties

### results

• `Optional` **results**: [NavisphereEvent](navisphereevent.md)[]

___

### totalCount

• `Optional` **totalCount**: number
