**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / GeneratedBOLRetrievalOptions

# Interface: GeneratedBOLRetrievalOptions

## Hierarchy

* BaseOptions

  ↳ **GeneratedBOLRetrievalOptions**

## Index

### Properties

* [customerCode](generatedbolretrievaloptions.md#customercode)
* [loadNumber](generatedbolretrievaloptions.md#loadnumber)
* [orderNumber](generatedbolretrievaloptions.md#ordernumber)
* [showItemNotes](generatedbolretrievaloptions.md#showitemnotes)

### Methods

* [BadRequest](generatedbolretrievaloptions.md#badrequest)
* [Created](generatedbolretrievaloptions.md#created)
* [Failed](generatedbolretrievaloptions.md#failed)

## Properties

### customerCode

•  **customerCode**: string

___

### loadNumber

•  **loadNumber**: number

___

### orderNumber

•  **orderNumber**: number

___

### showItemNotes

• `Optional` **showItemNotes**: boolean

## Methods

### BadRequest

▸ **BadRequest**(`errorMessages`: [ErrorMessage](errormessage.md)[]): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`errorMessages` | [ErrorMessage](errormessage.md)[] |

**Returns:** void

___

### Created

▸ **Created**(`clientResponse`: ClientResponse): void

#### Parameters:

Name | Type |
------ | ------ |
`clientResponse` | ClientResponse |

**Returns:** void

___

### Failed

▸ **Failed**(`clientResponse`: ClientResponse): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`clientResponse` | ClientResponse |

**Returns:** void
