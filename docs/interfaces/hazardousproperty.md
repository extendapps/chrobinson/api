**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / HazardousProperty

# Interface: HazardousProperty

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **HazardousProperty**

## Index

### Properties

* [authCode](hazardousproperty.md#authcode)
* [classCode](hazardousproperty.md#classcode)
* [description](hazardousproperty.md#description)
* [emergencyPhone](hazardousproperty.md#emergencyphone)
* [isNOSCommodity](hazardousproperty.md#isnoscommodity)
* [isPlacarded](hazardousproperty.md#isplacarded)
* [isPrimaryCommodity](hazardousproperty.md#isprimarycommodity)
* [isReportableQuantity](hazardousproperty.md#isreportablequantity)
* [packagingGroup](hazardousproperty.md#packaginggroup)
* [technicalName](hazardousproperty.md#technicalname)
* [unNumber](hazardousproperty.md#unnumber)

## Properties

### authCode

• `Optional` **authCode**: \"C\" \| \"D\" \| \"I\"

___

### classCode

• `Optional` **classCode**: string

___

### description

•  **description**: string

___

### emergencyPhone

• `Optional` **emergencyPhone**: string

___

### isNOSCommodity

• `Optional` **isNOSCommodity**: boolean

___

### isPlacarded

• `Optional` **isPlacarded**: boolean

___

### isPrimaryCommodity

• `Optional` **isPrimaryCommodity**: boolean

___

### isReportableQuantity

• `Optional` **isReportableQuantity**: boolean

___

### packagingGroup

• `Optional` **packagingGroup**: \"I\" \| \"II\" \| \"III\" \| string

___

### technicalName

• `Optional` **technicalName**: string

___

### unNumber

• `Optional` **unNumber**: string
