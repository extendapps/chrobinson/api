**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / InTransitAddress

# Interface: InTransitAddress

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **InTransitAddress**

## Index

### Properties

* [city](intransitaddress.md#city)
* [country](intransitaddress.md#country)
* [latitude](intransitaddress.md#latitude)
* [longitude](intransitaddress.md#longitude)
* [stateProvinceCode](intransitaddress.md#stateprovincecode)

## Properties

### city

•  **city**: string

___

### country

•  **country**: string

___

### latitude

• `Optional` **latitude**: number

___

### longitude

• `Optional` **longitude**: number

___

### stateProvinceCode

•  **stateProvinceCode**: string
