**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / InTransitEvent

# Interface: InTransitEvent

## Hierarchy

* [SingleLoadEvent](singleloadevent.md)

  ↳ **InTransitEvent**

## Index

### Properties

* [carrier](intransitevent.md#carrier)
* [eventType](intransitevent.md#eventtype)
* [loadNumber](intransitevent.md#loadnumber)
* [location](intransitevent.md#location)
* [notes](intransitevent.md#notes)
* [orderDetails](intransitevent.md#orderdetails)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

___

### eventType

•  **eventType**: \"IN TRANSIT\"

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [InTransitAddress](intransitaddress.md)

___

### notes

•  **notes**: string

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*
