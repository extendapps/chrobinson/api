**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / ItemEquipment

# Interface: ItemEquipment

## Hierarchy

* **ItemEquipment**

## Index

### Properties

* [coilRacks](itemequipment.md#coilracks)
* [isOwOd](itemequipment.md#isowod)
* [itemEquipmentCode](itemequipment.md#itemequipmentcode)
* [loadingTypeCode](itemequipment.md#loadingtypecode)
* [locksBars](itemequipment.md#locksbars)
* [maxTrailerAge](itemequipment.md#maxtrailerage)
* [requiredChainsCount](itemequipment.md#requiredchainscount)
* [requiredStrapsCount](itemequipment.md#requiredstrapscount)
* [safetyEquipmentRequired](itemequipment.md#safetyequipmentrequired)
* [tarpTypeCode](itemequipment.md#tarptypecode)

## Properties

### coilRacks

• `Optional` **coilRacks**: number

___

### isOwOd

• `Optional` **isOwOd**: boolean

___

### itemEquipmentCode

• `Optional` **itemEquipmentCode**: string

___

### loadingTypeCode

• `Optional` **loadingTypeCode**: [LoadingType](../README.md#loadingtype)

___

### locksBars

• `Optional` **locksBars**: number

___

### maxTrailerAge

• `Optional` **maxTrailerAge**: number

___

### requiredChainsCount

• `Optional` **requiredChainsCount**: number

___

### requiredStrapsCount

• `Optional` **requiredStrapsCount**: number

___

### safetyEquipmentRequired

• `Optional` **safetyEquipmentRequired**: boolean

___

### tarpTypeCode

• `Optional` **tarpTypeCode**: \"10\" \| \"2\" \| \"4\" \| \"6\" \| \"8\" \| \"MESH\" \| \"NO \|TARP\" \| \"SMOKE\" \| \"TOP\"
