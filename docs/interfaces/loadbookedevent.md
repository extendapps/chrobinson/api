**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / LoadBookedEvent

# Interface: LoadBookedEvent

## Hierarchy

* [LoadEvent](loadevent.md)

  ↳ **LoadBookedEvent**

## Index

### Properties

* [billTos](loadbookedevent.md#billtos)
* [carriers](loadbookedevent.md#carriers)
* [customers](loadbookedevent.md#customers)
* [deliveryAvailableByDate](loadbookedevent.md#deliveryavailablebydate)
* [deliveryByDate](loadbookedevent.md#deliverybydate)
* [equipmentType](loadbookedevent.md#equipmenttype)
* [eventType](loadbookedevent.md#eventtype)
* [items](loadbookedevent.md#items)
* [loadNumber](loadbookedevent.md#loadnumber)
* [locations](loadbookedevent.md#locations)
* [mode](loadbookedevent.md#mode)
* [orderDetails](loadbookedevent.md#orderdetails)
* [packages](loadbookedevent.md#packages)
* [pickupByDate](loadbookedevent.md#pickupbydate)
* [readyByDate](loadbookedevent.md#readybydate)
* [serviceOffering](loadbookedevent.md#serviceoffering)

## Properties

### billTos

•  **billTos**: [BookedBillTo](bookedbillto.md)[]

*Inherited from [LoadEvent](loadevent.md).[billTos](loadevent.md#billtos)*

___

### carriers

•  **carriers**: [Carrier](carrier.md)[]

*Inherited from [LoadEvent](loadevent.md).[carriers](loadevent.md#carriers)*

___

### customers

• `Optional` **customers**: [BookedCustomer](bookedcustomer.md)[]

*Inherited from [LoadEvent](loadevent.md).[customers](loadevent.md#customers)*

___

### deliveryAvailableByDate

•  **deliveryAvailableByDate**: string

*Inherited from [LoadEvent](loadevent.md).[deliveryAvailableByDate](loadevent.md#deliveryavailablebydate)*

___

### deliveryByDate

•  **deliveryByDate**: string

*Inherited from [LoadEvent](loadevent.md).[deliveryByDate](loadevent.md#deliverybydate)*

___

### equipmentType

•  **equipmentType**: string

*Inherited from [LoadEvent](loadevent.md).[equipmentType](loadevent.md#equipmenttype)*

___

### eventType

•  **eventType**: \"LOAD BOOKED\"

*Overrides [LoadEvent](loadevent.md).[eventType](loadevent.md#eventtype)*

___

### items

•  **items**: [OrderItem](orderitem.md)[]

*Inherited from [LoadEvent](loadevent.md).[items](loadevent.md#items)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### locations

•  **locations**: Location[]

*Inherited from [LoadEvent](loadevent.md).[locations](loadevent.md#locations)*

___

### mode

•  **mode**: string

*Inherited from [LoadEvent](loadevent.md).[mode](loadevent.md#mode)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [OrderPackage](orderpackage.md)[]

*Inherited from [LoadEvent](loadevent.md).[packages](loadevent.md#packages)*

___

### pickupByDate

•  **pickupByDate**: string

*Inherited from [LoadEvent](loadevent.md).[pickupByDate](loadevent.md#pickupbydate)*

___

### readyByDate

•  **readyByDate**: string

*Inherited from [LoadEvent](loadevent.md).[readyByDate](loadevent.md#readybydate)*

___

### serviceOffering

•  **serviceOffering**: string

*Inherited from [LoadEvent](loadevent.md).[serviceOffering](loadevent.md#serviceoffering)*
