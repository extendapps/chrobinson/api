**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / LoadCreatedEvent

# Interface: LoadCreatedEvent

## Hierarchy

* [LoadEvent](loadevent.md)

  ↳ **LoadCreatedEvent**

## Index

### Properties

* [billTos](loadcreatedevent.md#billtos)
* [carriers](loadcreatedevent.md#carriers)
* [customers](loadcreatedevent.md#customers)
* [deliveryAvailableByDate](loadcreatedevent.md#deliveryavailablebydate)
* [deliveryByDate](loadcreatedevent.md#deliverybydate)
* [equipmentType](loadcreatedevent.md#equipmenttype)
* [eventType](loadcreatedevent.md#eventtype)
* [items](loadcreatedevent.md#items)
* [loadNumber](loadcreatedevent.md#loadnumber)
* [locations](loadcreatedevent.md#locations)
* [mode](loadcreatedevent.md#mode)
* [orderDetails](loadcreatedevent.md#orderdetails)
* [packages](loadcreatedevent.md#packages)
* [pickupByDate](loadcreatedevent.md#pickupbydate)
* [readyByDate](loadcreatedevent.md#readybydate)
* [serviceOffering](loadcreatedevent.md#serviceoffering)

## Properties

### billTos

•  **billTos**: [BookedBillTo](bookedbillto.md)[]

*Inherited from [LoadEvent](loadevent.md).[billTos](loadevent.md#billtos)*

___

### carriers

•  **carriers**: [Carrier](carrier.md)[]

*Inherited from [LoadEvent](loadevent.md).[carriers](loadevent.md#carriers)*

___

### customers

• `Optional` **customers**: [BookedCustomer](bookedcustomer.md)[]

*Inherited from [LoadEvent](loadevent.md).[customers](loadevent.md#customers)*

___

### deliveryAvailableByDate

•  **deliveryAvailableByDate**: string

*Inherited from [LoadEvent](loadevent.md).[deliveryAvailableByDate](loadevent.md#deliveryavailablebydate)*

___

### deliveryByDate

•  **deliveryByDate**: string

*Inherited from [LoadEvent](loadevent.md).[deliveryByDate](loadevent.md#deliverybydate)*

___

### equipmentType

•  **equipmentType**: string

*Inherited from [LoadEvent](loadevent.md).[equipmentType](loadevent.md#equipmenttype)*

___

### eventType

•  **eventType**: \"LOAD CREATED\"

*Overrides [LoadEvent](loadevent.md).[eventType](loadevent.md#eventtype)*

___

### items

•  **items**: [OrderItem](orderitem.md)[]

*Inherited from [LoadEvent](loadevent.md).[items](loadevent.md#items)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### locations

•  **locations**: Location[]

*Inherited from [LoadEvent](loadevent.md).[locations](loadevent.md#locations)*

___

### mode

•  **mode**: string

*Inherited from [LoadEvent](loadevent.md).[mode](loadevent.md#mode)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [OrderPackage](orderpackage.md)[]

*Inherited from [LoadEvent](loadevent.md).[packages](loadevent.md#packages)*

___

### pickupByDate

•  **pickupByDate**: string

*Inherited from [LoadEvent](loadevent.md).[pickupByDate](loadevent.md#pickupbydate)*

___

### readyByDate

•  **readyByDate**: string

*Inherited from [LoadEvent](loadevent.md).[readyByDate](loadevent.md#readybydate)*

___

### serviceOffering

•  **serviceOffering**: string

*Inherited from [LoadEvent](loadevent.md).[serviceOffering](loadevent.md#serviceoffering)*
