**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / LoadDeliveredEvent

# Interface: LoadDeliveredEvent

## Hierarchy

* [StopEvent](stopevent.md)

  ↳ **LoadDeliveredEvent**

## Index

### Properties

* [carrier](loaddeliveredevent.md#carrier)
* [eventType](loaddeliveredevent.md#eventtype)
* [items](loaddeliveredevent.md#items)
* [lateReasonCode](loaddeliveredevent.md#latereasoncode)
* [loadNumber](loaddeliveredevent.md#loadnumber)
* [location](loaddeliveredevent.md#location)
* [orderDetails](loaddeliveredevent.md#orderdetails)
* [packages](loaddeliveredevent.md#packages)
* [proofOfDelivery](loaddeliveredevent.md#proofofdelivery)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

*Inherited from [StopEvent](stopevent.md).[carrier](stopevent.md#carrier)*

___

### eventType

•  **eventType**: \"LOAD DELIVERED\"

*Overrides [StopEvent](stopevent.md).[eventType](stopevent.md#eventtype)*

___

### items

•  **items**: [EventItem](eventitem.md)[]

*Inherited from [StopEvent](stopevent.md).[items](stopevent.md#items)*

___

### lateReasonCode

•  **lateReasonCode**: string

*Inherited from [StopEvent](stopevent.md).[lateReasonCode](stopevent.md#latereasoncode)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

*Inherited from [StopEvent](stopevent.md).[location](stopevent.md#location)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventLoadPackage](eventloadpackage.md)[]

*Inherited from [StopEvent](stopevent.md).[packages](stopevent.md#packages)*

___

### proofOfDelivery

•  **proofOfDelivery**: string

*Inherited from [StopEvent](stopevent.md).[proofOfDelivery](stopevent.md#proofofdelivery)*
