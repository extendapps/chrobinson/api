**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / LoadEvent

# Interface: LoadEvent

## Hierarchy

* [SingleLoadEvent](singleloadevent.md)

  ↳ **LoadEvent**

  ↳↳ [LoadCreatedEvent](loadcreatedevent.md)

  ↳↳ [LoadBookedEvent](loadbookedevent.md)

  ↳↳ [LoadCancelledEvent](loadcancelledevent.md)

## Index

### Properties

* [billTos](loadevent.md#billtos)
* [carriers](loadevent.md#carriers)
* [customers](loadevent.md#customers)
* [deliveryAvailableByDate](loadevent.md#deliveryavailablebydate)
* [deliveryByDate](loadevent.md#deliverybydate)
* [equipmentType](loadevent.md#equipmenttype)
* [eventType](loadevent.md#eventtype)
* [items](loadevent.md#items)
* [loadNumber](loadevent.md#loadnumber)
* [locations](loadevent.md#locations)
* [mode](loadevent.md#mode)
* [orderDetails](loadevent.md#orderdetails)
* [packages](loadevent.md#packages)
* [pickupByDate](loadevent.md#pickupbydate)
* [readyByDate](loadevent.md#readybydate)
* [serviceOffering](loadevent.md#serviceoffering)

## Properties

### billTos

•  **billTos**: [BookedBillTo](bookedbillto.md)[]

___

### carriers

•  **carriers**: [Carrier](carrier.md)[]

___

### customers

• `Optional` **customers**: [BookedCustomer](bookedcustomer.md)[]

___

### deliveryAvailableByDate

•  **deliveryAvailableByDate**: string

___

### deliveryByDate

•  **deliveryByDate**: string

___

### equipmentType

•  **equipmentType**: string

___

### eventType

•  **eventType**: \"LOAD CREATED\" \| \"LOAD BOOKED\" \| \"LOAD CANCELLED\"

___

### items

•  **items**: [OrderItem](orderitem.md)[]

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### locations

•  **locations**: Location[]

___

### mode

•  **mode**: string

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [OrderPackage](orderpackage.md)[]

___

### pickupByDate

•  **pickupByDate**: string

___

### readyByDate

•  **readyByDate**: string

___

### serviceOffering

•  **serviceOffering**: string
