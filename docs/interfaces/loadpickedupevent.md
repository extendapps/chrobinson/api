**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / LoadPickedUpEvent

# Interface: LoadPickedUpEvent

## Hierarchy

* [StopEvent](stopevent.md)

  ↳ **LoadPickedUpEvent**

## Index

### Properties

* [carrier](loadpickedupevent.md#carrier)
* [eventType](loadpickedupevent.md#eventtype)
* [items](loadpickedupevent.md#items)
* [lateReasonCode](loadpickedupevent.md#latereasoncode)
* [loadNumber](loadpickedupevent.md#loadnumber)
* [location](loadpickedupevent.md#location)
* [orderDetails](loadpickedupevent.md#orderdetails)
* [packages](loadpickedupevent.md#packages)
* [proofOfDelivery](loadpickedupevent.md#proofofdelivery)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

*Inherited from [StopEvent](stopevent.md).[carrier](stopevent.md#carrier)*

___

### eventType

•  **eventType**: \"LOAD PICKED UP\"

*Overrides [StopEvent](stopevent.md).[eventType](stopevent.md#eventtype)*

___

### items

•  **items**: [EventItem](eventitem.md)[]

*Inherited from [StopEvent](stopevent.md).[items](stopevent.md#items)*

___

### lateReasonCode

•  **lateReasonCode**: string

*Inherited from [StopEvent](stopevent.md).[lateReasonCode](stopevent.md#latereasoncode)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

*Inherited from [StopEvent](stopevent.md).[location](stopevent.md#location)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventLoadPackage](eventloadpackage.md)[]

*Inherited from [StopEvent](stopevent.md).[packages](stopevent.md#packages)*

___

### proofOfDelivery

•  **proofOfDelivery**: string

*Inherited from [StopEvent](stopevent.md).[proofOfDelivery](stopevent.md#proofofdelivery)*
