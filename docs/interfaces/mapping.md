**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Mapping

# Interface: Mapping

## Hierarchy

* **Mapping**

## Index

### Properties

* [claimCreatedUtc](mapping.md#claimcreatedutc)
* [customerName](mapping.md#customername)
* [key](mapping.md#key)
* [value](mapping.md#value)

## Properties

### claimCreatedUtc

•  **claimCreatedUtc**: Date

___

### customerName

•  **customerName**: string

___

### key

•  **key**: string

___

### value

•  **value**: string
