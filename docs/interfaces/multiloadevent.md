**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / MultiLoadEvent

# Interface: MultiLoadEvent

## Hierarchy

* **MultiLoadEvent**

  ↳ [OrderEvent](orderevent.md)

## Index

### Properties

* [loadNumbers](multiloadevent.md#loadnumbers)
* [orderDetail](multiloadevent.md#orderdetail)

## Properties

### loadNumbers

•  **loadNumbers**: number[]

___

### orderDetail

•  **orderDetail**: [OrderDetail](orderdetail.md)[]
