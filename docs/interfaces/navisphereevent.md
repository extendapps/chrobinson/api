**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / NavisphereEvent

# Interface: NavisphereEvent

## Hierarchy

* **NavisphereEvent**

## Index

### Properties

* [billToReferenceNumber](navisphereevent.md#billtoreferencenumber)
* [customer](navisphereevent.md#customer)
* [customerReferenceNumber](navisphereevent.md#customerreferencenumber)
* [event](navisphereevent.md#event)
* [eventTime](navisphereevent.md#eventtime)
* [time](navisphereevent.md#time)

## Properties

### billToReferenceNumber

•  **billToReferenceNumber**: string

___

### customer

•  **customer**: string

___

### customerReferenceNumber

•  **customerReferenceNumber**: string

___

### event

•  **event**: [OrderCreatedEvent](ordercreatedevent.md) \| [OrderRejectedEvent](orderrejectedevent.md) \| [OrderUpdatedEvent](orderupdatedevent.md) \| [OrderCanceledEvent](ordercanceledevent.md) \| [OrderCompletedEvent](ordercompletedevent.md) \| [LoadCreatedEvent](loadcreatedevent.md) \| [LoadBookedEvent](loadbookedevent.md) \| [LoadCancelledEvent](loadcancelledevent.md) \| [CarrierArrivedEvent](carrierarrivedevent.md) \| [CarrierDepartedEvent](carrierdepartedevent.md) \| [LoadPickedUpEvent](loadpickedupevent.md) \| [LoadDeliveredEvent](loaddeliveredevent.md) \| [AppointmentUpdatedEvent](appointmentupdatedevent.md) \| [InTransitEvent](intransitevent.md) \| [PackageInTransitEvent](packageintransitevent.md) \| [PackagePickedUpEvent](packagepickedupevent.md) \| [PackageDeliveredEvent](packagedeliveredevent.md)

___

### eventTime

•  **eventTime**: string

___

### time

•  **time**: string
