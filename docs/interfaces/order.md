**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Order

# Interface: Order

## Hierarchy

* **Order**

## Index

### Properties

* [billTos](order.md#billtos)
* [customers](order.md#customers)
* [measurementSystem](order.md#measurementsystem)
* [notes](order.md#notes)
* [orderNumber](order.md#ordernumber)
* [referenceNumbers](order.md#referencenumbers)
* [services](order.md#services)

## Properties

### billTos

•  **billTos**: [OrderBillTo](orderbillto.md)[]

___

### customers

•  **customers**: [OrderCustomer](ordercustomer.md)[]

___

### measurementSystem

• `Optional` **measurementSystem**: [MeasurementSystem](../README.md#measurementsystem)

___

### notes

• `Optional` **notes**: string

___

### orderNumber

• `Optional` **orderNumber**: number

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### services

•  **services**: [OrderService](orderservice.md)[]
