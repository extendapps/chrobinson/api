**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderAddress

# Interface: OrderAddress

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **OrderAddress**

## Index

### Properties

* [address1](orderaddress.md#address1)
* [address2](orderaddress.md#address2)
* [address3](orderaddress.md#address3)
* [address4](orderaddress.md#address4)
* [city](orderaddress.md#city)
* [country](orderaddress.md#country)
* [county](orderaddress.md#county)
* [lat](orderaddress.md#lat)
* [long](orderaddress.md#long)
* [postalCode](orderaddress.md#postalcode)
* [stateProvinceCode](orderaddress.md#stateprovincecode)

## Properties

### address1

•  **address1**: string

___

### address2

• `Optional` **address2**: string

___

### address3

• `Optional` **address3**: string

___

### address4

• `Optional` **address4**: string

___

### city

•  **city**: string

___

### country

•  **country**: string

___

### county

• `Optional` **county**: string

___

### lat

• `Optional` **lat**: number

___

### long

• `Optional` **long**: number

___

### postalCode

•  **postalCode**: string

___

### stateProvinceCode

•  **stateProvinceCode**: string
