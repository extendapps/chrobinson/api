**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderBillTo

# Interface: OrderBillTo

## Hierarchy

* **OrderBillTo**

## Index

### Properties

* [charges](orderbillto.md#charges)
* [contacts](orderbillto.md#contacts)
* [currencyCode](orderbillto.md#currencycode)
* [customerCode](orderbillto.md#customercode)
* [freightTerms](orderbillto.md#freightterms)
* [referenceNumbers](orderbillto.md#referencenumbers)

## Properties

### charges

• `Optional` **charges**: [Charge](charge.md)[]

___

### contacts

• `Optional` **contacts**: [Contact](contact.md)[]

___

### currencyCode

•  **currencyCode**: [CurrencyCode](../README.md#currencycode)

___

### customerCode

•  **customerCode**: string

___

### freightTerms

• `Optional` **freightTerms**: [FreightTerm](../README.md#freightterm)

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]
