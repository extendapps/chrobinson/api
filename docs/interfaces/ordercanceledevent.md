**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderCanceledEvent

# Interface: OrderCanceledEvent

## Hierarchy

* [OrderEvent](orderevent.md)

  ↳ **OrderCanceledEvent**

## Index

### Properties

* [eventType](ordercanceledevent.md#eventtype)
* [loadNumbers](ordercanceledevent.md#loadnumbers)
* [orderDetail](ordercanceledevent.md#orderdetail)
* [referenceNumbers](ordercanceledevent.md#referencenumbers)

## Properties

### eventType

•  **eventType**: \"ORDER CANCELED\"

*Overrides [OrderEvent](orderevent.md).[eventType](orderevent.md#eventtype)*

___

### loadNumbers

•  **loadNumbers**: number[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[loadNumbers](multiloadevent.md#loadnumbers)*

___

### orderDetail

•  **orderDetail**: [OrderDetail](orderdetail.md)[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[orderDetail](multiloadevent.md#orderdetail)*

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

*Inherited from [OrderEvent](orderevent.md).[referenceNumbers](orderevent.md#referencenumbers)*
