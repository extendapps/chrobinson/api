**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderCompletedEvent

# Interface: OrderCompletedEvent

## Hierarchy

* [OrderEvent](orderevent.md)

  ↳ **OrderCompletedEvent**

## Index

### Properties

* [eventType](ordercompletedevent.md#eventtype)
* [loadNumbers](ordercompletedevent.md#loadnumbers)
* [orderDetail](ordercompletedevent.md#orderdetail)
* [referenceNumbers](ordercompletedevent.md#referencenumbers)

## Properties

### eventType

•  **eventType**: \"ORDER COMPLETED\"

*Overrides [OrderEvent](orderevent.md).[eventType](orderevent.md#eventtype)*

___

### loadNumbers

•  **loadNumbers**: number[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[loadNumbers](multiloadevent.md#loadnumbers)*

___

### orderDetail

•  **orderDetail**: [OrderDetail](orderdetail.md)[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[orderDetail](multiloadevent.md#orderdetail)*

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

*Inherited from [OrderEvent](orderevent.md).[referenceNumbers](orderevent.md#referencenumbers)*
