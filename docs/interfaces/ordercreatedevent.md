**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderCreatedEvent

# Interface: OrderCreatedEvent

## Hierarchy

* [OrderEvent](orderevent.md)

  ↳ **OrderCreatedEvent**

## Index

### Properties

* [eventType](ordercreatedevent.md#eventtype)
* [loadNumbers](ordercreatedevent.md#loadnumbers)
* [orderDetail](ordercreatedevent.md#orderdetail)
* [referenceNumbers](ordercreatedevent.md#referencenumbers)

## Properties

### eventType

•  **eventType**: \"ORDER CREATED\"

*Overrides [OrderEvent](orderevent.md).[eventType](orderevent.md#eventtype)*

___

### loadNumbers

•  **loadNumbers**: number[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[loadNumbers](multiloadevent.md#loadnumbers)*

___

### orderDetail

•  **orderDetail**: [OrderDetail](orderdetail.md)[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[orderDetail](multiloadevent.md#orderdetail)*

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

*Inherited from [OrderEvent](orderevent.md).[referenceNumbers](orderevent.md#referencenumbers)*
