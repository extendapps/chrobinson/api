**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderCreateOptions

# Interface: OrderCreateOptions

## Hierarchy

* BaseOptions

  ↳ **OrderCreateOptions**

## Index

### Properties

* [order](ordercreateoptions.md#order)

### Methods

* [BadRequest](ordercreateoptions.md#badrequest)
* [Created](ordercreateoptions.md#created)
* [Failed](ordercreateoptions.md#failed)

## Properties

### order

•  **order**: [Order](order.md)

## Methods

### BadRequest

▸ **BadRequest**(`errorMessages`: [ErrorMessage](errormessage.md)[]): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`errorMessages` | [ErrorMessage](errormessage.md)[] |

**Returns:** void

___

### Created

▸ **Created**(`orderNumber`: number): void

#### Parameters:

Name | Type |
------ | ------ |
`orderNumber` | number |

**Returns:** void

___

### Failed

▸ **Failed**(`clientResponse`: ClientResponse): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`clientResponse` | ClientResponse |

**Returns:** void
