**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderCreateResponse

# Interface: OrderCreateResponse

## Hierarchy

* **OrderCreateResponse**

## Index

### Properties

* [orderNumber](ordercreateresponse.md#ordernumber)

## Properties

### orderNumber

•  **orderNumber**: number
