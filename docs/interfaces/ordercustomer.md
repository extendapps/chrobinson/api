**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderCustomer

# Interface: OrderCustomer

## Hierarchy

* **OrderCustomer**

## Index

### Properties

* [contacts](ordercustomer.md#contacts)
* [customerCode](ordercustomer.md#customercode)
* [referenceNumbers](ordercustomer.md#referencenumbers)

## Properties

### contacts

• `Optional` **contacts**: [Contact](contact.md)[]

___

### customerCode

•  **customerCode**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]
