**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderDeleteOptions

# Interface: OrderDeleteOptions

## Hierarchy

* BaseOptions

  ↳ **OrderDeleteOptions**

## Index

### Properties

* [orderNumber](orderdeleteoptions.md#ordernumber)

### Methods

* [BadRequest](orderdeleteoptions.md#badrequest)
* [Deleted](orderdeleteoptions.md#deleted)
* [Failed](orderdeleteoptions.md#failed)

## Properties

### orderNumber

•  **orderNumber**: number

## Methods

### BadRequest

▸ **BadRequest**(`errorMessages`: [ErrorMessage](errormessage.md)[]): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`errorMessages` | [ErrorMessage](errormessage.md)[] |

**Returns:** void

___

### Deleted

▸ **Deleted**(): void

**Returns:** void

___

### Failed

▸ **Failed**(`clientResponse`: ClientResponse): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`clientResponse` | ClientResponse |

**Returns:** void
