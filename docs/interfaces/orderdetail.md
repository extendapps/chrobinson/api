**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderDetail

# Interface: OrderDetail

## Hierarchy

* **OrderDetail**

## Index

### Properties

* [billToReferenceNumber](orderdetail.md#billtoreferencenumber)
* [condition](orderdetail.md#condition)
* [customerReferenceNumber](orderdetail.md#customerreferencenumber)
* [navisphereTrackingLink](orderdetail.md#navispheretrackinglink)
* [navisphereTrackingNumber](orderdetail.md#navispheretrackingnumber)
* [orderNumber](orderdetail.md#ordernumber)
* [services](orderdetail.md#services)

## Properties

### billToReferenceNumber

• `Optional` **billToReferenceNumber**: string

___

### condition

• `Optional` **condition**: [Condition](../README.md#condition)

___

### customerReferenceNumber

• `Optional` **customerReferenceNumber**: string

___

### navisphereTrackingLink

• `Optional` **navisphereTrackingLink**: string

___

### navisphereTrackingNumber

• `Optional` **navisphereTrackingNumber**: string

___

### orderNumber

• `Optional` **orderNumber**: number

___

### services

• `Optional` **services**: [OrderEventService](ordereventservice.md)[]
