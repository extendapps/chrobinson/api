**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderEvent

# Interface: OrderEvent

## Hierarchy

* [MultiLoadEvent](multiloadevent.md)

  ↳ **OrderEvent**

  ↳↳ [OrderCreatedEvent](ordercreatedevent.md)

  ↳↳ [OrderRejectedEvent](orderrejectedevent.md)

  ↳↳ [OrderUpdatedEvent](orderupdatedevent.md)

  ↳↳ [OrderCanceledEvent](ordercanceledevent.md)

  ↳↳ [OrderCompletedEvent](ordercompletedevent.md)

## Index

### Properties

* [eventType](orderevent.md#eventtype)
* [loadNumbers](orderevent.md#loadnumbers)
* [orderDetail](orderevent.md#orderdetail)
* [referenceNumbers](orderevent.md#referencenumbers)

## Properties

### eventType

•  **eventType**: \"ORDER CREATED\" \| \"ORDER REJECTED\" \| \"ORDER UPDATED\" \| \"ORDER CANCELED\" \| \"ORDER COMPLETED\"

___

### loadNumbers

•  **loadNumbers**: number[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[loadNumbers](multiloadevent.md#loadnumbers)*

___

### orderDetail

•  **orderDetail**: [OrderDetail](orderdetail.md)[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[orderDetail](multiloadevent.md#orderdetail)*

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]
