**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderEventItem

# Interface: OrderEventItem

## Hierarchy

* **OrderEventItem**

## Index

### Properties

* [description](ordereventitem.md#description)
* [freightClass](ordereventitem.md#freightclass)
* [height](ordereventitem.md#height)
* [heightUnitOfMeasure](ordereventitem.md#heightunitofmeasure)
* [insuranceValue](ordereventitem.md#insurancevalue)
* [length](ordereventitem.md#length)
* [lengthUnitOfMeasure](ordereventitem.md#lengthunitofmeasure)
* [maxWeight](ordereventitem.md#maxweight)
* [maxWeightUnitOfMeasure](ordereventitem.md#maxweightunitofmeasure)
* [minWeight](ordereventitem.md#minweight)
* [minWeightUnitOfMeasure](ordereventitem.md#minweightunitofmeasure)
* [packagingType](ordereventitem.md#packagingtype)
* [palletSpaces](ordereventitem.md#palletspaces)
* [pallets](ordereventitem.md#pallets)
* [pluNumber](ordereventitem.md#plunumber)
* [productCode](ordereventitem.md#productcode)
* [referenceNumbers](ordereventitem.md#referencenumbers)
* [skuNumber](ordereventitem.md#skunumber)
* [trailerLengthUsed](ordereventitem.md#trailerlengthused)
* [trailerLengthUsedUnitOfMeasure](ordereventitem.md#trailerlengthusedunitofmeasure)
* [upcNumber](ordereventitem.md#upcnumber)
* [volume](ordereventitem.md#volume)
* [volumeUnitOfMeasure](ordereventitem.md#volumeunitofmeasure)
* [width](ordereventitem.md#width)
* [widthUnitOfMeasure](ordereventitem.md#widthunitofmeasure)

## Properties

### description

• `Optional` **description**: string

___

### freightClass

• `Optional` **freightClass**: [FreightClass](../README.md#freightclass)

___

### height

• `Optional` **height**: number

___

### heightUnitOfMeasure

• `Optional` **heightUnitOfMeasure**: [LinearUnit](../README.md#linearunit)

___

### insuranceValue

• `Optional` **insuranceValue**: number

___

### length

• `Optional` **length**: number

___

### lengthUnitOfMeasure

• `Optional` **lengthUnitOfMeasure**: [LinearUnit](../README.md#linearunit)

___

### maxWeight

• `Optional` **maxWeight**: number

___

### maxWeightUnitOfMeasure

• `Optional` **maxWeightUnitOfMeasure**: [WeightUnit](../README.md#weightunit)

___

### minWeight

• `Optional` **minWeight**: number

___

### minWeightUnitOfMeasure

• `Optional` **minWeightUnitOfMeasure**: [WeightUnit](../README.md#weightunit)

___

### packagingType

•  **packagingType**: [PackagingCode](../README.md#packagingcode)

___

### palletSpaces

• `Optional` **palletSpaces**: number

___

### pallets

• `Optional` **pallets**: number

___

### pluNumber

• `Optional` **pluNumber**: string

___

### productCode

• `Optional` **productCode**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### skuNumber

• `Optional` **skuNumber**: string

___

### trailerLengthUsed

• `Optional` **trailerLengthUsed**: number

___

### trailerLengthUsedUnitOfMeasure

• `Optional` **trailerLengthUsedUnitOfMeasure**: [LinearUnit](../README.md#linearunit)

___

### upcNumber

• `Optional` **upcNumber**: string

___

### volume

• `Optional` **volume**: number

___

### volumeUnitOfMeasure

• `Optional` **volumeUnitOfMeasure**: [VolumeUnit](../README.md#volumeunit)

___

### width

• `Optional` **width**: number

___

### widthUnitOfMeasure

• `Optional` **widthUnitOfMeasure**: [LinearUnit](../README.md#linearunit)
