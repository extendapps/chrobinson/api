**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderEventLocation

# Interface: OrderEventLocation

## Hierarchy

* **OrderEventLocation**

## Index

### Properties

* [address](ordereventlocation.md#address)
* [chrLocationId](ordereventlocation.md#chrlocationid)
* [customerLocationId](ordereventlocation.md#customerlocationid)
* [name](ordereventlocation.md#name)
* [referenceNumbers](ordereventlocation.md#referencenumbers)
* [style](ordereventlocation.md#style)
* [type](ordereventlocation.md#type)

## Properties

### address

• `Optional` **address**: [EventAddress](eventaddress.md)

___

### chrLocationId

•  **chrLocationId**: string

___

### customerLocationId

• `Optional` **customerLocationId**: string

___

### name

• `Optional` **name**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### style

•  **style**: [OrderEventLocationStyle](../README.md#ordereventlocationstyle)

___

### type

•  **type**: [OrderEventLocationType](../README.md#ordereventlocationtype)
