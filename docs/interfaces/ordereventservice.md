**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderEventService

# Interface: OrderEventService

## Hierarchy

* **OrderEventService**

## Index

### Properties

* [condition](ordereventservice.md#condition)
* [description](ordereventservice.md#description)
* [items](ordereventservice.md#items)
* [loadNumbers](ordereventservice.md#loadnumbers)
* [locations](ordereventservice.md#locations)
* [mode](ordereventservice.md#mode)
* [serviceLevel](ordereventservice.md#servicelevel)

## Properties

### condition

•  **condition**: [Condition](../README.md#condition)

___

### description

•  **description**: string

___

### items

•  **items**: [OrderEventItem](ordereventitem.md)[]

___

### loadNumbers

• `Optional` **loadNumbers**: number[]

___

### locations

•  **locations**: [OrderEventLocation](ordereventlocation.md)[]

___

### mode

•  **mode**: string

___

### serviceLevel

•  **serviceLevel**: string
