**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderItem

# Interface: OrderItem

## Hierarchy

* **OrderItem**

## Index

### Properties

* [actualQuantity](orderitem.md#actualquantity)
* [actualWeight](orderitem.md#actualweight)
* [description](orderitem.md#description)
* [equipmentHeight](orderitem.md#equipmentheight)
* [equipmentLength](orderitem.md#equipmentlength)
* [equipmentUnitOfMeasure](orderitem.md#equipmentunitofmeasure)
* [equipmentWidth](orderitem.md#equipmentwidth)
* [freightClass](orderitem.md#freightclass)
* [hazardousProperties](orderitem.md#hazardousproperties)
* [insuranceValue](orderitem.md#insurancevalue)
* [isStackable](orderitem.md#isstackable)
* [isTemperatureSensitive](orderitem.md#istemperaturesensitive)
* [itemEquipment](orderitem.md#itemequipment)
* [nationalMotorFreightClass](orderitem.md#nationalmotorfreightclass)
* [notes](orderitem.md#notes)
* [packagingHeight](orderitem.md#packagingheight)
* [packagingLength](orderitem.md#packaginglength)
* [packagingType](orderitem.md#packagingtype)
* [packagingUnitOfMeasure](orderitem.md#packagingunitofmeasure)
* [packagingVolume](orderitem.md#packagingvolume)
* [packagingVolumeUnitOfMeasure](orderitem.md#packagingvolumeunitofmeasure)
* [packagingWidth](orderitem.md#packagingwidth)
* [palletSpaces](orderitem.md#palletspaces)
* [pallets](orderitem.md#pallets)
* [pluNumber](orderitem.md#plunumber)
* [productCode](orderitem.md#productcode)
* [quantity](orderitem.md#quantity)
* [referenceNumbers](orderitem.md#referencenumbers)
* [relationships](orderitem.md#relationships)
* [requiredTemperatureHigh](orderitem.md#requiredtemperaturehigh)
* [requiredTemperatureLow](orderitem.md#requiredtemperaturelow)
* [skuNumber](orderitem.md#skunumber)
* [temperatureType](orderitem.md#temperaturetype)
* [temperatureUnit](orderitem.md#temperatureunit)
* [totalTrailerFeetUsed](orderitem.md#totaltrailerfeetused)
* [upcNumber](orderitem.md#upcnumber)
* [weight](orderitem.md#weight)
* [weightUnitOfMeasure](orderitem.md#weightunitofmeasure)

## Properties

### actualQuantity

• `Optional` **actualQuantity**: string

___

### actualWeight

• `Optional` **actualWeight**: number

___

### description

• `Optional` **description**: string

___

### equipmentHeight

• `Optional` **equipmentHeight**: number

___

### equipmentLength

• `Optional` **equipmentLength**: number

___

### equipmentUnitOfMeasure

• `Optional` **equipmentUnitOfMeasure**: [LinearUnit](../README.md#linearunit)

___

### equipmentWidth

• `Optional` **equipmentWidth**: number

___

### freightClass

• `Optional` **freightClass**: [FreightClass](../README.md#freightclass)

___

### hazardousProperties

• `Optional` **hazardousProperties**: [HazardousProperty](hazardousproperty.md)

___

### insuranceValue

• `Optional` **insuranceValue**: number

___

### isStackable

• `Optional` **isStackable**: boolean

___

### isTemperatureSensitive

• `Optional` **isTemperatureSensitive**: boolean

___

### itemEquipment

• `Optional` **itemEquipment**: [ItemEquipment](itemequipment.md)

___

### nationalMotorFreightClass

• `Optional` **nationalMotorFreightClass**: string

___

### notes

• `Optional` **notes**: string

___

### packagingHeight

• `Optional` **packagingHeight**: number

___

### packagingLength

• `Optional` **packagingLength**: number

___

### packagingType

•  **packagingType**: [PackagingCode](../README.md#packagingcode)

___

### packagingUnitOfMeasure

• `Optional` **packagingUnitOfMeasure**: [LinearUnit](../README.md#linearunit)

___

### packagingVolume

• `Optional` **packagingVolume**: number

___

### packagingVolumeUnitOfMeasure

• `Optional` **packagingVolumeUnitOfMeasure**: [VolumeUnit](../README.md#volumeunit)

___

### packagingWidth

• `Optional` **packagingWidth**: number

___

### palletSpaces

• `Optional` **palletSpaces**: number

___

### pallets

• `Optional` **pallets**: number

___

### pluNumber

• `Optional` **pluNumber**: string

___

### productCode

• `Optional` **productCode**: string

___

### quantity

• `Optional` **quantity**: number

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### relationships

• `Optional` **relationships**: [Relationships](relationships.md)

___

### requiredTemperatureHigh

• `Optional` **requiredTemperatureHigh**: number

___

### requiredTemperatureLow

• `Optional` **requiredTemperatureLow**: number

___

### skuNumber

• `Optional` **skuNumber**: string

___

### temperatureType

• `Optional` **temperatureType**: [TemperatureType](../README.md#temperaturetype)

___

### temperatureUnit

• `Optional` **temperatureUnit**: [TemperatureUnit](../README.md#temperatureunit)

___

### totalTrailerFeetUsed

• `Optional` **totalTrailerFeetUsed**: number

___

### upcNumber

• `Optional` **upcNumber**: string

___

### weight

• `Optional` **weight**: number

___

### weightUnitOfMeasure

• `Optional` **weightUnitOfMeasure**: [WeightUnit](../README.md#weightunit)
