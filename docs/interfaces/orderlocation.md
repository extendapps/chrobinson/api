**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderLocation

# Interface: OrderLocation

## Hierarchy

* **OrderLocation**

## Index

### Properties

* [address](orderlocation.md#address)
* [arrivalDate](orderlocation.md#arrivaldate)
* [bindLocation](orderlocation.md#bindlocation)
* [closeDateTime](orderlocation.md#closedatetime)
* [contacts](orderlocation.md#contacts)
* [customerLocationId](orderlocation.md#customerlocationid)
* [departureDate](orderlocation.md#departuredate)
* [hasDropTrailer](orderlocation.md#hasdroptrailer)
* [loadingTypeCode](orderlocation.md#loadingtypecode)
* [name](orderlocation.md#name)
* [openDateTime](orderlocation.md#opendatetime)
* [readyAvailableDateTime](orderlocation.md#readyavailabledatetime)
* [referenceNumbers](orderlocation.md#referencenumbers)
* [rsDateTime](orderlocation.md#rsdatetime)
* [scalesRequired](orderlocation.md#scalesrequired)
* [scheduledCloseDateTime](orderlocation.md#scheduledclosedatetime)
* [scheduledOpenDateTime](orderlocation.md#scheduledopendatetime)
* [specialInstructions](orderlocation.md#specialinstructions)
* [specialRequirement](orderlocation.md#specialrequirement)
* [type](orderlocation.md#type)

## Properties

### address

• `Optional` **address**: [OrderAddress](orderaddress.md)

___

### arrivalDate

• `Optional` **arrivalDate**: string

___

### bindLocation

• `Optional` **bindLocation**: [BlindLocation](blindlocation.md)

___

### closeDateTime

• `Optional` **closeDateTime**: string

___

### contacts

• `Optional` **contacts**: [Contact](contact.md)[]

___

### customerLocationId

• `Optional` **customerLocationId**: string

___

### departureDate

• `Optional` **departureDate**: string

___

### hasDropTrailer

• `Optional` **hasDropTrailer**: boolean

___

### loadingTypeCode

• `Optional` **loadingTypeCode**: [LoadingType](../README.md#loadingtype)

___

### name

• `Optional` **name**: string

___

### openDateTime

• `Optional` **openDateTime**: string

___

### readyAvailableDateTime

• `Optional` **readyAvailableDateTime**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### rsDateTime

• `Optional` **rsDateTime**: string

___

### scalesRequired

• `Optional` **scalesRequired**: boolean

___

### scheduledCloseDateTime

• `Optional` **scheduledCloseDateTime**: string

___

### scheduledOpenDateTime

• `Optional` **scheduledOpenDateTime**: string

___

### specialInstructions

• `Optional` **specialInstructions**: string

___

### specialRequirement

• `Optional` **specialRequirement**: [SpecialRequirement](specialrequirement.md)

___

### type

•  **type**: \"origin\" \| \"destination\"
