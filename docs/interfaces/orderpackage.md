**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderPackage

# Interface: OrderPackage

## Hierarchy

* **OrderPackage**

## Index

### Properties

* [codAmount](orderpackage.md#codamount)
* [declaredValue](orderpackage.md#declaredvalue)
* [height](orderpackage.md#height)
* [length](orderpackage.md#length)
* [notes](orderpackage.md#notes)
* [packageType](orderpackage.md#packagetype)
* [packageUnitOfMeasure](orderpackage.md#packageunitofmeasure)
* [packageVolumeUnitOfMeasure](orderpackage.md#packagevolumeunitofmeasure)
* [parcelPackageType](orderpackage.md#parcelpackagetype)
* [trackingNumber](orderpackage.md#trackingnumber)
* [volume](orderpackage.md#volume)
* [weight](orderpackage.md#weight)
* [weightLb](orderpackage.md#weightlb)
* [width](orderpackage.md#width)

## Properties

### codAmount

• `Optional` **codAmount**: number

___

### declaredValue

• `Optional` **declaredValue**: number

___

### height

• `Optional` **height**: number

___

### length

• `Optional` **length**: number

___

### notes

• `Optional` **notes**: string

___

### packageType

• `Optional` **packageType**: [PackagingCode](../README.md#packagingcode)

___

### packageUnitOfMeasure

• `Optional` **packageUnitOfMeasure**: [LinearUnit](../README.md#linearunit)

___

### packageVolumeUnitOfMeasure

• `Optional` **packageVolumeUnitOfMeasure**: [VolumeUnit](../README.md#volumeunit)

___

### parcelPackageType

• `Optional` **parcelPackageType**: string

___

### trackingNumber

• `Optional` **trackingNumber**: string

___

### volume

• `Optional` **volume**: number

___

### weight

• `Optional` **weight**: number

___

### weightLb

• `Optional` **weightLb**: number

___

### width

• `Optional` **width**: number
