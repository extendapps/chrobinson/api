**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderRejectedEvent

# Interface: OrderRejectedEvent

## Hierarchy

* [OrderEvent](orderevent.md)

  ↳ **OrderRejectedEvent**

## Index

### Properties

* [eventType](orderrejectedevent.md#eventtype)
* [loadNumbers](orderrejectedevent.md#loadnumbers)
* [orderDetail](orderrejectedevent.md#orderdetail)
* [referenceNumbers](orderrejectedevent.md#referencenumbers)

## Properties

### eventType

•  **eventType**: \"ORDER REJECTED\"

*Overrides [OrderEvent](orderevent.md).[eventType](orderevent.md#eventtype)*

___

### loadNumbers

•  **loadNumbers**: number[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[loadNumbers](multiloadevent.md#loadnumbers)*

___

### orderDetail

•  **orderDetail**: [OrderDetail](orderdetail.md)[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[orderDetail](multiloadevent.md#orderdetail)*

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

*Inherited from [OrderEvent](orderevent.md).[referenceNumbers](orderevent.md#referencenumbers)*
