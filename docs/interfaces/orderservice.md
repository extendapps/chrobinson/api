**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderService

# Interface: OrderService

## Hierarchy

* **OrderService**

## Index

### Properties

* [collectOnDelivered](orderservice.md#collectondelivered)
* [dateCalculationRule](orderservice.md#datecalculationrule)
* [isCritical](orderservice.md#iscritical)
* [isDangerous](orderservice.md#isdangerous)
* [isHighValue](orderservice.md#ishighvalue)
* [isHot](orderservice.md#ishot)
* [isPartial](orderservice.md#ispartial)
* [items](orderservice.md#items)
* [locations](orderservice.md#locations)
* [packages](orderservice.md#packages)
* [referenceNumbers](orderservice.md#referencenumbers)
* [sellMode](orderservice.md#sellmode)
* [serviceLevel](orderservice.md#servicelevel)
* [suggestedCarrierPartyCode](orderservice.md#suggestedcarrierpartycode)
* [suggestedScac](orderservice.md#suggestedscac)

## Properties

### collectOnDelivered

• `Optional` **collectOnDelivered**: [CollectOnDelivered](collectondelivered.md)

___

### dateCalculationRule

• `Optional` **dateCalculationRule**: \"promisedShipDate\" \| \"promisedDeliveryDate\"

___

### isCritical

• `Optional` **isCritical**: boolean

___

### isDangerous

• `Optional` **isDangerous**: boolean

___

### isHighValue

• `Optional` **isHighValue**: boolean

___

### isHot

• `Optional` **isHot**: boolean

___

### isPartial

• `Optional` **isPartial**: boolean

___

### items

•  **items**: [OrderItem](orderitem.md)[]

___

### locations

•  **locations**: [OrderLocation](orderlocation.md)[]

___

### packages

• `Optional` **packages**: [OrderPackage](orderpackage.md)[]

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### sellMode

• `Optional` **sellMode**: [SellMode](../README.md#sellmode)

___

### serviceLevel

• `Optional` **serviceLevel**: string

___

### suggestedCarrierPartyCode

• `Optional` **suggestedCarrierPartyCode**: string

___

### suggestedScac

• `Optional` **suggestedScac**: string
