**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / OrderUpdatedEvent

# Interface: OrderUpdatedEvent

## Hierarchy

* [OrderEvent](orderevent.md)

  ↳ **OrderUpdatedEvent**

## Index

### Properties

* [eventType](orderupdatedevent.md#eventtype)
* [loadNumbers](orderupdatedevent.md#loadnumbers)
* [orderDetail](orderupdatedevent.md#orderdetail)
* [referenceNumbers](orderupdatedevent.md#referencenumbers)

## Properties

### eventType

•  **eventType**: \"ORDER UPDATED\"

*Overrides [OrderEvent](orderevent.md).[eventType](orderevent.md#eventtype)*

___

### loadNumbers

•  **loadNumbers**: number[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[loadNumbers](multiloadevent.md#loadnumbers)*

___

### orderDetail

•  **orderDetail**: [OrderDetail](orderdetail.md)[]

*Inherited from [MultiLoadEvent](multiloadevent.md).[orderDetail](multiloadevent.md#orderdetail)*

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

*Inherited from [OrderEvent](orderevent.md).[referenceNumbers](orderevent.md#referencenumbers)*
