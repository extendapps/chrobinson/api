**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / PackageDeliveredEvent

# Interface: PackageDeliveredEvent

## Hierarchy

* [PackageStopEvent](packagestopevent.md)

  ↳ **PackageDeliveredEvent**

## Index

### Properties

* [carrier](packagedeliveredevent.md#carrier)
* [eventType](packagedeliveredevent.md#eventtype)
* [loadNumber](packagedeliveredevent.md#loadnumber)
* [location](packagedeliveredevent.md#location)
* [orderDetails](packagedeliveredevent.md#orderdetails)
* [packages](packagedeliveredevent.md#packages)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

*Inherited from [PackageStopEvent](packagestopevent.md).[carrier](packagestopevent.md#carrier)*

___

### eventType

•  **eventType**: \"PACKAGE DELIVERED\"

*Overrides [PackageStopEvent](packagestopevent.md).[eventType](packagestopevent.md#eventtype)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

*Inherited from [PackageStopEvent](packagestopevent.md).[location](packagestopevent.md#location)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventPackage](eventpackage.md)[]

*Inherited from [PackageStopEvent](packagestopevent.md).[packages](packagestopevent.md#packages)*
