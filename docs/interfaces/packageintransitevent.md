**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / PackageInTransitEvent

# Interface: PackageInTransitEvent

## Hierarchy

* [SingleLoadEvent](singleloadevent.md)

  ↳ **PackageInTransitEvent**

## Index

### Properties

* [carrier](packageintransitevent.md#carrier)
* [eventType](packageintransitevent.md#eventtype)
* [loadNumber](packageintransitevent.md#loadnumber)
* [location](packageintransitevent.md#location)
* [orderDetails](packageintransitevent.md#orderdetails)
* [packages](packageintransitevent.md#packages)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

___

### eventType

•  **eventType**: \"PACKAGE IN TRANSIT\"

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [InTransitAddress](intransitaddress.md)

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventPackage](eventpackage.md)[]
