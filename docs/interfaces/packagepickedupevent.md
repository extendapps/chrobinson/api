**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / PackagePickedUpEvent

# Interface: PackagePickedUpEvent

## Hierarchy

* [PackageStopEvent](packagestopevent.md)

  ↳ **PackagePickedUpEvent**

## Index

### Properties

* [carrier](packagepickedupevent.md#carrier)
* [eventType](packagepickedupevent.md#eventtype)
* [loadNumber](packagepickedupevent.md#loadnumber)
* [location](packagepickedupevent.md#location)
* [orderDetails](packagepickedupevent.md#orderdetails)
* [packages](packagepickedupevent.md#packages)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

*Inherited from [PackageStopEvent](packagestopevent.md).[carrier](packagestopevent.md#carrier)*

___

### eventType

•  **eventType**: \"PACKAGE PICKED UP\"

*Overrides [PackageStopEvent](packagestopevent.md).[eventType](packagestopevent.md#eventtype)*

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

*Inherited from [PackageStopEvent](packagestopevent.md).[location](packagestopevent.md#location)*

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventPackage](eventpackage.md)[]

*Inherited from [PackageStopEvent](packagestopevent.md).[packages](packagestopevent.md#packages)*
