**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / PackageStopEvent

# Interface: PackageStopEvent

## Hierarchy

* [SingleLoadEvent](singleloadevent.md)

  ↳ **PackageStopEvent**

  ↳↳ [PackagePickedUpEvent](packagepickedupevent.md)

  ↳↳ [PackageDeliveredEvent](packagedeliveredevent.md)

## Index

### Properties

* [carrier](packagestopevent.md#carrier)
* [eventType](packagestopevent.md#eventtype)
* [loadNumber](packagestopevent.md#loadnumber)
* [location](packagestopevent.md#location)
* [orderDetails](packagestopevent.md#orderdetails)
* [packages](packagestopevent.md#packages)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

___

### eventType

•  **eventType**: \"PACKAGE PICKED UP\" \| \"PACKAGE DELIVERED\"

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventPackage](eventpackage.md)[]
