**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Quote

# Interface: Quote

## Hierarchy

* **Quote**

## Index

### Properties

* [cargoLiability](quote.md#cargoliability)
* [carrier](quote.md#carrier)
* [customer](quote.md#customer)
* [distance](quote.md#distance)
* [equipmentType](quote.md#equipmenttype)
* [quoteExpirationDate](quote.md#quoteexpirationdate)
* [quoteId](quote.md#quoteid)
* [rates](quote.md#rates)
* [totalAccessorialCharge](quote.md#totalaccessorialcharge)
* [totalCharge](quote.md#totalcharge)
* [totalFreightCharge](quote.md#totalfreightcharge)
* [transit](quote.md#transit)
* [transportModeType](quote.md#transportmodetype)

## Properties

### cargoLiability

• `Optional` **cargoLiability**: [CargoLiability](cargoliability.md)

___

### carrier

• `Optional` **carrier**: [Carrier](carrier.md)

___

### customer

•  **customer**: [OrderCustomer](ordercustomer.md)

___

### distance

•  **distance**: number

___

### equipmentType

•  **equipmentType**: [EquipmentType](../README.md#equipmenttype)

___

### quoteExpirationDate

• `Optional` **quoteExpirationDate**: string

___

### quoteId

•  **quoteId**: number

___

### rates

•  **rates**: [Rate](rate.md)[]

___

### totalAccessorialCharge

•  **totalAccessorialCharge**: number

___

### totalCharge

•  **totalCharge**: number

___

### totalFreightCharge

•  **totalFreightCharge**: number

___

### transit

•  **transit**: [Transit](transit.md)

___

### transportModeType

•  **transportModeType**: [TransportModeType](../README.md#transportmodetype)
