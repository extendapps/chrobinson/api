**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / QuoteBillTo

# Interface: QuoteBillTo

## Hierarchy

* **QuoteBillTo**

## Index

### Properties

* [contacts](quotebillto.md#contacts)
* [currencyCode](quotebillto.md#currencycode)
* [referenceNumbers](quotebillto.md#referencenumbers)

## Properties

### contacts

• `Optional` **contacts**: [Contact](contact.md)[]

___

### currencyCode

•  **currencyCode**: [CurrencyCode](../README.md#currencycode)

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]
