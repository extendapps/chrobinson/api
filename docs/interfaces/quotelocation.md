**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / QuoteLocation

# Interface: QuoteLocation

## Hierarchy

* **QuoteLocation**

## Index

### Properties

* [address](quotelocation.md#address)
* [contactName](quotelocation.md#contactname)
* [customerLocationId](quotelocation.md#customerlocationid)
* [emailAddress](quotelocation.md#emailaddress)
* [locationName](quotelocation.md#locationname)
* [phone](quotelocation.md#phone)
* [referenceNumbers](quotelocation.md#referencenumbers)
* [requestedShipDateClose](quotelocation.md#requestedshipdateclose)
* [requestedShipDateOpen](quotelocation.md#requestedshipdateopen)
* [specialInstructions](quotelocation.md#specialinstructions)

## Properties

### address

• `Optional` **address**: [OrderAddress](orderaddress.md)

___

### contactName

• `Optional` **contactName**: string

___

### customerLocationId

• `Optional` **customerLocationId**: string

___

### emailAddress

• `Optional` **emailAddress**: string

___

### locationName

• `Optional` **locationName**: string

___

### phone

• `Optional` **phone**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### requestedShipDateClose

•  **requestedShipDateClose**: string

___

### requestedShipDateOpen

•  **requestedShipDateOpen**: string

___

### specialInstructions

• `Optional` **specialInstructions**: string
