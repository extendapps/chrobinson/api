**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / QuoteOrder

# Interface: QuoteOrder

## Hierarchy

* **QuoteOrder**

## Index

### Properties

* [orderNumber](quoteorder.md#ordernumber)
* [quoteId](quoteorder.md#quoteid)
* [trackingNumber](quoteorder.md#trackingnumber)

## Properties

### orderNumber

•  **orderNumber**: number

___

### quoteId

•  **quoteId**: number

___

### trackingNumber

•  **trackingNumber**: string
