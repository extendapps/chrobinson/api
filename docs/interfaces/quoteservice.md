**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / QuoteService

# Interface: QuoteService

## Hierarchy

* **QuoteService**

## Index

### Properties

* [referenceNumbers](quoteservice.md#referencenumbers)
* [suggestedCarrierPartyCode](quoteservice.md#suggestedcarrierpartycode)
* [suggestedScac](quoteservice.md#suggestedscac)

## Properties

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### suggestedCarrierPartyCode

• `Optional` **suggestedCarrierPartyCode**: string

___

### suggestedScac

• `Optional` **suggestedScac**: string
