**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / QuoteToOrder

# Interface: QuoteToOrder

## Hierarchy

* **QuoteToOrder**

## Index

### Properties

* [billTo](quotetoorder.md#billto)
* [customer](quotetoorder.md#customer)
* [destination](quotetoorder.md#destination)
* [notes](quotetoorder.md#notes)
* [origin](quotetoorder.md#origin)
* [quoteId](quotetoorder.md#quoteid)
* [referenceNumbers](quotetoorder.md#referencenumbers)
* [service](quotetoorder.md#service)

## Properties

### billTo

• `Optional` **billTo**: [QuoteBillTo](quotebillto.md)

___

### customer

• `Optional` **customer**: [OrderCustomer](ordercustomer.md)

___

### destination

•  **destination**: [QuoteLocation](quotelocation.md)

___

### notes

• `Optional` **notes**: string

___

### origin

•  **origin**: [QuoteLocation](quotelocation.md)

___

### quoteId

•  **quoteId**: number

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### service

• `Optional` **service**: [QuoteService](quoteservice.md)
