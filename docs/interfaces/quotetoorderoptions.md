**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / QuoteToOrderOptions

# Interface: QuoteToOrderOptions

## Hierarchy

* BaseOptions

  ↳ **QuoteToOrderOptions**

## Index

### Properties

* [quoteToOrders](quotetoorderoptions.md#quotetoorders)

### Methods

* [BadRequest](quotetoorderoptions.md#badrequest)
* [Created](quotetoorderoptions.md#created)
* [Failed](quotetoorderoptions.md#failed)

## Properties

### quoteToOrders

•  **quoteToOrders**: [QuoteToOrder](quotetoorder.md)[]

## Methods

### BadRequest

▸ **BadRequest**(`errorMessages`: [ErrorMessage](errormessage.md)[]): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`errorMessages` | [ErrorMessage](errormessage.md)[] |

**Returns:** void

___

### Created

▸ **Created**(`quoteOrders`: [QuoteOrder](quoteorder.md)[]): void

#### Parameters:

Name | Type |
------ | ------ |
`quoteOrders` | [QuoteOrder](quoteorder.md)[] |

**Returns:** void

___

### Failed

▸ **Failed**(`clientResponse`: ClientResponse): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`clientResponse` | ClientResponse |

**Returns:** void
