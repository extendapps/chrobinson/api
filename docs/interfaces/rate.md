**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Rate

# Interface: Rate

## Hierarchy

* **Rate**

## Index

### Properties

* [currencyCode](rate.md#currencycode)
* [isOptional](rate.md#isoptional)
* [quantity](rate.md#quantity)
* [rateCode](rate.md#ratecode)
* [rateCodeValue](rate.md#ratecodevalue)
* [rateId](rate.md#rateid)
* [totalRate](rate.md#totalrate)
* [unitRate](rate.md#unitrate)

## Properties

### currencyCode

•  **currencyCode**: [CurrencyCode](../README.md#currencycode)

___

### isOptional

•  **isOptional**: boolean

___

### quantity

•  **quantity**: number

___

### rateCode

•  **rateCode**: string

___

### rateCodeValue

•  **rateCodeValue**: string

___

### rateId

•  **rateId**: number

___

### totalRate

•  **totalRate**: number

___

### unitRate

•  **unitRate**: number
