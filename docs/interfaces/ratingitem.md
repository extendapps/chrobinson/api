**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / RatingItem

# Interface: RatingItem

## Hierarchy

* **RatingItem**

## Index

### Properties

* [actualWeight](ratingitem.md#actualweight)
* [declaredValue](ratingitem.md#declaredvalue)
* [density](ratingitem.md#density)
* [description](ratingitem.md#description)
* [freightClass](ratingitem.md#freightclass)
* [hazardousDescription](ratingitem.md#hazardousdescription)
* [hazardousEmergencyPhone](ratingitem.md#hazardousemergencyphone)
* [height](ratingitem.md#height)
* [isHazardous](ratingitem.md#ishazardous)
* [isOverWeightOverDimensional](ratingitem.md#isoverweightoverdimensional)
* [isStackable](ratingitem.md#isstackable)
* [isUsedGood](ratingitem.md#isusedgood)
* [length](ratingitem.md#length)
* [linearSpace](ratingitem.md#linearspace)
* [linearUnit](ratingitem.md#linearunit)
* [nmfc](ratingitem.md#nmfc)
* [packagingCode](ratingitem.md#packagingcode)
* [palletSpaces](ratingitem.md#palletspaces)
* [pallets](ratingitem.md#pallets)
* [pieces](ratingitem.md#pieces)
* [plu](ratingitem.md#plu)
* [productCode](ratingitem.md#productcode)
* [productName](ratingitem.md#productname)
* [referenceNumbers](ratingitem.md#referencenumbers)
* [requiredTemperatureHigh](ratingitem.md#requiredtemperaturehigh)
* [requiredTemperatureLow](ratingitem.md#requiredtemperaturelow)
* [sku](ratingitem.md#sku)
* [temperatureSensitive](ratingitem.md#temperaturesensitive)
* [temperatureUnit](ratingitem.md#temperatureunit)
* [unitVolume](ratingitem.md#unitvolume)
* [unitWeight](ratingitem.md#unitweight)
* [unitsPerPallet](ratingitem.md#unitsperpallet)
* [upc](ratingitem.md#upc)
* [volume](ratingitem.md#volume)
* [volumeUnit](ratingitem.md#volumeunit)
* [weightUnit](ratingitem.md#weightunit)
* [width](ratingitem.md#width)

## Properties

### actualWeight

•  **actualWeight**: number

___

### declaredValue

• `Optional` **declaredValue**: number

___

### density

• `Optional` **density**: number

___

### description

• `Optional` **description**: string

___

### freightClass

• `Optional` **freightClass**: [FreightClass](../README.md#freightclass)

___

### hazardousDescription

• `Optional` **hazardousDescription**: string

___

### hazardousEmergencyPhone

• `Optional` **hazardousEmergencyPhone**: string

___

### height

• `Optional` **height**: number

___

### isHazardous

• `Optional` **isHazardous**: boolean

___

### isOverWeightOverDimensional

• `Optional` **isOverWeightOverDimensional**: boolean

___

### isStackable

• `Optional` **isStackable**: boolean

___

### isUsedGood

• `Optional` **isUsedGood**: boolean

___

### length

• `Optional` **length**: number

___

### linearSpace

• `Optional` **linearSpace**: number

___

### linearUnit

• `Optional` **linearUnit**: [LinearUnit](../README.md#linearunit)

___

### nmfc

• `Optional` **nmfc**: string

___

### packagingCode

• `Optional` **packagingCode**: [PackagingCode](../README.md#packagingcode)

___

### palletSpaces

• `Optional` **palletSpaces**: number

___

### pallets

• `Optional` **pallets**: number

___

### pieces

• `Optional` **pieces**: number

___

### plu

• `Optional` **plu**: string

___

### productCode

• `Optional` **productCode**: string

___

### productName

• `Optional` **productName**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### requiredTemperatureHigh

• `Optional` **requiredTemperatureHigh**: number

___

### requiredTemperatureLow

• `Optional` **requiredTemperatureLow**: number

___

### sku

• `Optional` **sku**: string

___

### temperatureSensitive

• `Optional` **temperatureSensitive**: [TemperatureType](../README.md#temperaturetype)

___

### temperatureUnit

• `Optional` **temperatureUnit**: [TemperatureUnit](../README.md#temperatureunit)

___

### unitVolume

• `Optional` **unitVolume**: number

___

### unitWeight

• `Optional` **unitWeight**: number

___

### unitsPerPallet

• `Optional` **unitsPerPallet**: number

___

### upc

• `Optional` **upc**: string

___

### volume

• `Optional` **volume**: number

___

### volumeUnit

• `Optional` **volumeUnit**: [VolumeUnit](../README.md#volumeunit)

___

### weightUnit

•  **weightUnit**: [WeightUnit](../README.md#weightunit)

___

### width

• `Optional` **width**: number
