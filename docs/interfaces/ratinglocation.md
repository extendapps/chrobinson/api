**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / RatingLocation

# Interface: RatingLocation

## Hierarchy

* **RatingLocation**

## Index

### Properties

* [address1](ratinglocation.md#address1)
* [address2](ratinglocation.md#address2)
* [address3](ratinglocation.md#address3)
* [city](ratinglocation.md#city)
* [countryCode](ratinglocation.md#countrycode)
* [customerLocationId](ratinglocation.md#customerlocationid)
* [iata](ratinglocation.md#iata)
* [isPort](ratinglocation.md#isport)
* [latitude](ratinglocation.md#latitude)
* [locationName](ratinglocation.md#locationname)
* [longitude](ratinglocation.md#longitude)
* [postalCode](ratinglocation.md#postalcode)
* [referenceNumbers](ratinglocation.md#referencenumbers)
* [requestedCloseDateTime](ratinglocation.md#requestedclosedatetime)
* [requestedOpenDateTime](ratinglocation.md#requestedopendatetime)
* [specialRequirement](ratinglocation.md#specialrequirement)
* [stateProvinceCode](ratinglocation.md#stateprovincecode)
* [unLocode](ratinglocation.md#unlocode)

## Properties

### address1

• `Optional` **address1**: string

___

### address2

• `Optional` **address2**: string

___

### address3

• `Optional` **address3**: string

___

### city

• `Optional` **city**: string

___

### countryCode

•  **countryCode**: string

___

### customerLocationId

• `Optional` **customerLocationId**: string

___

### iata

• `Optional` **iata**: string

___

### isPort

• `Optional` **isPort**: boolean \| string

___

### latitude

• `Optional` **latitude**: string \| number

___

### locationName

• `Optional` **locationName**: string

___

### longitude

• `Optional` **longitude**: string \| number

___

### postalCode

•  **postalCode**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### requestedCloseDateTime

• `Optional` **requestedCloseDateTime**: string

___

### requestedOpenDateTime

• `Optional` **requestedOpenDateTime**: string

___

### specialRequirement

• `Optional` **specialRequirement**: [SpecialRequirement](specialrequirement.md)

___

### stateProvinceCode

• `Optional` **stateProvinceCode**: string

___

### unLocode

• `Optional` **unLocode**: string
