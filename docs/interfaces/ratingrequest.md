**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / RatingRequest

# Interface: RatingRequest

## Hierarchy

* **RatingRequest**

## Index

### Properties

* [customerCode](ratingrequest.md#customercode)
* [destination](ratingrequest.md#destination)
* [items](ratingrequest.md#items)
* [optionalAccessorials](ratingrequest.md#optionalaccessorials)
* [origin](ratingrequest.md#origin)
* [platform](ratingrequest.md#platform)
* [referenceNumbers](ratingrequest.md#referencenumbers)
* [shipDate](ratingrequest.md#shipdate)
* [transactionId](ratingrequest.md#transactionid)
* [transportModes](ratingrequest.md#transportmodes)

## Properties

### customerCode

•  **customerCode**: string

___

### destination

•  **destination**: [RatingLocation](ratinglocation.md)

___

### items

•  **items**: [RatingItem](ratingitem.md)[]

___

### optionalAccessorials

• `Optional` **optionalAccessorials**: string[]

___

### origin

•  **origin**: [RatingLocation](ratinglocation.md)

___

### platform

•  **platform**: string

___

### referenceNumbers

• `Optional` **referenceNumbers**: [ReferenceNumber](referencenumber.md)[]

___

### shipDate

•  **shipDate**: string

___

### transactionId

• `Optional` **transactionId**: string

___

### transportModes

•  **transportModes**: [TransportMode](transportmode.md)[]
