**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / RatingRequestOptions

# Interface: RatingRequestOptions

## Hierarchy

* BaseOptions

  ↳ **RatingRequestOptions**

## Index

### Properties

* [request](ratingrequestoptions.md#request)

### Methods

* [BadRequest](ratingrequestoptions.md#badrequest)
* [Created](ratingrequestoptions.md#created)
* [Failed](ratingrequestoptions.md#failed)

## Properties

### request

•  **request**: [RatingRequest](ratingrequest.md)

## Methods

### BadRequest

▸ **BadRequest**(`errorMessages`: [ErrorMessage](errormessage.md)[]): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`errorMessages` | [ErrorMessage](errormessage.md)[] |

**Returns:** void

___

### Created

▸ **Created**(`quoteSummaries`: [Quote](quote.md)[]): void

#### Parameters:

Name | Type |
------ | ------ |
`quoteSummaries` | [Quote](quote.md)[] |

**Returns:** void

___

### Failed

▸ **Failed**(`clientResponse`: ClientResponse): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`clientResponse` | ClientResponse |

**Returns:** void
