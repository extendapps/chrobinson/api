**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / RatingRequestResponse

# Interface: RatingRequestResponse

## Hierarchy

* **RatingRequestResponse**

## Index

### Properties

* [quoteSummaries](ratingrequestresponse.md#quotesummaries)

## Properties

### quoteSummaries

•  **quoteSummaries**: [Quote](quote.md)[]
