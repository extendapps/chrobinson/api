**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / ReferenceNumber

# Interface: ReferenceNumber

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **ReferenceNumber**

## Index

### Properties

* [type](referencenumber.md#type)
* [value](referencenumber.md#value)

## Properties

### type

•  **type**: \"POLineNum\" \| \"ORD\" \| \"QUOTEID\" \| \"InfoFrom\" \| \"CustomerItemIdentifier1\" \| \"OrderItemId\" \| \"OrderID\" \| \"ORDTRK\" \| \"PRO\" \| \"SHID\" \| \"MBOL\" \| \"PO\" \| \"BOL\" \| \"CRID\" \| \"CUSTPO\" \| \"DEL\" \| \"JNO\" \| \"PU\" \| \"CON\" \| \"APPT\"

___

### value

•  **value**: string
