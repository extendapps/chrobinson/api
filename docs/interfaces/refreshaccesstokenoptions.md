**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / RefreshAccessTokenOptions

# Interface: RefreshAccessTokenOptions

## Hierarchy

* BaseOptions

  ↳ **RefreshAccessTokenOptions**

## Index

### Methods

* [BadRequest](refreshaccesstokenoptions.md#badrequest)
* [Failed](refreshaccesstokenoptions.md#failed)
* [Refreshed](refreshaccesstokenoptions.md#refreshed)

## Methods

### BadRequest

▸ **BadRequest**(`errorMessages`: [ErrorMessage](errormessage.md)[]): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`errorMessages` | [ErrorMessage](errormessage.md)[] |

**Returns:** void

___

### Failed

▸ **Failed**(`clientResponse`: ClientResponse): void

*Inherited from void*

#### Parameters:

Name | Type |
------ | ------ |
`clientResponse` | ClientResponse |

**Returns:** void

___

### Refreshed

▸ **Refreshed**(): void

**Returns:** void
