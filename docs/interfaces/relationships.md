**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Relationships

# Interface: Relationships

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **Relationships**

## Index

### Properties

* [innerPackQuantity](relationships.md#innerpackquantity)
* [innerPackUomCode](relationships.md#innerpackuomcode)
* [masterPackQuantity](relationships.md#masterpackquantity)

## Properties

### innerPackQuantity

• `Optional` **innerPackQuantity**: number

___

### innerPackUomCode

• `Optional` **innerPackUomCode**: string

___

### masterPackQuantity

• `Optional` **masterPackQuantity**: number
