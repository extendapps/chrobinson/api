**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / SingleLoadEvent

# Interface: SingleLoadEvent

## Hierarchy

* **SingleLoadEvent**

  ↳ [LoadEvent](loadevent.md)

  ↳ [AppointmentEvent](appointmentevent.md)

  ↳ [StopEvent](stopevent.md)

  ↳ [InTransitEvent](intransitevent.md)

  ↳ [PackageInTransitEvent](packageintransitevent.md)

  ↳ [PackageStopEvent](packagestopevent.md)

## Index

### Properties

* [loadNumber](singleloadevent.md#loadnumber)
* [orderDetails](singleloadevent.md#orderdetails)

## Properties

### loadNumber

•  **loadNumber**: number

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]
