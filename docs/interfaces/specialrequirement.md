**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / SpecialRequirement

# Interface: SpecialRequirement

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **SpecialRequirement**

## Index

### Properties

* [appointmentNotification](specialrequirement.md#appointmentnotification)
* [constructionSite](specialrequirement.md#constructionsite)
* [dropOffAtCarrierTerminal](specialrequirement.md#dropoffatcarrierterminal)
* [insideDelivery](specialrequirement.md#insidedelivery)
* [insidePickup](specialrequirement.md#insidepickup)
* [liftGate](specialrequirement.md#liftgate)
* [limitedAccess](specialrequirement.md#limitedaccess)
* [pickupAtCarrierTerminal](specialrequirement.md#pickupatcarrierterminal)
* [residentialNonCommercial](specialrequirement.md#residentialnoncommercial)
* [sortAndSegregate](specialrequirement.md#sortandsegregate)
* [tradeShoworConvention](specialrequirement.md#tradeshoworconvention)

## Properties

### appointmentNotification

• `Optional` **appointmentNotification**: boolean \| string

___

### constructionSite

• `Optional` **constructionSite**: boolean \| string

___

### dropOffAtCarrierTerminal

• `Optional` **dropOffAtCarrierTerminal**: boolean \| string

___

### insideDelivery

• `Optional` **insideDelivery**: boolean \| string

___

### insidePickup

• `Optional` **insidePickup**: boolean \| string

___

### liftGate

• `Optional` **liftGate**: boolean \| string

___

### limitedAccess

• `Optional` **limitedAccess**: boolean \| string

___

### pickupAtCarrierTerminal

• `Optional` **pickupAtCarrierTerminal**: boolean \| string

___

### residentialNonCommercial

• `Optional` **residentialNonCommercial**: boolean \| string

___

### sortAndSegregate

• `Optional` **sortAndSegregate**: boolean \| string

___

### tradeShoworConvention

• `Optional` **tradeShoworConvention**: boolean \| string
