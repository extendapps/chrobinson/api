**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / StopEvent

# Interface: StopEvent

## Hierarchy

* [SingleLoadEvent](singleloadevent.md)

  ↳ **StopEvent**

  ↳↳ [CarrierArrivedEvent](carrierarrivedevent.md)

  ↳↳ [CarrierDepartedEvent](carrierdepartedevent.md)

  ↳↳ [LoadPickedUpEvent](loadpickedupevent.md)

  ↳↳ [LoadDeliveredEvent](loaddeliveredevent.md)

## Index

### Properties

* [carrier](stopevent.md#carrier)
* [eventType](stopevent.md#eventtype)
* [items](stopevent.md#items)
* [lateReasonCode](stopevent.md#latereasoncode)
* [loadNumber](stopevent.md#loadnumber)
* [location](stopevent.md#location)
* [orderDetails](stopevent.md#orderdetails)
* [packages](stopevent.md#packages)
* [proofOfDelivery](stopevent.md#proofofdelivery)

## Properties

### carrier

•  **carrier**: [Carrier](carrier.md)

___

### eventType

•  **eventType**: \"CARRIER ARRIVED\" \| \"CARRIER DEPARTED\" \| \"LOAD PICKED UP\" \| \"LOAD DELIVERED\"

___

### items

•  **items**: [EventItem](eventitem.md)[]

___

### lateReasonCode

•  **lateReasonCode**: string

___

### loadNumber

•  **loadNumber**: number

*Inherited from [SingleLoadEvent](singleloadevent.md).[loadNumber](singleloadevent.md#loadnumber)*

___

### location

•  **location**: [EventLocation](eventlocation.md)

___

### orderDetails

•  **orderDetails**: [OrderDetail](orderdetail.md)[]

*Inherited from [SingleLoadEvent](singleloadevent.md).[orderDetails](singleloadevent.md#orderdetails)*

___

### packages

•  **packages**: [EventLoadPackage](eventloadpackage.md)[]

___

### proofOfDelivery

•  **proofOfDelivery**: string
