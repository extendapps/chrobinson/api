**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / Transit

# Interface: Transit

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **Transit**

## Index

### Properties

* [destinationService](transit.md#destinationservice)
* [maximumTransitDays](transit.md#maximumtransitdays)
* [minimumTransitDays](transit.md#minimumtransitdays)
* [originService](transit.md#originservice)

## Properties

### destinationService

• `Optional` **destinationService**: string

___

### maximumTransitDays

•  **maximumTransitDays**: number

___

### minimumTransitDays

•  **minimumTransitDays**: number

___

### originService

• `Optional` **originService**: string
