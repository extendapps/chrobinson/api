**[C.H.Robinson API NetSuite module](../README.md)**

> [Globals](../README.md) / TransportMode

# Interface: TransportMode

## Hierarchy

* **TransportMode**

## Index

### Properties

* [equipments](transportmode.md#equipments)
* [mode](transportmode.md#mode)

## Properties

### equipments

• `Optional` **equipments**: [Equipment](equipment.md)[]

___

### mode

•  **mode**: [TransportModeType](../README.md#transportmodetype)
